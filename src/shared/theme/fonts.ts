export default {
  montserrat: {
    black: "Montserrat-Black",
    blackItalic: "Montserrat-BlackItalic",
    bold: "Montserrat-Bold",
    boldItalic: "Montserrat-BoldItalic",
    extraBold: "Montserrat-ExtraBold",
    extraBoldItalic: "Montserrat-ExtraBoldItalic",
    extraLight: "Montserrat-ExtraLight",
    extraLightItalic: "Montserrat-ExtraLightItalic",
    italic: "Montserrat-Italic",
    light: "Montserrat-Light",
    lightItalic: "Montserrat-LightItalic",
    medium: "Montserrat-Medium",
    mediumItalic: "Montserrat-MediumItalic",
    regular: "Montserrat-Regular",
    semiBold: "Montserrat-SemiBold",
    semiBoldItalic: "Montserrat-SemiBoldItalic",
    thin: "Montserrat-Thin",
    thinItalic: "Montserrat-ThinItalic",
  },
  poppins: {
    black: "Poppins-Black",
    blackItalic: "Poppins-BlackItalic",
    bold: "Poppins-Bold",
    boldItalic: "Poppins-BoldItalic",
    extraBold: "Poppins-ExtraBold",
    extraBoldItalic: "Poppins-ExtraBoldItalic",
    extraLight: "Poppins-ExtraLight",
    extraLightItalic: "Poppins-ExtraLightItalic",
    italic: "Poppins-Italic",
    light: "Poppins-Light",
    lightItalic: "Poppins-LightItalic",
    medium: "Poppins-Medium",
    mediumItalic: "Poppins-MediumItalic",
    regular: "Poppins-Regular",
    semiBold: "Poppins-SemiBold",
    semiBoldItalic: "Poppins-SemiBoldItalic",
    thin: "Poppins-Thin",
    thinItalic: "Poppins-ThinItalic",
  },
};
