import { TextInput, View, TextInputProps, TextStyle } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "./text-wrapper/TextWrapper";
import createStyles from "./components.style";
import { useTheme } from "@react-navigation/native";

interface InputTextProps extends TextInputProps {
    label?: string;
    disable?: boolean;
    isMultiline?: boolean;
    inputStyle?: TextStyle;
    rightIcon?: React.ReactNode;
    borderColor?: string;
    secureTextEntry?: boolean;
    maxLength?: number;
    borderWidth?: number;
    height?: number;
}

const InputText: React.FC<InputTextProps> = (props) => {
    const theme = useTheme();
    const { colors } = theme;
    const { label, placeholder, value, onChangeText, style, inputStyle, keyboardType, disable, onFocus, onBlur, numberOfLines, rightIcon, borderColor = colors.primary,borderWidth = 0.5, secureTextEntry=false, maxLength=30,height = 46} = props;
    const styles = useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={style}>
            {label && <TextWrapper>{label}</TextWrapper>}
            <TextInput
                style={[styles.input, {borderColor: borderColor, borderWidth: borderWidth, height: height,},inputStyle]}
                onChangeText={onChangeText}
                value={value}
                editable={!disable}
                placeholder={placeholder}
                placeholderTextColor={colors.lightGray}
                keyboardType={keyboardType}
                onFocus={onFocus}
                onBlur={onBlur}
                multiline={numberOfLines ? true : false}
                numberOfLines={numberOfLines}
                secureTextEntry={secureTextEntry}
                maxLength={maxLength}
                textAlignVertical={numberOfLines ? "top" : "center"}
            />
            {rightIcon}
        </View>
    );
};

export default InputText;
