import React, { useState, useRef, useMemo } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "../components.style";

interface OTPInputProps {
  length: number;
  onOtpChange: (otp: string) => void;
}

const OTPInputComponent: React.FC<OTPInputProps> = ({ length, onOtpChange }) => {
  const theme = useTheme();
  const { colors } = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  const [otp, setOtp] = useState<string[]>(new Array(length).fill(''));
  const inputs = useRef<TextInput[]>([]);

  const handleChange = (value: string, index: number) => {
    if (isNaN(Number(value))) return;
    const newOtp = [...otp];
    newOtp[index] = value;
    setOtp(newOtp);
    onOtpChange(newOtp.join(''));

    // Move focus to the next input
    if (value && index < length - 1) {
      inputs.current[index + 1].focus();
    }
  };

  const handleBackspace = (value: string, index: number) => {
    if (!value && index > 0) {
      inputs.current[index - 1].focus();
    }
  };

  return (
    <View style={styles.otpContainer}>
      {otp.map((_, index) => (
        <TextInput
          key={index}
          ref={(ref) => (inputs.current[index] = ref!)}
          style={styles.otpInputBox}
          keyboardType="number-pad"
          maxLength={1}
          value={otp[index]}
          onChangeText={(value) => handleChange(value, index)}
          onKeyPress={({ nativeEvent }) =>
            nativeEvent.key === 'Backspace' ? handleBackspace(otp[index], index) : null
          }
        />
      ))}
    </View>
  );
};

export default OTPInputComponent;
