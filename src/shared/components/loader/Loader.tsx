import { View, TextInputProps, ActivityIndicator } from "react-native";
import React, { useMemo } from "react";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";

interface InputTextProps extends TextInputProps {
    isLoading?: boolean;
}

const Loader: React.FC<InputTextProps> = () => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);

    return <View style={styles.loaderBg}>
        <ActivityIndicator size={'large'}/>
    </View>
};

export default Loader;
