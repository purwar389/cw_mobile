import { StyleSheet } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import fonts from "../theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    input: {
        backgroundColor: colors.background,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        borderColor: colors.primary,
        borderRadius: 5,
        paddingVertical: 0,
        fontFamily: fonts.poppins.regular
    },
    mh20: {
      marginHorizontal: 20
    },
    filledButton:{
      backgroundColor: colors.primary,
      borderRadius: 5,
      marginVertical: 10,
      alignItems: 'center',
      justifyContent: 'center'
    },
    notSelectedButton:{
      backgroundColor: colors.background,
      borderRadius: 5,
      marginHorizontal: 5,
      borderWidth: 0.5,
      marginVertical: 10,
      alignItems: 'center',
      justifyContent: 'center'
    },
    selectedButton: {
      backgroundColor: colors.primary,
      borderRadius: 5,
      marginVertical: 10,
      alignItems: 'center',
      justifyContent: 'center',
      marginHorizontal: 5,
      paddingVertical: 5
    },
    roundedButton:{
      backgroundColor: colors.primary,
      borderRadius: 20,
      marginVertical: 10,
      alignItems: 'center',
      paddingHorizontal: 20,
      justifyContent: 'center'
    },
    roundedButtonOutlined:{
      borderRadius: 20,
      borderWidth: 0.5,
      borderColor: colors.primary,
      marginVertical: 10,
      paddingHorizontal: 20,
      alignItems: 'center',
      justifyContent: 'center'
    },
    filledButtonText: {
      fontSize: 20,
      fontWeight: '500',
    },
    outlinedButton: {
      backgroundColor: colors.background,
      borderRadius: 5,
      marginVertical: 10,
      alignItems: 'center',
      justifyContent: 'center'
    },
    outlinedButtonText: {
      fontSize: 20,
      fontWeight: '500',
    },
    headerContainer: {
      height: 60,
      // marginTop: 30,
      backgroundColor: colors.iconWhite,
      justifyContent: 'flex-start',
      flexDirection: 'row',
      alignItems: 'center'
    },
    outlinedBtnStyle: {
      flexDirection: 'row',
      paddingHorizontal: 5,
      marginHorizontal: 10,
      backgroundColor: colors.iconWhite
    },
    outlinedBtnStyle2: {
      flexDirection: 'row',
      paddingHorizontal: 10,
      paddingVertical: 2,
      marginHorizontal: 10,
      backgroundColor: colors.background
    },
    outlinedbuttonLabel: {
      fontSize: 14,
      color: colors.primary,
      marginHorizontal: 5,
    },
    outlinedbuttonLabel2: {
      fontSize: 12,
      color: colors.primary,
      marginHorizontal: 5,
    },
    labelStyle: {
      fontSize: 14,
      color: colors.primary,
      marginHorizontal: 5
    },
    labelStyle3: {
      fontSize: 12,
      color: colors.primary,
      marginHorizontal: 5
    },
    labelStyle2: {
      fontSize: 12,
      color: colors.iconWhite,
      marginHorizontal: 5,
      justifyContent: 'center'
    },
    fdRow: {
      flexDirection: 'row'
    },
    textSeparator: {
      width: 2,
      height: 16,
      marginHorizontal: 5,
      marginVertical: 3,
      backgroundColor: colors.primary
    },
    remarksButton: {
      borderColor: colors.iconBlack,
      borderWidth: 1,
      borderRadius: 17,
      flexDirection: 'row',
      alignItems: 'center',
      alignSelf: 'flex-start',
      backgroundColor: '#F7F7F7',
      marginHorizontal: 10,
      paddingHorizontal: 8,
      paddingVertical: 3,
      marginTop: 10,
      marginBottom: 5,
    },
    remarksText: {
      color: colors.gray,
      fontSize: 12,
    },
    mh5: {
      marginHorizontal: 5
    },
    separator: {
      height: 1, 
      backgroundColor: colors.primary, 
      marginVertical: 10
    },
    caseCardFooterContainer: {
      justifyContent: 'space-between', 
      flexDirection: 'row', 
      paddingHorizontal: 10
    },
    caseCardContainer: {
      marginHorizontal: 20, 
      paddingVertical: 10,
      marginVertical: 5, 
      backgroundColor: 'white', 
      borderRadius: 5, 
      elevation: 3
    },
    searchTextInput: {
      fontSize: 14,
      color: colors.gray
    },
    dropDownStyle: {
      borderColor: colors.primary,
      borderWidth: 0.5,
      borderRadius: 5,
      paddingHorizontal: 10,
      paddingVertical: 10,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
  mh_10: {
      marginHorizontal: -15,
      marginVertical: 5
  },
  authCardStyle: {
      backgroundColor: colors.iconWhite, 
      borderColor: colors.primary, 
      borderWidth: 0.5, 
      borderRadius: 5, 
      padding: 10, 
      marginHorizontal: 20, 
      marginVertical: 5
  },
  modalOverlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)', // Modal background transparency
  },
  modalContent: {
    width: 300,
    padding: 20,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  fs16: {fontSize: 16},
  fs14: {fontSize: 14},
  otpInputBox: {
    borderWidth: 0.5,
    borderColor: '#000',
    width: 42,
    height: 42,
    backgroundColor: colors.white,
    textAlign: 'center',
    fontSize: 18,
    margin: 5,
    borderRadius: 5,
  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: 20
  },
  loaderBg: {
    alignItems: 'center', 
    position: 'absolute', 
    justifyContent: 'center', 
    height: '100%', 
    width: '100%', 
    zIndex: 1, 
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  }
  });
};
