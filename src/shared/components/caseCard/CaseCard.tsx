import { Text, View, TextInputProps, TouchableOpacity, Pressable, Dimensions, Modal } from "react-native";
import React, { useMemo, useState } from "react";
import createStyles from "../components.style";
import { useNavigation, useTheme } from "@react-navigation/native";
import TextWrapper from "../../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../../shared/components/buttons/OutlinedButton";
import { SCREENS } from "../../../shared/constants";
import OrderWalaHathoda from "../../../assets/icons/orderWalaHathoda";
import RespondentIcon from "../../../assets/icons/respondentIcon";
import UsersIcon from "../../../assets/icons/users";
import fonts from "../../../shared/theme/fonts";
import LinkIcon from "../../../assets/icons/linkIcon";
import RemarksIcon from "../../../assets/icons/remarksIcon";
import { localStrings } from "../../../shared/localization";
import TarajuIcon from "../../../assets/icons/tarajuIcon";
import StarIcon from "../../../assets/icons/starIcon";
import StarIconFilled from "../../../assets/icons/starIconFilled";

interface InputTextProps {
    itemData?: string;
    title: string;
}

const CaseCard: React.FC<InputTextProps> = ({itemData, title}) => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [showRemarks, setShowRemarks] = useState(false)

    const toggleStar = async() => {
        const apiEndpoint = `/{caseId}/mark-favorite`;
        // setIsLoading(true)
        // try {
        //     const response = await API.getAPIService(apiEndpoint);
        //     console.log(`response ==> `+JSON.stringify(response))
        //     setIsLoading(false);
        //     setCaseData(response)
        // } catch (error) {
        //     setIsLoading(false)
        //     console.error('Error fetching active cases:', error);
        //     throw error;
        // }
    }

    return (
        <TouchableOpacity style={styles.caseCardContainer}
            onPress={() => navigation.navigate(SCREENS.CASE_DETAIL)}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginStart: 10
                    }}>
                        <TouchableOpacity onPress={() => toggleStar(itemData?.fav_mark)}>
                            {itemData?.fav_mark == "0" ? <StarIcon/> : <StarIconFilled/>}
                        </TouchableOpacity>
                        <Text ellipsizeMode="tail" numberOfLines={1} style={{overflow: 'hidden',flex:1, marginHorizontal: 10, color: '#352F36', fontFamily: fonts.poppins.regular, lineHeight: 19}}>{`${itemData?.case_category}/${itemData?.case_no}/${itemData?.case_year}`}</Text>
                        <LinkIcon/>
                    </View>
                    <OutlinedButton label={itemData?.status} style={styles.outlinedBtnStyle} labelStyle={styles.outlinedbuttonLabel} labelFontFamily={fonts.poppins.medium}/>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center',paddingHorizontal: 10}}>
                    <TextWrapper style={{fontSize: 16}} color={colors.text}>{itemData.title}</TextWrapper>
                </View>
                <View style={{height: 10}}/>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={{marginHorizontal: 10, flexDirection: 'row', alignItems: 'center'}}
                    style={{ fontSize: 14, marginHorizontal: 5 }} 
                    leftIcon={<TarajuIcon/>}
                >{itemData?.court_bench}</TextWrapper>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={{marginHorizontal: 10, flexDirection: 'row', alignItems: 'center'}}
                    style={{ fontSize: 14, marginHorizontal: 5 }} 
                    leftIcon={<OrderWalaHathoda/>}
                >{itemData?.judge_name}</TextWrapper>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={{marginHorizontal: 10, flexDirection: 'row', alignItems: 'center'}}
                    style={{ fontSize: 14, marginHorizontal: 5 }} 
                    leftIcon={<RespondentIcon/>}
                >{itemData?.respondents}</TextWrapper>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={{marginHorizontal: 10, flexDirection: 'row', alignItems: 'center'}}
                    style={{ fontSize: 14, marginHorizontal: 5 }} 
                    leftIcon={<UsersIcon/>}
                >{itemData?.organization_id}</TextWrapper>

                <Pressable style={styles.remarksButton} onPress={() => setShowRemarks(true)}>
                    <View style={styles.mh5}>
                        <RemarksIcon/>
                    </View>
                    <TextWrapper style={styles.mh5} color={colors.gray}>{localStrings.remarks}</TextWrapper>
                </Pressable>

                <View style={styles.separator}/>

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={showRemarks}
                    onRequestClose={() => setShowRemarks(false)}>
                        <View style={styles.modalOverlay}>
                            <View style={styles.modalContent}>
                                <TextWrapper color={colors.primary} style={styles.fs16} fontFamily={fonts.poppins.medium}>{localStrings.remarks}</TextWrapper>
                                <TextWrapper color={colors.darkGray} style={styles.fs14}>{itemData?.remarks}</TextWrapper>
                            </View>
                        </View>
                </Modal>

                <View style={styles.caseCardFooterContainer}>
                    <View style={[styles.fdRow, {alignItems: 'center'}]}>
                        <TextWrapper color={colors.text}>{itemData?.court_room_no}</TextWrapper>
                        <View style={styles.textSeparator}/>
                        <TextWrapper color={colors.text}>{itemData?.sr_no_in_court}</TextWrapper>
                    </View>
                    {title != "Today's Cases" ? <TextWrapper color={colors.text}>{itemData?.next_date}</TextWrapper>
                    : <View style={{backgroundColor: colors.gray, paddingHorizontal: 20, paddingVertical: 3, borderRadius: 17, alignItems: 'center', justifyContent: 'center'}}>
                        <TextWrapper color={colors.white} style={{fontSize: 12}}>Attended</TextWrapper>
                    </View>}
                </View>
        </TouchableOpacity>

    );
};

export default CaseCard;
