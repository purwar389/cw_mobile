import { View, TouchableOpacity, TouchableOpacityProps } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import { NavigationProp, useTheme } from "@react-navigation/native";
import BackButtonIcon from "../../../assets/icons/backButton";
import { localStrings } from "../../../shared/localization";
import NotificationIcon from "../../../assets/icons/notificationIcon";
import MenuIcon from "../../../assets/icons/menuIcon";
import fonts from "../../theme/fonts";
interface HeaderProps extends TouchableOpacityProps {
    label: string;
    navigation: NavigationProp<ReactNavigation.RootParamList>;
}

const Header2: React.FC<HeaderProps> = (props) => {
    const { label, navigation } = props;
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    
    const _handleIconPress = () => {
        navigation.goBack();
    }
  
    return (
        <View style={styles.headerContainer}>
            <TouchableOpacity style={{marginHorizontal: 10}}
                onPress={_handleIconPress}>
                <BackButtonIcon/>
            </TouchableOpacity>

            <View style={{flex: 1, alignItems: 'center'}}>
                <TextWrapper color={colors.darkGray} fontFamily={fonts.poppins.medium} style={styles.outlinedButtonText}>{label}</TextWrapper>
                <TextWrapper color={colors.gray} fontFamily={fonts.poppins.regular} style={{...styles.outlinedButtonText, fontSize: 14}}>999 Cases</TextWrapper>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity 
                    style={{ marginHorizontal: 10 }}
                    onPress={() => {
                        navigation.navigate(localStrings.notifications)
                    }}>
                        <NotificationIcon />
                </TouchableOpacity>
                <TouchableOpacity 
                    style={{ marginHorizontal: 10 }}
                    onPress={() => navigation.openDrawer()}>
                        <MenuIcon />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Header2;
