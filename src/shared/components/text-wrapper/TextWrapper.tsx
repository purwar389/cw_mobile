import React from "react";
import { Text, TextStyle, View, ViewStyle } from "react-native";

/**
 * ? Local Imports
 */
import fonts from "../../theme/fonts";

interface ITextWrapperProps {
  color?: string;
  fontFamily?: string;
  style?: TextStyle;
  viewStyle?: ViewStyle;
  leftIcon?: React.ReactNode;
  rightIcon?: React.ReactNode;
  children?: React.ReactNode;
}

const TextWrapper: React.FC<ITextWrapperProps> = ({
  fontFamily = fonts.poppins.regular,
  color = "#757575",
  style,
  viewStyle,
  children,
  leftIcon,
  rightIcon,
  ...rest
}) => {
  return (
    <View style={viewStyle}>
      {leftIcon}
      <Text
        style={[{ fontFamily, color, includeFontPadding: false }, style]}
        {...rest}
      >
        {children}
      </Text>
      {rightIcon}
    </View>
  );
};

export default TextWrapper;
