import { Text, View } from "react-native";
import React, { useMemo } from "react";
import createStyles from "../components.style";
import { useNavigation, useTheme } from "@react-navigation/native";
import Feather from 'react-native-vector-icons/Feather';
import TextWrapper from "../../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../../shared/components/buttons/OutlinedButton";
import RoundedButton from "../../../shared/components/buttons/RoundedButton";
import MoveInProgressIcon from "../../../assets/icons/moveInProgressIcon";
import { SCREENS } from "../../../shared/constants";
import TimerIcon from "../../../assets/icons/timerIcon";
import fonts from "../../theme/fonts";
import DeleteIcon from "../../../assets/icons/deleteIcon";

interface WorkListCardProps {
    isCompleted?: boolean;
    onEdit?: () => void;
}

const WorkListCard: React.FC<WorkListCardProps> = (props) => {
    const { isCompleted , onEdit} = props;
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();

    return (
        <View style={{marginHorizontal: 20, paddingVertical: 10,marginVertical: 5, backgroundColor: colors.iconWhite, borderRadius: 5, elevation: 3}}>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: 10,
                justifyContent: 'space-between'
            }}>
                <Text style={{fontFamily: fonts.poppins.medium, color: colors.primary, fontSize: 16}}>DRAFT WS</Text>
                {<OutlinedButton 
                    label={isCompleted ? "Move to Inprogress" :"Mark as Completed"} 
                    style={styles.outlinedBtnStyle2} 
                    labelStyle={styles.outlinedbuttonLabel2} 
                    borderWidth={1}
                    iconLeft={isCompleted ? <MoveInProgressIcon/> : <Feather name='check-square' color={colors.primary} size={16}/>}
                />}
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center',paddingHorizontal: 10}}>
                <TextWrapper fontFamily={fonts.poppins.medium} style={{fontSize: 14}} color={colors.text}>Sachin Sanjay Devprasad Singh</TextWrapper>
                <TextWrapper style={{fontSize: 16}} color={colors.text}> VS</TextWrapper>
            </View>
            <TextWrapper fontFamily={fonts.poppins.medium} style={{fontSize: 14,paddingHorizontal: 10}} color={colors.text}>Sachin Sanjay Devprasad Singh</TextWrapper>
            <View style={{height: 10}}/>
            <View style={[styles.fdRow, {alignItems: 'center', marginStart: 10}]}>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={{flexDirection: 'row', alignItems: 'center'}}
                    style={{ fontSize: 14, marginStart: 5 }} 
                    leftIcon={<TimerIcon/>}
                >12th April 2024</TextWrapper>
                <View style={{height: 15, backgroundColor: colors.primary, width: 2, marginHorizontal: 5}}/>
                <TextWrapper 
                    color={colors.text} 
                    style={{ fontSize: 14 }} 
                >12:00 PM</TextWrapper>
            </View>
            

            {!isCompleted && <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                <RoundedButton 
                    label="Edit Info" 
                    btnType="filled" 
                    style={{height: 30, marginHorizontal: 5, flexDirection: 'row', flex: 1}} 
                    labelStyle={styles.labelStyle2} 
                    labelFont={fonts.poppins.medium}
                    iconLeft={<Feather name='edit' color={colors.background} size={14}/>}
                    onPress={() => {navigation.navigate(SCREENS.ADD_WORK)}}
                />
                <RoundedButton label="Delete" btnType="outlined" style={{height: 30, marginHorizontal: 5, flexDirection: 'row', flex: 1}} labelFont={fonts.poppins.medium} labelStyle={styles.labelStyle3} iconLeft={<DeleteIcon/>}/>
            </View>}
        </View>
    );
};

export default WorkListCard;
