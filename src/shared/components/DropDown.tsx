import { View, TextInputProps, ViewStyle, TouchableOpacity } from "react-native";
import React, { useMemo, useState } from "react";
import TextWrapper from "./text-wrapper/TextWrapper";
import createStyles from "./components.style";
import { useTheme } from "@react-navigation/native";
import DropDownIcon from "../../assets/icons/dropDownIcon";
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchBar from "./SearchBar/SearchBar";

interface InputTextProps extends TextInputProps {
    label?: string;
    backgroundColor?: string;
    style?: ViewStyle;
    disable?: boolean;
    data: string[];
    labelColor?: string;
    isSearch?: boolean;
    searchPlaceHolder?: string;
}

const DropDown: React.FC<InputTextProps> = (props) => {
    const theme = useTheme();
    const { colors } = theme;
    const { label, placeholder, value, isSearch = false, style, searchPlaceHolder, labelColor = colors.gray, disable, backgroundColor = colors.iconWhite, onBlur, data} = props;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const [open, setOpen] = useState<boolean>(false);
    const [selectedValue, setSelectedValue] = useState<string>();

    return (
        <View style={style}>
            <TouchableOpacity onPress={() => setOpen(!open)}>
                <TextWrapper 
                    color={labelColor} 
                    viewStyle={{...styles.dropDownStyle, backgroundColor: backgroundColor}}
                    rightIcon={<DropDownIcon/>}
                >{selectedValue || label}</TextWrapper>
            </TouchableOpacity>
            {open && (
                <View style={{borderColor: colors.gray, borderWidth: 1, borderRadius: 5, padding: 10}}>
                    {isSearch && <SearchBar 
                        placeholder={searchPlaceHolder} 
                        leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                        style={{ marginHorizontal: 0, height: 37, marginBottom: 5}}    
                    />}
                    {data.map((item: string, index) => (
                        <TouchableOpacity onPress={() => {
                            setSelectedValue(item);
                            setOpen(false);
                        }}>
                            <TextWrapper key={index} viewStyle={{marginVertical: 5}}>{item}</TextWrapper>
                        </TouchableOpacity>
                    ))}
                </View>
            )}
        </View>
    );
};

export default DropDown;
