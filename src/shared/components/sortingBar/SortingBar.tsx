import {
    ScrollView,
    TouchableOpacity,
    View,
} from "react-native";
import React, { useMemo, useRef, useState } from "react";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import SortingIcon from "../../../assets/icons/sortingIcon";
import FilterIcon from "../../../assets/icons/filterIcon";
import ExtractIcon from "../../../assets/icons/extractIcon";
import TextWrapper from "../../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../theme/fonts";
import RBSheet from "react-native-raw-bottom-sheet";
import SortBy from "../../../screens/sortBy/SortBy";
import Filter from "../../../screens/filterScreen/Filter";
import Extract from "../../../screens/extract/Extract";
import { SafeAreaView } from "react-native-safe-area-context";


interface SortingProps {
    children?: any;
    onPress: () => void;
}

const SortingBar: React.FC<SortingProps> = (props) => {
    const { children } = props;
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const refRBSheet = useRef(null);
    const [sheetItem, setSheetItem] = useState("SortBy")
    const [sheetHeight, setSheetHeight] = useState(500);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{flex: 1}}
            >
                {children}
            </ScrollView>

            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                height={sheetHeight}
                closeOnPressBack={true}
                animationType="fade"
                openDuration={250}
                closeDuration={250}
                onClose={() => { }}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        backgroundColor: '#fff',
                    },
                }}>
                {sheetItem == "SortBy" && <SortBy/>}
                {sheetItem == "Filter" && <Filter/>}
                {sheetItem == "Extract" && <Extract/>}

            </RBSheet>

            <View style={{ flexDirection: 'row', backgroundColor: colors.iconWhite, borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', marginVertical: 10 }}
                    onPress={() => { 
                        setSheetHeight(500)
                        setSheetItem("SortBy")
                        refRBSheet?.current?.open() }}>
                    <SortingIcon />
                    <TextWrapper style={{ fontSize: 12, marginTop: 5 }} fontFamily={fonts.poppins.medium}>Sort</TextWrapper>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', marginVertical: 10 }}
                    onPress={() => { 
                        setSheetHeight(500)
                        setSheetItem("Filter")
                        refRBSheet?.current?.open() }}>
                    <FilterIcon />
                    <TextWrapper style={{ fontSize: 12, marginTop: 5 }} fontFamily={fonts.poppins.medium}>Filter</TextWrapper>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', marginVertical: 10 }}
                    onPress={() => { 
                        setSheetHeight(230);
                        setSheetItem("Extract")
                        refRBSheet?.current?.open() }}>
                    <ExtractIcon />
                    <TextWrapper style={{ fontSize: 12, marginTop: 5 }} fontFamily={fonts.poppins.medium}>Extract</TextWrapper>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

export default SortingBar;
