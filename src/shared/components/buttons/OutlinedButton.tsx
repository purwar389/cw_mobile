import { ViewStyle, TouchableOpacity, TouchableOpacityProps, TextStyle } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import fonts from "../../theme/fonts";

interface OutlinedButtonProps extends TouchableOpacityProps {
  label: string;
  style?: ViewStyle;
  labelStyle?: TextStyle;
  labelFontFamily?: string;
  labelColor?: string;
  disable?: boolean;
  variant?: "primary" | "secondary";
  iconLeft?: React.ReactNode;
  size?: "sm" | "md";
  borderWidth?: number;
}

const OutlinedButton: React.FC<OutlinedButtonProps> = (props) => {
  const theme = useTheme();
  const { colors } = theme;
  const { style, label, variant, onPress, iconLeft, size, labelStyle, labelColor=colors.primary, labelFontFamily=fonts.poppins.regular, borderWidth = 0.5 } = props;
  const styles = useMemo(() => createStyles(theme), [theme]);
  
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.outlinedButton,
        style,
        {borderWidth: borderWidth,
          borderColor: labelColor
        }
      ]}
    >
      {iconLeft}
      <TextWrapper color={labelColor} style={labelStyle ? labelStyle : styles.filledButtonText} fontFamily={labelFontFamily}>{label}</TextWrapper>
    </TouchableOpacity>
  );
};

export default OutlinedButton;

[
  {
    id: "345678987654567",
    title: "Active Cases",
    distictCases: "51",
    highCourtCases: "13"
  },
  {
    id: "345678987654567",
    title: "Today's Cases",
    distictCases: "51",
    highCourtCases: "13"
  },
  {
    id: "345678987654567",
    title: "Tomorrow's Cases",
    distictCases: "51",
    highCourtCases: "13"
  },
  {
    id: "345678987654567",
    title: "Date Awaited Cases",
    distictCases: "51",
    highCourtCases: "13"
  },
  {
    title: "Daily Board",
    distictCases: "51",
    highCourtCases: "13"
  },
  {
    id: "345678987654567",
    title: "Archives",
    distictCases: "51",
    highCourtCases: "13"
  },
]
