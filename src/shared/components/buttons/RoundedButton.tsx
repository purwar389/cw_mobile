import React, { useMemo } from "react";
import { ViewStyle, TouchableOpacity, TouchableOpacityProps, TextStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import fonts from "../../theme/fonts";

interface RoundedButtonProps extends TouchableOpacityProps {
  label: string;
  style?: ViewStyle;
  labelStyle?: TextStyle;
  borderWidth?: number;
  disable?: boolean;
  variant?: "primary" | "secondary";
  iconLeft?: React.ReactNode;
  size?: "sm" | "md";
  btnType?: "filled" | "outlined";
  outlineColor?: string;
  labelFont?: string;
}

const RoundedButton: React.FC<RoundedButtonProps> = ({
  label,
  style,
  labelStyle,
  borderWidth = 1,
  disable = false,
  variant,
  iconLeft,
  size,
  btnType = "filled",
  onPress,
  outlineColor,
  labelFont = fonts.poppins.medium,
  ...rest
}) => {
  const { colors } = useTheme();
  const styles = useMemo(() => createStyles(useTheme()), [colors]);

  // Define button styles based on type
  const buttonStyle = btnType === "outlined" 
    ? [styles.roundedButtonOutlined, { borderColor: outlineColor || colors.primary, borderWidth }, style]
    : [styles.roundedButton, style];
  
  // Define text color based on button type
  const textColor = btnType === "outlined" ? outlineColor || colors.primary : colors.iconWhite;
  const textStyle = labelStyle || (btnType === "filled" ? styles.filledButtonText : styles.outlinedButtonText);

  return (
    <TouchableOpacity
      onPress={!disable ? onPress : undefined}
      style={buttonStyle}
      {...rest}
    >
      {iconLeft}
      <TextWrapper color={textColor} style={textStyle} fontFamily={labelFont}>
        {label}
      </TextWrapper>
    </TouchableOpacity>
  );
};

export default RoundedButton;
