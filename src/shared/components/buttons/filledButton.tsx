import { Text, TextInput, View, TextInputProps, ViewStyle, TouchableOpacity, TouchableOpacityProps, TextStyle, ActivityIndicator } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import fonts from "../../theme/fonts";

interface FilledButtonProps extends TouchableOpacityProps {
  label: string;
  labelFont: string;
  style?: ViewStyle;
  labelStyle?: TextStyle;
  disable?: boolean;
  variant?: "primary" | "secondary";
  iconLeft?: React.ReactNode;
  size?: "sm" | "md";
  isLoading?: boolean;
}

const FilledButton: React.FC<FilledButtonProps> = (props) => {
  const { style, label, variant, onPress, iconLeft, size , labelStyle, labelFont = fonts.poppins.medium, isLoading = false} = props;
  const theme = useTheme();
  const { colors } = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.filledButton,
        style,
        {
          backgroundColor: variant === "secondary" ? colors.gray : colors.primary,
        },
      ]}
    >
      {iconLeft}
      {isLoading ? <ActivityIndicator size={'small'} color={colors.white}/> : <TextWrapper fontFamily={labelFont} color={colors.iconWhite} style={labelStyle ? labelStyle : styles.filledButtonText}>{label}</TextWrapper>}
    </TouchableOpacity>
  );
};

export default FilledButton;
