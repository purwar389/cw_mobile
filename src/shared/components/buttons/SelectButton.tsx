import { ViewStyle, TouchableOpacity, TouchableOpacityProps, TextStyle } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import fonts from "../../theme/fonts";

interface SelectButtonProps extends TouchableOpacityProps {
  label: string;
  style?: ViewStyle;
  labelStyle?: TextStyle;
  disable?: boolean;
  variant?: "primary" | "secondary";
  selected : boolean;
  outlineColor?: string;
  labelFontFamily?: string;
}

const SelectButton: React.FC<SelectButtonProps> = (props) => {
  const theme = useTheme();
  const { colors } = theme;
  const { style, label, variant, onPress, selected, labelStyle,labelFontFamily=fonts.poppins.regular, outlineColor = colors.primary} = props;
  const styles = useMemo(() => createStyles(theme), [theme]);
  
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        selected ? styles.selectedButton : styles.notSelectedButton,
        style,
        {
          borderColor: outlineColor
        }
      ]}
    >
      <TextWrapper color={selected ? colors.background : outlineColor} style={labelStyle ? labelStyle : styles.filledButtonText} fontFamily={labelFontFamily}>{label}</TextWrapper>
    </TouchableOpacity>
  );
};

export default SelectButton;
