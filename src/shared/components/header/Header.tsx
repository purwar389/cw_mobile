import { View, TouchableOpacity, TouchableOpacityProps } from "react-native";
import React, { useMemo } from "react";
import TextWrapper from "../text-wrapper/TextWrapper";
import createStyles from "../components.style";
import { NavigationProp, useTheme } from "@react-navigation/native";
import BackButtonIcon from "../../../assets/icons/backButton";
interface HeaderProps extends TouchableOpacityProps {
    label: string;
    navigation: NavigationProp<ReactNavigation.RootParamList>;
}

const Header: React.FC<HeaderProps> = (props) => {
    const { label, navigation } = props;
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    
    const _handleIconPress = () => {
        navigation.goBack();
    }
  
    return (
        <View style={styles.headerContainer}>
            <TouchableOpacity style={{flex:0.5, margin: 10}}
                onPress={_handleIconPress}>
                <BackButtonIcon/>
            </TouchableOpacity>
            <TextWrapper color={colors.darkGray} style={styles.outlinedButtonText}>{label}</TextWrapper>
        </View>
    );
};

export default Header;
