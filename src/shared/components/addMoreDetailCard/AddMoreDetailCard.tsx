import { View, TextInputProps, TouchableOpacity } from "react-native";
import React, { useMemo, useState } from "react";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import TextWrapper from "../../../shared/components/text-wrapper/TextWrapper";
import { localStrings } from "../../../shared/localization";
import InputText from "../../../shared/components/InputText";
import FilledButton from "../../../shared/components/buttons/filledButton";
import AddNewIcon from "../../../assets/icons/addNewIcon";
import DropDown from "../../../shared/components/DropDown";
import fonts from "../../../shared/theme/fonts";
import DeleteIcon from "assets/icons/deleteIcon";
import DeleteIconGrey from "assets/icons/deleteIconGrey";

interface InputTextProps extends TextInputProps {
    labels: string[];
    isLabelAValue?: boolean;
    title: string;
    isDropDown?: boolean;
    isButton?: boolean;
}

const AddMoreDetailCard: React.FC<InputTextProps> = ({ labels, title, isDropDown = false, isButton = true, isLabelAValue = false }) => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    // Step 1: Create state to hold values for each InputText field (if they are editable)
    const [inputValues, setInputValues] = useState<string[]>(labels.map(() => ''));

    // Step 2: Update the specific input field value based on the index
    const onChangeText = (index: number, text: string) => {
        const updatedValues = [...inputValues];
        updatedValues[index] = text; // Update the value for the specific index
        setInputValues(updatedValues); // Update the state
    };

    return (
        <View style={styles.authCardStyle}>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 5 }}>
                <TextWrapper fontFamily={fonts.poppins.regular} viewStyle={{ marginBottom: 10, flex: 1 }} style={{ fontSize: 15 }} color={colors.darkGray}>
                    {title}
                </TextWrapper>
                {!isButton && <TouchableOpacity><DeleteIconGrey /></TouchableOpacity>}
            </View>

            {labels.map((item, index) => (
                isDropDown ? (
                    <DropDown
                        key={index}
                        data={['dfe', 'efeg']} // Replace this with actual dropdown data
                        label={title}
                        style={{ marginVertical: 5, marginHorizontal: 5 , height: 40}}
                        backgroundColor={colors.background}
                    />
                ) : (
                    <InputText
                        key={index}
                        placeholder={isLabelAValue ? '' : item} // Use label as placeholder if not treating as value
                        value={isLabelAValue ? item : inputValues[index]} // If isLabelAValue is true, use the value from labels
                        onChangeText={(text) => onChangeText(index, text)} // Update the state when user types
                        style={styles.mh_10}
                        inputStyle={{ backgroundColor: colors.background, height: 40 }}
                        keyboardType="default" // Adjust this based on your needs
                    />
                )
            ))}

            {isButton && (
                <FilledButton
                    label={localStrings.addMore!}
                    labelFont={fonts.poppins.regular}
                    iconLeft={<AddNewIcon color={colors.iconWhite} />}
                    labelStyle={{ marginHorizontal: 10, fontSize: 16 }}
                    style={{ flexDirection: 'row', marginHorizontal: 5, paddingVertical: 5 }}
                />
            )}
        </View>
    );
};

export default AddMoreDetailCard;
