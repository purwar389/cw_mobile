import { Text } from 'react-native'
import React from 'react'
import RBSheet from "react-native-raw-bottom-sheet";


interface BottomSheetProps {
    sheetRef: React.RefObject<RBSheet>;
}

const SortingSheet: React.FC<BottomSheetProps> = ({ sheetRef }) => {

    return (
        <RBSheet
            ref={sheetRef}
            closeOnDragDown={true}
            height={360}
            closeOnPressBack={true}
            animationType="fade"
            openDuration={250}
            closeDuration={250}
            onClose={() => {}}
            customStyles={{
            container: {
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                backgroundColor: '#fff',
            },
            }}>

            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
            <Text>ewfeije</Text>
        </RBSheet>
    )
}

export default SortingSheet