import { TextInput, View, TextInputProps } from "react-native";
import React, { useMemo } from "react";
import createStyles from "../components.style";
import { useTheme } from "@react-navigation/native";
import fonts from "../../theme/fonts";

interface InputTextProps extends TextInputProps {
    disable?: boolean;
    leftIcon?: React.ReactNode;
    borderWidth?: number;
}

const SearchBar: React.FC<InputTextProps> = (props) => {
    const { leftIcon, placeholder, value, onChangeText, keyboardType, disable, onFocus, borderWidth = 0.5, style } = props;
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={[styles.input, style, {flexDirection: 'row', alignItems: 'center', borderWidth: borderWidth}]}>
            {leftIcon}
            <View style={{flex: 1, marginStart: 5}}>
                <TextInput 
                    placeholder={placeholder} 
                    style={{color: colors.gray, fontFamily: fonts.poppins.regular, paddingVertical: 0}}
                    placeholderTextColor={colors.lightGray}
                />
            </View>
        </View>
    );
};

export default SearchBar;
