import React from 'react';
import {TouchableOpacity, useColorScheme, View} from 'react-native';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {isReadyRef, navigationRef} from 'react-navigation-helpers';
/**
 * ? Local & Shared Imports
 */
import {SCREENS} from '../shared/constants';
import {LightTheme, DarkTheme} from '../shared/theme/themes';
// ? Screens
import ProfileScreen from '../screens/profile/ProfileScreen';
import Login from '../screens/login/Login';
import Register from '../screens/register/Register';
import Plan from '../screens/plan/Plan';
import ForgotPassword from '../screens/forgotPassword/ForgotPassword';
import HomeScreen from '../screens/home/HomeScreen';
import BareActs from '../screens/bareActs/BareActs';
import Archives from '../screens/archives/Archives';
import ConnectedMatters from '../screens/connectedMatters/ConnectedMatters';
import AddWork from '../screens/addWork/AddWork';
import MyAccounts from '../screens/myAccounts/MyAccount';
import ResetPassword from '../screens/resetPassword/ResetPassword';
import {localStrings} from '../shared/localization';
import Support from '../screens/support/Support';
import PlanDetails from '../screens/planDetails/PlanDetails';
import AddMyClients from '../screens/addMyClients/AddMyClients';
import AddConnectedMatters from '../screens/addConnectedMatters/AddConnectedMatters';
import CaseDetail from '../screens/caseDetail/CaseDetail';
import BackButtonIcon from '../assets/icons/backButton';
import Calender from '../screens/calendar/Calender';
import LoginWithOtp from '../screens/loginWithOtp/LoginWithOtp';
import OtpScreen from '../screens/otpScreen/OtpScreen';
import AddNote from '../screens/addNote/AddNote';
import NotificationIcon from '../assets/icons/notificationIcon';
import MenuIcon from '../assets/icons/menuIcon';
import TellUsWhoUR from '../screens/tellUsWhoUR/TellUsWhoUR';
import ContactUs from '../screens/contactUs/ContactUs';
import Splash from '../screens/splash/Splash';

// ? If you want to use stack or tab or both
const Stack = createStackNavigator();

const Navigation = () => {
  const scheme = useColorScheme();
  const isDarkMode = scheme === 'dark';

  React.useEffect((): any => {
    return () => (isReadyRef.current = false);
  }, []);

  interface Options {
    headerShown: boolean;
  }

  const options: Options = {headerShown: false};
  const options2 = (title: string): StackNavigationOptions => ({
    title,
    headerTitleAlign: 'center',
    headerTitleStyle: {fontSize: 20},
    headerLeft: () => (
      <TouchableOpacity
        style={{marginHorizontal: 10}}
        onPress={() => navigationRef.current?.goBack()}>
        <BackButtonIcon />
      </TouchableOpacity>
    ),
    headerRight: () => (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TouchableOpacity
          style={{marginHorizontal: 10}}
          onPress={() => {
            navigationRef.navigate(SCREENS.HOME, {
              screen: localStrings.notifications,
            });
          }}>
          <NotificationIcon />
        </TouchableOpacity>
        <TouchableOpacity style={{marginHorizontal: 10}} onPress={() => {}}>
          <MenuIcon />
        </TouchableOpacity>
      </View>
    ),
  });
  const optionsDailyBoard = (title: string): StackNavigationOptions => ({
    title,
    headerTitleAlign: 'center',
    headerTitleStyle: {fontSize: 20},
    headerLeft: () => (
      <TouchableOpacity
        style={{marginHorizontal: 10}}
        onPress={() => navigationRef.current?.goBack()}>
        <BackButtonIcon />
      </TouchableOpacity>
    ),
  });

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}
      theme={isDarkMode ? DarkTheme : LightTheme}>
      <Stack.Navigator initialRouteName={SCREENS.SPLASH}>
        <Stack.Screen
          name={SCREENS.LOGIN_WITH_OTP}
          component={LoginWithOtp}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.LOGIN}
          component={Login}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.OTP_SCREEN}
          component={OtpScreen}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.REGISTER}
          component={Register}
          options={options}
        />
        <Stack.Screen name={SCREENS.PLAN} component={Plan} options={options} />
        <Stack.Screen
          name={SCREENS.FORGOT_PASSWORD}
          component={ForgotPassword}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.HOME}
          component={HomeScreen}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.BAREACTS}
          component={BareActs}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.ARCHIVES}
          component={Archives}
          options={options}
        />
        {/* <Stack.Screen name={SCREENS.ACTIVE_CASES} component={ActiveCases} options={options} /> */}
        <Stack.Screen
          name={SCREENS.CONNECTED_MATTERS}
          component={ConnectedMatters}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.ADD_WORK}
          component={AddWork}
          options={() => options2(localStrings.addAWork!)}
        />
        <Stack.Screen
          name={SCREENS.MY_ACCOUNTS}
          component={MyAccounts}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.PROFILE}
          component={ProfileScreen}
          options={() => options2(localStrings.profile!)}
        />
        <Stack.Screen
          name={SCREENS.RESET_PASSWORD}
          component={ResetPassword}
          options={() => options2(localStrings.resetPassword!)}
        />
        <Stack.Screen
          name={SCREENS.SUPPORT}
          component={Support}
          options={() => options2(localStrings.support!)}
        />
        <Stack.Screen
          name={SCREENS.PLAN_DETAILS}
          component={PlanDetails}
          options={() => options2(localStrings.planDetails!)}
        />
        <Stack.Screen
          name={SCREENS.ADD_MY_CLIENTS}
          component={AddMyClients}
          options={() => options2(localStrings.myClients!)}
        />
        <Stack.Screen
          name={SCREENS.ADD_CONNECTED_MATTERS}
          component={AddConnectedMatters}
          options={() => options2(localStrings.connectedMatters!)}
        />
        <Stack.Screen
          name={SCREENS.CASE_DETAIL}
          component={CaseDetail}
          options={() => options2(localStrings.caseDetail!)}
        />
        <Stack.Screen
          name={SCREENS.CALENDAR}
          component={Calender}
          options={() => options2(localStrings.selectADate!)}
        />
        <Stack.Screen
          name={SCREENS.ADD_NOTE}
          component={AddNote}
          options={() => options2(localStrings.notes!)}
        />
        <Stack.Screen
          name={SCREENS.TELL_US_WHO_U_R}
          component={TellUsWhoUR}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.CONTACT_US}
          component={ContactUs}
          options={options}
        />
        <Stack.Screen
          name={SCREENS.SPLASH}
          component={Splash}
          options={options}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
