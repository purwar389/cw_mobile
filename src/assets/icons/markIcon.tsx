import * as React from "react"
import Svg, { Path } from "react-native-svg"

function MarkIcon(props) {
  return (
    <Svg
      width={13}
      height={10}
      viewBox="0 0 13 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 5l3.5 3.5 7-7.5"
        stroke="#5D595E"
        strokeOpacity={0.5}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default MarkIcon
