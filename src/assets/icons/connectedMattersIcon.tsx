import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ConnectedMattersIcon({color = "#352F36", ...props}) {
  return (
    <Svg
      width={22}
      height={19}
      viewBox="0 0 22 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M20.998 5.925a4.764 4.764 0 01-1.4 3.241l-3.158 3.159a4.751 4.751 0 01-3.384 1.4h-.004a4.782 4.782 0 01-4.778-4.917.545.545 0 01.545-.53h.016a.545.545 0 01.53.56 3.692 3.692 0 006.299 2.715l3.158-3.157a3.693 3.693 0 00-5.22-5.223l-1 1a.546.546 0 11-.77-.772l.999-1A4.782 4.782 0 0120.66 4.02a4.8 4.8 0 01.338 1.906zM9.4 14.915l-1 .999a3.668 3.668 0 01-2.616 1.083 3.693 3.693 0 01-2.608-6.304l3.156-3.157a3.693 3.693 0 016.305 2.715.546.546 0 00.53.56h.016a.546.546 0 00.545-.531A4.783 4.783 0 005.56 6.765L2.4 9.922a4.784 4.784 0 005.212 7.804 4.75 4.75 0 001.55-1.04l1-1a.547.547 0 00-.385-.933.545.545 0 00-.386.163l.008-.002z"
        fill={color}
        stroke={color}
        strokeWidth={0.3}
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ConnectedMattersIcon
