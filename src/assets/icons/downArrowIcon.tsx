import * as React from "react"
import Svg, { Path, SvgProps } from "react-native-svg"

interface DownArrowIconProps extends SvgProps {
  color?: string;
}

const DownArrowIcon: React.FC<DownArrowIconProps> = ({ color = "#5D595E", ...props }) => {
  return (
    <Svg
      width={19}
      height={10}
      viewBox="0 0 19 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1.602.269a.95.95 0 00-1.52 1.08.952.952 0 00.215.303L8.842 9.74a.948.948 0 001.305 0l8.546-8.088A.951.951 0 1017.389.27l-7.894 7.47L1.602.27z"
        fill={color}
      />
    </Svg>
  )
}

export default DownArrowIcon
