import * as React from "react"
import Svg, { Path } from "react-native-svg"

function AddAWorkIcon(props) {
  return (
    <Svg
      width={24}
      height={21}
      viewBox="0 0 24 21"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 2.44l1.44 1.44L5.32 1M1 9.16l1.44 1.44 2.88-2.88M1 15.881l1.44 1.44 2.88-2.88m3.36-5.28h10.561m-10.56 6.72h5.405M8.681 2.441h10.56"
        stroke="#A40E08"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M19.637 11.31a4.362 4.362 0 100 8.725 4.362 4.362 0 000-8.725zm2.181 4.799h-1.744v1.745H19.2v-1.745h-1.745v-.873h1.745v-1.744h.873v1.744h1.744v.873z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default AddAWorkIcon
