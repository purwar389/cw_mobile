import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DocIcon(props) {
  return (
    <Svg
      width={19}
      height={22}
      viewBox="0 0 19 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 3.833v14.16"
        stroke="#A40E08"
        strokeWidth={1.24937}
        strokeLinecap="round"
      />
      <Path
        d="M18 8.083V18M3.833 1h7.084"
        stroke="#A40E08"
        strokeWidth={1.24937}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M3.833 20.833h11.334"
        stroke="#A40E08"
        strokeWidth={1.24937}
        strokeLinecap="round"
      />
      <Path
        d="M18 18c.008 1.415-1.417 2.833-2.834 2.833M1 18c0 1.416 1.417 2.833 2.833 2.833M1 3.831c0-1.414 1.417-2.8 2.833-2.83M17.992 8.09L10.918 1M10.917 5.25c.004 1.409 1.422 2.833 2.833 2.833M10.917 5.25V1M13.75 8.083H18"
        stroke="#A40E08"
        strokeWidth={1.24937}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M3.833 18h4.25M3.833 15.166h7.084M3.833 12.333h4.25"
        stroke="#A40E08"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default DocIcon
