import * as React from "react"
import Svg, { Path } from "react-native-svg"

function UsersIcon(props) {
  return (
    <Svg
      width={18}
      height={13}
      viewBox="0 0 18 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M14.214 3.571c-.104 1.453-1.182 2.572-2.357 2.572-1.175 0-2.254-1.119-2.357-2.572C9.393 2.061 10.442 1 11.857 1c1.416 0 2.465 1.088 2.357 2.571z"
        stroke="#A40E08"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M11.857 8.428c-2.327 0-4.565 1.156-5.126 3.408-.074.297.113.592.419.592h9.415c.307 0 .492-.295.42-.592-.562-2.288-2.8-3.408-5.128-3.408z"
        stroke="#A40E08"
        strokeMiterlimit={10}
      />
      <Path
        d="M7 4.212c-.084 1.16-.954 2.074-1.893 2.074-.938 0-1.81-.913-1.893-2.074-.085-1.206.763-2.069 1.893-2.069S7.085 3.028 7 4.213z"
        stroke="#A40E08"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M7.214 8.5c-.644-.296-1.354-.41-2.107-.41-1.857 0-3.646.924-4.094 2.722-.06.238.09.473.334.473h4.01"
        stroke="#A40E08"
        strokeMiterlimit={10}
        strokeLinecap="round"
      />
    </Svg>
  )
}

export default UsersIcon
