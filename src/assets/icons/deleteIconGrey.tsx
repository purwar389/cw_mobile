import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DeleteIconGrey(props) {
  return (
    <Svg
      width={16}
      height={18}
      viewBox="0 0 18 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 5h15.75M6.906 9v6m3.938-6v6M1.984 5l.985 12c0 .53.207 1.04.576 1.414.37.375.87.586 1.393.586h7.875c.522 0 1.022-.21 1.392-.586.369-.375.576-.884.576-1.414l.985-12M5.922 5V2c0-.265.104-.52.288-.707A.977.977 0 016.906 1h3.938c.26 0 .511.105.696.293.184.187.288.442.288.707v3"
        stroke="#5D595E"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default DeleteIconGrey
