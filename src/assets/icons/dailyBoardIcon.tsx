import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DailyBoardIcon(props) {
  return (
    <Svg
      width={19}
      height={22}
      viewBox="0 0 19 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M5.004 2.5c-1.556.047-2.483.22-3.125.862C1 4.242 1 5.657 1 8.488v6.506c0 2.832 0 4.247.879 5.127C2.757 21 4.172 21 7 21h5c2.829 0 4.243 0 5.121-.88.88-.879.88-2.294.88-5.126V8.488c0-2.83 0-4.246-.88-5.126-.641-.642-1.569-.815-3.125-.862"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M11.004 10h3.5m-10 1s.5 0 1 1c0 0 1.588-2.5 3-3m2.5 7h3.5m-9 0h1M5 2.75C5 1.784 5.784 1 6.75 1h5.5a1.75 1.75 0 110 3.5h-5.5A1.75 1.75 0 015 2.75z"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default DailyBoardIcon
