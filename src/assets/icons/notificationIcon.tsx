import * as React from "react";
import Svg, { Path, SvgProps } from "react-native-svg";

interface NotificationIconProps extends SvgProps {
  width?: number;
  height?: number;
}

const NotificationIcon: React.FC<NotificationIconProps> = ({
  width = 20,
  height = 27,
  ...props
}) => {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 23 30"
      fill="none"
      {...props}
    >
      <Path
        d="M3.885 22.673V9.693a8.654 8.654 0 1117.307 0v12.98m-17.307 0h17.307m-17.307 0H1m20.192 0h2.885M11.097 27h2.884"
        stroke="#5D595E"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};

export default NotificationIcon;
