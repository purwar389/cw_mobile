import * as React from "react"
import Svg, { Path } from "react-native-svg"

function TarajuIcon(props) {
  return (
    <Svg
      width={20}
      height={18}
      viewBox="0 0 20 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M7.538 10.75L4.27 3.6 1 10.75m6.538 0a3.24 3.24 0 01-.957 2.298 3.28 3.28 0 01-4.623 0A3.24 3.24 0 011 10.75m6.538 0H1m17 0L14.73 3.6l-3.269 7.15m6.539 0a3.24 3.24 0 01-.957 2.298 3.28 3.28 0 01-4.624 0 3.24 3.24 0 01-.958-2.298m6.539 0h-6.539M2.308 3.6h14.384M9.5 3.6V1"
        stroke="#A40E08"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default TarajuIcon
