import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DescendingIcon(props) {
  return (
    <Svg
      width={11}
      height={13}
      viewBox="0 0 11 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M10.243 6.38a.875.875 0 00-1.238 0l-2.88 2.882V.875a.875.875 0 10-1.75 0v8.387L1.495 6.38A.875.875 0 00.255 7.618l4.375 4.375a.873.873 0 001.238 0l4.374-4.375a.875.875 0 000-1.238z"
        fill="#9A9A9A"
      />
    </Svg>
  )
}

export default DescendingIcon
