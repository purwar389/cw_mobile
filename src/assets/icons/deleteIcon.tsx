import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DeleteIcon(props) {
  return (
    <Svg
      width={14.2}
      height={15.975}
      viewBox="0 0 16 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 4.556H14.867M6.2 8.111V13.444M9.667 8.111V13.444M1.867 4.556L2.733 15.222C2.733 15.694 2.916 16.146 3.241 16.479C3.566 16.813 4.007 17 4.467 17H11.4C11.86 17 12.301 16.813 12.626 16.479C12.951 16.146 13.133 15.694 13.133 15.222L14 4.556M5.333 4.556V1.889C5.333 1.653 5.425 1.427 5.587 1.26C5.75 1.094 5.97 1 6.2 1H9.667C9.897 1 10.117 1.094 10.28 1.26C10.442 1.427 10.533 1.653 10.533 1.889V4.556"
        stroke="#A40E08"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1}
      />
    </Svg>
  )
}

export default DeleteIcon
