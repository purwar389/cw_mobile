import React from 'react';
import Svg, { Path } from 'react-native-svg';

const EmailSentIcon = () => (
  <Svg 
    width="42.8" 
    height="29.484444" 
    viewBox="0 0 45 31" 
    fill="none"
  >
    {/* First Path */}
    <Path 
      d="M17.289,9.144L26.452,15.252L35.615,9.144M5.072,18.306H9.145M1,12.198H9.145"
      stroke="#ffffff" 
      strokeWidth="2" 
      strokeLinecap="round" 
      strokeLinejoin="round"
    />
    
    {/* Second Path */}
    <Path 
      d="M9.144,6.09V5.072C9.144,3.992 9.573,2.956 10.336,2.193C11.1,1.429 12.136,1 13.216,1H39.686C40.766,1 41.802,1.429 42.566,2.193C43.329,2.956 43.758,3.992 43.758,5.072V25.434C43.758,26.514 43.329,27.55 42.566,28.314C41.802,29.077 40.766,29.506 39.686,29.506H13.216C12.136,29.506 11.1,29.077 10.336,28.314C9.573,27.55 9.144,26.514 9.144,25.434V24.416"
      stroke="#ffffff" 
      strokeWidth="2" 
      strokeLinecap="round" 
      fill="none"
    />
  </Svg>
);

export default EmailSentIcon;
