import * as React from "react"
import Svg, { Path } from "react-native-svg"

function RightIcon(props) {
  return (
    <Svg
      width={15}
      height={10}
      viewBox="0 0 15 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M13.875.393a1.466 1.466 0 00-1.997 0L5.226 6.75 2.408 4.057a1.466 1.466 0 00-1.997 0c-.548.523-.548 1.37 0 1.91l3.833 3.648c.274.262.628.385.982.385.37 0 .725-.123.999-.385l7.65-7.313c.548-.524.548-1.37 0-1.91z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default RightIcon
