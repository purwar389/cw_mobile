import * as React from "react"
import Svg, { Path } from "react-native-svg"

function LocationIcon(props) {
  return (
    <Svg
      width={15}
      height={17}
      viewBox="0 0 15 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M10.286 7.5a2.785 2.785 0 11-5.571 0 2.785 2.785 0 015.57 0zm-.929 0a1.857 1.857 0 10-3.714 0 1.857 1.857 0 003.714 0zm2.74 4.6A6.504 6.504 0 007.5 1 6.5 6.5 0 001 7.502c0 1.724.685 3.378 1.904 4.598l1.412 1.392 1.897 1.843.124.11c.72.582 1.773.545 2.45-.11l2.262-2.2 1.047-1.035zM3.557 3.559a5.571 5.571 0 019.514 3.825 5.58 5.58 0 01-1.467 3.89l-.164.171-1.226 1.211-2.07 2.014-.088.074a.93.93 0 01-1.114 0l-.086-.075-2.767-2.697-.532-.527-.163-.17a5.58 5.58 0 01.163-7.716z"
        fill="#A40E08"
        stroke="#A40E08"
        strokeWidth={0.5}
      />
    </Svg>
  )
}

export default LocationIcon
