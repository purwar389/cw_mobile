import * as React from "react"
import Svg, { Path } from "react-native-svg"

function FilterIcon(props) {
  return (
    <Svg
      width={28}
      height={27}
      viewBox="0 0 28 27"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M2.41 1h22.576a1.411 1.411 0 011.41 1.41V4.65c0 .374-.148.733-.413.997l-9.05 9.05a1.41 1.41 0 00-.413.997v8.896a1.41 1.41 0 01-1.754 1.369l-2.822-.706a1.411 1.411 0 01-1.068-1.368v-8.19a1.41 1.41 0 00-.413-.998l-9.05-9.05A1.41 1.41 0 011 4.65V2.41A1.41 1.41 0 012.41 1z"
        stroke="#5D595E"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default FilterIcon
