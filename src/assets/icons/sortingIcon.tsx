import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SortingIcon(props) {
  return (
    <Svg
      width={24}
      height={27}
      viewBox="0 0 24 27"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M5.688 26V10.375m0 15.625L1 21.312M5.688 26l4.687-4.688M18.188 1v15.625m0-15.625l4.687 4.688M18.187 1L13.5 5.688"
        stroke="#5D595E"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default SortingIcon
