import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ActiveCasesIcon(props) {
  return (
    <Svg
      width={19}
      height={20}
      viewBox="0 0 21 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 1h9.474v16.842H1V1zm9.474 3.158h9.473V21h-9.473V4.158z"
        stroke="#fff"
        strokeWidth={1.66667}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M4.685 7.315L6.79 9.31l-1.93 2.216m9.298-1.053l2.106 1.995-1.93 2.216"
        stroke="#fff"
        strokeWidth={1.66667}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ActiveCasesIcon
