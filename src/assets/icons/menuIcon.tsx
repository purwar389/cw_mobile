import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"

function MenuIcon(props) {
  return (
    <Svg
      width={32}
      height={32}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle cx={16} cy={16} r={15} fill="#fff" stroke="#A40E08" />
      <Path
        d="M10 10h12.046M10 16.115h12.046M10 22.23h12.046"
        stroke="#A40E08"
        strokeWidth={1.5}
        strokeLinecap="round"
      />
    </Svg>
  )
}

export default MenuIcon
