import * as React from "react"
import Svg, { Path } from "react-native-svg"

function RespondentIcon(props) {
  return (
    <Svg
      width={16}
      height={14}
      viewBox="0 0 16 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M14 2.333h-2.917V1.75A1.75 1.75 0 009.333 0h-3.5a1.75 1.75 0 00-1.75 1.75v.583H1.167A1.167 1.167 0 000 3.5v9.333A1.167 1.167 0 001.167 14H14a1.167 1.167 0 001.167-1.167V3.5A1.167 1.167 0 0014 2.333zM5.25 1.75a.583.583 0 01.583-.583h3.5a.583.583 0 01.584.583v.583H5.25V1.75zM14 3.5v3.034a13.422 13.422 0 01-12.833 0V3.5H14zm0 9.333H1.167V7.85A14.597 14.597 0 0014 7.848v4.985zM5.833 6.417a.583.583 0 01.584-.584H8.75A.583.583 0 018.75 7H6.417a.583.583 0 01-.584-.583z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default RespondentIcon
