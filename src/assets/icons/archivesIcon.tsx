import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ArchivesIcon(props) {
  return (
    <Svg
      width={24}
      height={20}
      viewBox="0 0 24 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M9.167 10a.833.833 0 100 1.667h5a.833.833 0 000-1.667h-5zM0 2.5A2.5 2.5 0 012.5 0h18.333a2.5 2.5 0 012.5 2.5v1.667a2.5 2.5 0 01-1.666 2.358v9.308A4.167 4.167 0 0117.5 20H5.833a4.167 4.167 0 01-4.166-4.167V6.525A2.5 2.5 0 010 4.167V2.5zm2.5-.833a.833.833 0 00-.833.833v1.667A.833.833 0 002.5 5h18.333a.833.833 0 00.834-.833V2.5a.833.833 0 00-.834-.833H2.5zm.833 5v9.166a2.5 2.5 0 002.5 2.5H17.5a2.5 2.5 0 002.5-2.5V6.667H3.333z"
        fill="#fff"
      />
    </Svg>
  )
}

export default ArchivesIcon
