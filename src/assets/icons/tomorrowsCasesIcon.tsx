import * as React from "react"
import Svg, { Path } from "react-native-svg"

function TomorrowsCasesIcon(props) {
  return (
    <Svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M13.847 16.05a1.584 1.584 0 100-3.168 1.584 1.584 0 000 3.168z"
        fill="#fff"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.942 1.058A3.611 3.611 0 0120 3.61v12.778A3.611 3.611 0 0116.389 20H3.611a3.603 3.603 0 01-2.553-1.058A3.611 3.611 0 010 16.39V3.611A3.61 3.61 0 013.611 0h12.778c.958 0 1.876.38 2.553 1.058zM16.39 18.333a1.944 1.944 0 001.944-1.944V6.111H1.667v10.278a1.944 1.944 0 001.944 1.944h12.778zm0-16.666H3.611A1.945 1.945 0 001.667 3.61v.834h16.666V3.61a1.945 1.945 0 00-1.944-1.944z"
        fill="#fff"
      />
    </Svg>
  )
}

export default TomorrowsCasesIcon
