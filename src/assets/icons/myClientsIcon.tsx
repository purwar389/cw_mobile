import * as React from "react"
import Svg, { Path } from "react-native-svg"

function MyClientsIcon({color="#352F36", ...props}) {
  return (
    <Svg
      width={25}
      height={19}
      viewBox="0 0 25 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M24.051 10.754a.591.591 0 01-.828-.118 5.288 5.288 0 00-4.264-2.132.592.592 0 110-1.184 2.566 2.566 0 10-2.484-3.207.592.592 0 11-1.147-.296 3.75 3.75 0 115.912 3.91 6.483 6.483 0 012.93 2.198.593.593 0 01-.119.83zm-5.369 6.335a.595.595 0 01-.216.81.582.582 0 01-.592 0 .593.593 0 01-.217-.217 5.822 5.822 0 00-10.028 0 .592.592 0 11-1.024-.592 6.917 6.917 0 013.572-3.005 4.54 4.54 0 114.944 0 6.918 6.918 0 013.561 3.004zm-6.038-3.453a3.355 3.355 0 100-6.71 3.355 3.355 0 000 6.71zM6.921 7.912a.592.592 0 00-.592-.592 2.566 2.566 0 112.484-3.208.592.592 0 101.147-.296 3.75 3.75 0 10-5.912 3.91 6.483 6.483 0 00-2.93 2.2.592.592 0 10.948.71 5.287 5.287 0 014.263-2.132.592.592 0 00.592-.592z"
        fill={color}
        stroke={color}
        strokeWidth={0.2}
      />
    </Svg>
  )
}

export default MyClientsIcon
