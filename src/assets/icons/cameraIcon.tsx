import * as React from "react"
import Svg, { Path } from "react-native-svg"

function CameraIcon(props) {
  return (
    <Svg
      width={17}
      height={15}
      viewBox="0 0 17 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M14.95 2.18h-2.326L11.474.458a.484.484 0 00-.404-.216H5.899a.485.485 0 00-.405.216L4.345 2.181H2.02A1.778 1.778 0 00.241 3.959v9.052a1.778 1.778 0 001.778 1.778H14.95a1.778 1.778 0 001.778-1.778V3.959A1.778 1.778 0 0014.95 2.18zm.809 10.83a.808.808 0 01-.809.809H2.02a.808.808 0 01-.809-.808V3.959a.808.808 0 01.808-.808h2.586a.485.485 0 00.405-.216L6.158 1.21h4.653l1.149 1.724a.484.484 0 00.404.216h2.586a.808.808 0 01.809.808v9.052zM8.485 4.768a3.394 3.394 0 100 6.789 3.394 3.394 0 000-6.789zm0 5.819a2.425 2.425 0 110-4.85 2.425 2.425 0 010 4.85z"
        fill="#fff"
      />
    </Svg>
  )
}

export default CameraIcon
