import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ArchiveMenuIcon({ color = "#352F36", ...props}) {
  return (
    <Svg
      width={22}
      height={22}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M21 11H1m13.333-5H7.667m6.666 10H7.667M12.11 1H9.89c-4.19 0-6.286 0-7.587 1.172C1.002 3.344 1 5.229 1 9v4c0 3.771 0 5.657 1.302 6.828C3.604 20.999 5.7 21 9.89 21h2.222c4.19 0 6.286 0 7.587-1.172C20.998 18.656 21 16.771 21 13V9c0-3.771 0-5.657-1.302-6.828C18.396 1.001 16.3 1 12.11 1z"
        stroke={color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ArchiveMenuIcon