import * as React from "react"
import Svg, { Path } from "react-native-svg"

function CsvIcon(props) {
  return (
    <Svg
      width={15}
      height={20}
      viewBox="0 0 15 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M15 17.5V5.625L9.375 0H2.5A2.5 2.5 0 000 2.5v15A2.5 2.5 0 002.5 20h10a2.5 2.5 0 002.5-2.5zM9.375 3.75a1.875 1.875 0 001.875 1.875h2.5v5.625H1.25V2.5A1.25 1.25 0 012.5 1.25h6.875v2.5zM1.25 15v-2.5h2.5V15h-2.5zm0 1.25h2.5v2.5H2.5a1.25 1.25 0 01-1.25-1.25v-1.25zM5 18.75v-2.5h3.75v2.5H5zm5 0v-2.5h3.75v1.25a1.25 1.25 0 01-1.25 1.25H10zM13.75 15H10v-2.5h3.75V15zM5 15v-2.5h3.75V15H5z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default CsvIcon
