import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ShareIcon(props) {
  return (
    <Svg
      width={19}
      height={22}
      viewBox="0 0 19 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M6.556 11A2.778 2.778 0 111 11a2.778 2.778 0 015.556 0z"
        stroke="#A40E08"
        strokeWidth={1.25}
      />
      <Path
        d="M12.11 4.889L6.556 8.778m5.556 8.333l-5.556-3.889"
        stroke="#A40E08"
        strokeWidth={1.25}
        strokeLinecap="round"
      />
      <Path
        d="M17.667 18.222a2.778 2.778 0 11-5.556 0 2.778 2.778 0 015.556 0zm0-14.444a2.778 2.778 0 11-5.556 0 2.778 2.778 0 015.556 0z"
        stroke="#A40E08"
        strokeWidth={1.25}
      />
    </Svg>
  )
}

export default ShareIcon
