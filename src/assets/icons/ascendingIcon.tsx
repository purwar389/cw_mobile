import * as React from "react"
import Svg, { Path } from "react-native-svg"

function AscendingIcon(props) {
  return (
    <Svg
      width={11}
      height={13}
      viewBox="0 0 11 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M.257 6.62a.875.875 0 001.238 0l2.88-2.882v8.387a.875.875 0 101.75 0V3.738l2.88 2.882a.875.875 0 001.239-1.238L5.869 1.007a.875.875 0 00-1.238 0L.257 5.382a.875.875 0 000 1.238z"
        fill="#9A9A9A"
      />
    </Svg>
  )
}

export default AscendingIcon
