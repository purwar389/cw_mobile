import * as React from "react"
import Svg, { Rect } from "react-native-svg"

function BareActsIcon({ color = "#352F36", ...props }) {
    return (
        <Svg
            width={24}
            height={22}
            viewBox="0 0 24 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <Rect
                x={1}
                y={1.59082}
                width={5.74796}
                height={19.1599}
                rx={2}
                stroke={color}
                strokeWidth={1.5}
                strokeMiterlimit={1.41421}
                strokeLinejoin="round"
            />
            <Rect
                x={6.74805}
                y={1.59082}
                width={5.74796}
                height={19.1599}
                rx={2}
                stroke={color}
                strokeWidth={1.5}
                strokeMiterlimit={1.41421}
                strokeLinejoin="round"
            />
            <Rect
                x={12.4958}
                y={2.54883}
                width={5.74796}
                height={19.1599}
                rx={2}
                transform="rotate(-15.631 12.496 2.549)"
                stroke={color}
                strokeWidth={1.5}
                strokeMiterlimit={1.41421}
                strokeLinejoin="round"
            />
        </Svg>
    )
}

export default BareActsIcon
