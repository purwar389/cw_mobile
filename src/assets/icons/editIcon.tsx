import * as React from "react"
import Svg, { Path } from "react-native-svg"

function EditIcon(props) {
  return (
    <Svg
      width={15}
      height={15}
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M8.106 1.787H2.579C2.16 1.787 1.759 1.953 1.463 2.249C1.166 2.545 1 2.947 1 3.366V14.42C1 14.839 1.166 15.241 1.463 15.537C1.759 15.833 2.16 16 2.579 16H13.634C14.053 16 14.454 15.833 14.75 15.537C15.047 15.241 15.213 14.839 15.213 14.42V8.893"
        stroke="#ffffff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1}
      />
      <Path
        d="M13.14 1.491C13.455 1.176 13.881 1 14.325 1C14.769 1 15.195 1.176 15.509 1.491C15.823 1.805 16 2.231 16 2.675C16 3.119 15.823 3.545 15.509 3.859L8.107 11.262L4.948 12.052L5.738 8.893L13.14 1.491Z"
        stroke="#ffffff"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={1}
      />
    </Svg>
  )
}

export default EditIcon
