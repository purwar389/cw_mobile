import * as React from "react"
import Svg, { Path } from "react-native-svg"

function TimerIcon(props) {
  return (
    <Svg
      width={10}
      height={13}
      viewBox="0 0 10 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M0 0v1.083h1.083V3.25A3.792 3.792 0 002.928 6.5a3.792 3.792 0 00-1.845 3.25v2.167H0V13h9.75v-1.083H8.667V9.75A3.792 3.792 0 006.822 6.5a3.792 3.792 0 001.845-3.25V1.083H9.75V0H0zm2.167 1.083h5.416V3.25a2.7 2.7 0 01-2.708 2.708A2.7 2.7 0 012.167 3.25V1.083zm2.708 5.959A2.7 2.7 0 017.583 9.75v2.167h-.541v-1.084a2.166 2.166 0 10-4.334 0v1.084h-.541V9.75a2.7 2.7 0 012.708-2.708z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default TimerIcon