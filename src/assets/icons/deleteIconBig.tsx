import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DeleteIconBig(props) {
  return (
    <Svg
      width={38}
      height={42}
      viewBox="0 0 38 42"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 9.889h35.238m-24.146 8.889l1.273 13.333m12.047-13.333L24.138 32.11M3.202 9.89l2.203 26.667c0 1.178.464 2.309 1.29 3.142A4.385 4.385 0 009.81 41h17.619a4.385 4.385 0 003.114-1.302 4.465 4.465 0 001.29-3.142l2.203-26.667m-22.024 0V3.222c0-.59.232-1.154.645-1.571.413-.417.973-.651 1.557-.651h8.81c.584 0 1.144.234 1.557.65.413.418.645.983.645 1.572V9.89m-6.395 8.889V32.11"
        stroke="#fff"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default DeleteIconBig
