import * as React from "react"
import Svg, { Path } from "react-native-svg"

function WorkListIcon({color="#352F36", ...props}) {
  return (
    <Svg
      width={22}
      height={20}
      viewBox="0 0 22 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 2.579l1.579 1.579L5.737 1M1 9.947l1.579 1.58 3.158-3.159M1 17.316l1.579 1.579 3.158-3.158m3.684-5.79H21M9.421 17.316H21M9.421 2.579H21"
        stroke={color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default WorkListIcon
