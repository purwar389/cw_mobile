import React from 'react';
import Svg, { Path } from 'react-native-svg';

const TrashIcon = ({ width = 14, height = 15.75, fillColor = 'none', strokeColor = '#5D595E', strokeWidth = 1 }) => (
  <Svg width={width} height={height} viewBox="0 0 16 18" fill={fillColor}>
    <Path
      d="M1 4.556h14M6.25 8.111v5.333M9.75 8.111v5.333M1.875 4.556L2.75 15.222c0 .472.184.924.513 1.257.328.334.773.521 1.237.521h7c.464 0 .909-.187 1.237-.521.329-.333.513-.785.513-1.257L14.125 4.556M5.375 4.556V1.889c0-.236.092-.462.256-.629.164-.166.387-.26.619-.26h3.5c.232 0 .455.094.619.26.164.167.256.393.256.629V4.556"
      stroke={strokeColor}
      strokeWidth={strokeWidth}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </Svg>
);

export default TrashIcon;
