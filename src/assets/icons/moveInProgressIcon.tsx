import * as React from "react"
import Svg, { Path } from "react-native-svg"

function MoveInProgressIcon(props) {
  return (
    <Svg
      width={15}
      height={15}
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M6.092 14a6.666 6.666 0 01-1.836-.718M9.054 1a6.668 6.668 0 010 13m-6.976-2.728a6.664 6.664 0 01-.91-1.92M1 6.39c.118-.703.347-1.37.667-1.98l.125-.227m2.01-2.178A6.665 6.665 0 016.092 1"
        stroke="#A40E08"
        strokeWidth={1.2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M5.352 7.5l1.48 1.481L9.796 6.02"
        stroke="#A40E08"
        strokeWidth={1.2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default MoveInProgressIcon
