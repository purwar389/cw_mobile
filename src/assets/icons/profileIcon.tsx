import * as React from "react"
import Svg, { Path } from "react-native-svg"

function ProfileIcon(props) {
  return (
    <Svg
      width={26}
      height={20}
      viewBox="0 0 28 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 11c0-4.714 0-7.071 1.903-8.536C4.81 1 7.872 1 14 1c6.128 0 9.192 0 11.096 1.464C26.998 3.93 27 6.287 27 11c0 4.713 0 7.071-1.904 8.536C23.194 21 20.128 21 14 21s-9.192 0-11.097-1.464C1 18.07 1 15.713 1 11z"
        stroke="#352F36"
        strokeWidth={1.7}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M6.2 15.445h5.2m5.2-8.89h5.2M16.6 11h5.2m-5.2 4.445h5.2M9.32 6.555H8.28c-.98 0-1.47 0-1.776.26-.304.263-.304.68-.304 1.518v.89c0 .837 0 1.256.304 1.517.307.26.796.26 1.776.26h1.04c.98 0 1.47 0 1.776-.26.304-.262.304-.68.304-1.518v-.889c0-.837 0-1.256-.304-1.517-.307-.26-.796-.26-1.776-.26z"
        stroke="#352F36"
        strokeWidth={1.7}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default ProfileIcon
