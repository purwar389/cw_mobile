import * as React from "react"
import Svg, { Path } from "react-native-svg"

function MailIcon(props) {
  return (
    <Svg
      width={15}
      height={15}
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M2.48 3.34l3.699-2.002a2.818 2.818 0 012.677 0l3.7 2.001a2.818 2.818 0 011.48 2.473v5.37A2.819 2.819 0 0111.215 14H3.819A2.818 2.818 0 011 11.182v-5.37A2.818 2.818 0 012.48 3.34z"
        stroke="#A40E08"
        strokeWidth={1.25}
      />
      <Path
        d="M1.113 5.03L6.11 7.92a2.818 2.818 0 002.818 0l5.031-2.82"
        stroke="#A40E08"
        strokeWidth={1.25}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default MailIcon
