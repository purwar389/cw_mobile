import * as React from "react"
import Svg, { Path } from "react-native-svg"

function DateAwaitedIcon(props) {
  return (
    <Svg
      width={22}
      height={22}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M1 11c0-4.714 0-7.072 1.464-8.536C3.928 1 6.285 1 11 1c4.714 0 7.072 0 8.536 1.464C21 3.928 21 6.285 21 11c0 4.714 0 7.072-1.464 8.536C18.072 21 15.715 21 11 21c-4.714 0-7.072 0-8.536-1.464C1 18.072 1 15.715 1 11z"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M11.008 9.43a1.579 1.579 0 100 3.158 1.579 1.579 0 000-3.158zm0 0V5.737m3.165 8.442l-2.052-2.05"
        stroke="#fff"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default DateAwaitedIcon
