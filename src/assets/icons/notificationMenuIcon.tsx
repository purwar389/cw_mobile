import * as React from "react"
import Svg, { Path } from "react-native-svg"

function NotificationMenuIcon({color="#352F36", ...props}) {
  return (
    <Svg
      width={20}
      height={20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M3.176 16V7c0-1.591.688-3.117 1.913-4.243C6.313 1.632 7.974 1 9.706 1c1.732 0 3.392.632 4.617 1.757C15.547 3.883 16.235 5.41 16.235 7v9M3.176 16h13.06m-13.06 0H1m15.235 0h2.177m-9.794 3h2.176"
        stroke={color}
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default NotificationMenuIcon
