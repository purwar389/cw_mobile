import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"

function BackButtonIcon(props) {
  return (
    <Svg
      width={32}
      height={32}
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle
        cx={16}
        cy={16}
        r={15}
        fill="#fff"
        stroke="#5D595E"
        strokeWidth={1.5}
      />
      <Path
        d="M15.615 21L11 16l4.615-5m-3.974 5H21"
        stroke="#5D595E"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default BackButtonIcon
