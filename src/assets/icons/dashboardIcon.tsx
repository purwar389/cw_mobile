import * as React from "react";
import Svg, { Path } from "react-native-svg";
 
function DashboardIcon({ color = "#352F36", ...props }) {
  return (
    <Svg
      width={22}
      height={22}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M18.911 12.622h-4.2a2.1 2.1 0 00-2.088 2.09v4.2A2.089 2.089 0 0014.71 21h4.2A2.09 2.09 0 0021 18.911v-4.2a2.089 2.089 0 00-2.089-2.088m0-11.623H3.09A2.088 2.088 0 001 3.089v4.2a2.089 2.089 0 002.089 2.09H18.91A2.089 2.089 0 0021 7.288v-4.2A2.089 2.089 0 0018.911 1zM7.29 12.622h-4.2A2.09 2.09 0 001 14.712v4.2A2.088 2.088 0 003.089 21h4.2a2.089 2.089 0 002.088-2.089v-4.2a2.1 2.1 0 00-2.088-2.09z"
        stroke={color}  // Use the color prop here
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}
 
export default DashboardIcon;