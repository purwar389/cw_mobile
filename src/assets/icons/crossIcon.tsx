import * as React from "react"
import Svg, { Path } from "react-native-svg"

function CrossIcon(props) {
  return (
    <Svg
      width={10}
      height={10}
      viewBox="0 0 10 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M8.833 8.833l-8-8m8 0l-8 8"
        stroke="#5D595E"
        strokeLinecap="round"
      />
    </Svg>
  )
}

export default CrossIcon
