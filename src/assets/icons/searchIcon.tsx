import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SearchIcon(props) {
  return (
    <Svg
      width={17}
      height={17}
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M12.425 10.967a.938.938 0 00-.664-.275h-.266a.28.28 0 01-.195-.079.29.29 0 01-.022-.388 6.286 6.286 0 001.358-3.907 6.318 6.318 0 10-6.318 6.318c1.47 0 2.83-.506 3.907-1.358a.29.29 0 01.388.022.28.28 0 01.079.195v.266c0 .249.099.488.275.664l3.86 3.852a1.025 1.025 0 001.45-1.45l-3.852-3.86zm-6.107-.275a4.368 4.368 0 01-4.374-4.374 4.368 4.368 0 014.374-4.374 4.368 4.368 0 014.374 4.374 4.368 4.368 0 01-4.374 4.374z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default SearchIcon
