import * as React from "react"
import Svg, { Path } from "react-native-svg"

function LabelIcon(props) {
  return (
    <Svg
      width={13}
      height={17}
      viewBox="0 0 15 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.583 1.75a.083.083 0 00-.083.083v14.601l5.572-2.88a.75.75 0 01.69 0l5.571 2.88v-14.6a.083.083 0 00-.083-.084H1.583zM.463.714c.298-.297.7-.464 1.12-.464H13.25a1.583 1.583 0 011.583 1.583v15.834a.75.75 0 01-1.094.666l-6.322-3.27-6.323 3.27A.75.75 0 010 17.667V1.833c0-.42.167-.822.464-1.12z"
        fill="#A40E08"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.333 6.833a.75.75 0 01.75-.75h6.667a.75.75 0 010 1.5H4.083a.75.75 0 01-.75-.75z"
        fill="#A40E08"
      />
    </Svg>
  )
}

export default LabelIcon
