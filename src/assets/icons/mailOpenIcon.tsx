import * as React from "react"
import Svg, { Path } from "react-native-svg"

function MailOpenIcon(props) {
  return (
    <Svg
      width={22}
      height={22}
      viewBox="0 0 22 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M3.276 4.599L8.967 1.52a4.336 4.336 0 014.12 0l5.69 3.079a4.336 4.336 0 012.277 3.805v8.26A4.336 4.336 0 0116.718 21H5.336A4.336 4.336 0 011 16.664v-8.26a4.336 4.336 0 012.276-3.805z"
        stroke="#A40E08"
        strokeWidth={1.5}
      />
      <Path
        d="M1.174 7.2l7.685 4.445a4.336 4.336 0 004.336 0l7.74-4.336"
        stroke="#A40E08"
        strokeWidth={1.5}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default MailOpenIcon
