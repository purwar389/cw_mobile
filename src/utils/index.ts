import Toast from "react-native-toast-message";

export const capitalizeFirstLetter = (str: string) => {
  return str && str.length ? str.charAt(0).toUpperCase() + str.slice(1) : str;
};

export const generateRandomNumber = (min: number, max: number) => {
  return Math.floor(min + Math.random() * (max + 1 - min));
};

export const showToast = (message: string) => {
  Toast.show({
    type: 'success',
    position: 'bottom',
    text1: 'Casewise',
    text2: message,
  });
};

export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

export const validatePassword = (password: string): string | null => {
  if (password.length < 8) {
    return 'Password must be at least 8 characters long';
  }
  if (!/[A-Z]/.test(password)) {
    return 'Password must contain at least one uppercase letter';
  }
  if (!/[a-z]/.test(password)) {
    return 'Password must contain at least one lowercase letter';
  }
  if (!/[0-9]/.test(password)) {
    return 'Password must contain at least one digit';
  }
  if (!/[!@#$%^&*]/.test(password)) {
    return 'Password must contain at least one special character';
  }
  return null; // No error
};

export const formatDate = (date: Date) => {
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0'); // getMonth() is 0-indexed
  const year = date.getFullYear();
  return `${day}-${month}-${year}`;
};

