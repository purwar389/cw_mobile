import { persistCombineReducers } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
/**
 * ? Local Imports
 */
import userReducer, { InitialState as UserStore } from "./user/UserReducers";
import TokenReducer from "./token/TokenReducer";

export const reducers = {
  user: userReducer,
  token: TokenReducer,
};

export type MainState = {
  user: UserStore;
};

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  timeout: undefined,
  whitelist: ["user", "token"],
};

export const persistedRootReducer = persistCombineReducers(
  persistConfig,
  reducers,
);

export type RootState = ReturnType<typeof persistedRootReducer>;

export default persistedRootReducer;
