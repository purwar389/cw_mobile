import { RootState } from "./RootReducer";

export const selectToken = (state: RootState) => state.token.token;
