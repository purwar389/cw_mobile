import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface TokenState {
  token: string | null;
}

const initialState: TokenState = {
  token: null, // Initialize token as null
};

const tokenReducer = createSlice({
  name: "token",
  initialState,
  reducers: {
    setToken(state, action: PayloadAction<string>) {
      state.token = action.payload;
    },
    resetToken(state) {
      state.token = null;
    },
  },
});

export const { setToken, resetToken } = tokenReducer.actions;

export default tokenReducer.reducer;
