import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, FlatList, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./CMCard.style";
import TextWrapper from "../../../shared/components/text-wrapper/TextWrapper";
import Feather from 'react-native-vector-icons/Feather';
import RoundedButton from "../../../shared/components/buttons/RoundedButton";
import fonts from "../../../shared/theme/fonts";
import LinkIcon from "../../../assets/icons/linkIcon";
import { SCREENS } from "../../../shared/constants";
import TrashIcon from "../../../assets/icons/trashIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const data2 = [
    {
        name: 'iwrfwie',
    },
    {
        name: 'iwrfwie',
    }
]

const CMCard: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const navigation = useNavigation();

    console.log('connected matter data primary_case' + JSON.stringify(data.primary_case.case_category));

    const CaseCard = ({caseData}: {caseData: any}) => {
        console.log(caseData)
        return (
            <>
                <TextWrapper style={styles.mh20} color={colors.text}>{caseData?.case_category}/{caseData?.case_no}/{caseData?.case_year}</TextWrapper>
                <View style={[styles.fdRow, styles.mv10, styles.mh20]}>
                    <TextWrapper fontFamily={fonts.poppins.semiBold} color={colors.text}>{caseData?.petitioner}</TextWrapper>
                    <TextWrapper fontFamily={fonts.poppins.semiBold} color={colors.text}> VS</TextWrapper>
                </View>
                <View style={[styles.mv10, styles.mh20]}>
                    <TextWrapper fontFamily={fonts.poppins.semiBold} color={colors.text}>{caseData?.respondent}</TextWrapper>
                </View>
            </>
        )
    }

    const Separator = () => (
        <View style={styles.separator} />
    );

    return (
        <TouchableOpacity style={styles.cMCardContainer} onPress={() => { navigation.navigate(SCREENS.CASE_DETAIL) }}>
            <CaseCard caseData={data.primary_case}/>
            <View style={[{ marginVertical: 10 }, styles.mh20]}>
                <LinkIcon />
            </View>
            <FlatList
                data={data.connected_cases}
                scrollEnabled={false}
                renderItem={({item}) => <CaseCard caseData={item}/>}
                keyExtractor={(item, indx) => indx.toString()}
                ItemSeparatorComponent={Separator}
            />
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginHorizontal: 10 }}>
                <RoundedButton label="Edit Info" btnType="filled" labelFont={fonts.poppins.regular} style={{ height: 25, marginHorizontal: 5, flexDirection: 'row', flex: 1 }} labelStyle={{ ...styles.labelStyle, color: colors.iconWhite }} iconLeft={<Feather name='edit' color={colors.background} size={14} />} />
                <RoundedButton label="Delete" btnType="outlined" labelFont={fonts.poppins.regular} style={{ height: 25, marginHorizontal: 5, flexDirection: 'row', flex: 1 }} labelStyle={styles.labelStyle} iconLeft={<TrashIcon strokeColor={colors.primary} />} />
            </View>
        </TouchableOpacity>
    );
};

export default CMCard;