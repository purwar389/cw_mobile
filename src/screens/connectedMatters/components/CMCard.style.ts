import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../../shared/theme/fonts";
import { color } from "react-native-reanimated";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    titleTextStyle: {
      fontSize: 32,
    },
    buttonStyle: {
      height: 45,
      width: ScreenWidth * 0.9,
      marginTop: 32,
      borderRadius: 12,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.primary,
      shadowRadius: 5,
      shadowOpacity: 0.7,
      shadowColor: colors.shadow,
      shadowOffset: {
        width: 0,
        height: 3,
      },
    },
    buttonTextStyle: {
      color: colors.iconWhite,
      fontWeight: "700",
    },
    header: {
      width: ScreenWidth * 0.9,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
    contentContainer: {
      flex: 1,
      marginTop: 16,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    separator: {
        height: 2, // Height of the separator
        backgroundColor: '#CED0CE',
        marginVertical: 10,
        marginHorizontal: 20
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 24,
      fontFamily: fonts.poppins.regular,
      fontWeight: '600',
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    labelStyle: {
        fontSize: 12,
        color: colors.primary,
        marginHorizontal: 5
      },
      cMCardContainer: {backgroundColor: colors.iconWhite, justifyContent: 'center', paddingVertical: 10, marginHorizontal: 20,marginVertical: 10, borderRadius: 5, elevation: 3}
  });
};