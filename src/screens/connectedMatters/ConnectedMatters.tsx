import { useEffect, useMemo, useState } from "react";
import { FlatList, TouchableOpacity, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./ConnectedMatters.style";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import Ionicons from 'react-native-vector-icons/Ionicons';
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import { localStrings } from "../../shared/localization";
import CMCard from "./components/CMCard";
import { SCREENS } from "../../shared/constants";
import LinkIcon from "../../assets/icons/linkIcon";
import { API } from "@services/apiservices/apiHandler";

interface LoginScreenProps { }

const ConnectedMatters: React.FC<LoginScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [cmData, setCmData] = useState();
    
    const data = [
        { name: "The Indian Penal Code, 1860" },
        { name: "The National Highways Authority of India Act, 1988" },
    ]

    const fetchAllConnectedMatters = async () => {
        try {
            setIsLoading(true);
            const apiEndpoint = '/api/cases/connected-matters';

            // Make API request
            const response = await API.getAPIService(apiEndpoint);
            console.log(response.connected_matters);
            setCmData(response.connected_matters);
        } catch (error) {
            setIsLoading(false);
            console.error(error);
        } finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        fetchAllConnectedMatters()
    }, [])



    return (
        <View style={styles.container}>
            <SearchBar
                placeholder={localStrings.search}
                onChangeText={() => { }}
                leftIcon={<Ionicons name='search' color={colors.primary} size={20} />}
                style={{ flexDirection: 'row', backgroundColor: colors.white, elevation: 3, marginTop: 10, marginBottom: 5, height: 37 }}
            // value={cnrNumber}
            // style={styles.textInput}
            />

            <TouchableOpacity style={styles.linkMattersCard} onPress={() => navigation.navigate(SCREENS.ADD_CONNECTED_MATTERS)}>
                <LinkIcon />
                <TextWrapper style={styles.linkMattersText} color={colors.text}>{localStrings.linkMatters}</TextWrapper>
            </TouchableOpacity>

            <FlatList
                data={cmData}
                renderItem={({item}) => <CMCard data={item} />}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
}

export default ConnectedMatters;