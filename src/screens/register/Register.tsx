import { useMemo, useState } from "react";
import { Linking, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./Register.style";
import { localStrings } from "../../shared/localization";
import InputText from "../../shared/components/InputText";
import Header from "../../shared/components/header/Header";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import { SafeAreaView } from "react-native-safe-area-context";
import fonts from "../../shared/theme/fonts";

interface RegisterScreenProps { };

interface UserData {
    name: string,
    email: string,
    password: string,
    cnfPassword: string,
    phone: string
};

const Register: React.FC<RegisterScreenProps> = () => {
    const theme = useTheme();
    const navigation = useNavigation();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const [isError, setIsError] = useState<boolean>(false);

    const [userData, setUserData] = useState<UserData>({
        name: '',
        email: '',
        password: '',
        cnfPassword: '',
        phone: ''
    });

    const _handleChange = (text: string, field: keyof UserData) => {
        setUserData({ ...userData, [field]: text });
    }

    const handleTermsPress = () => {
        Linking.openURL('https://www.example.com/terms');
    };

    const handlePrivacyPress = () => {
        Linking.openURL('https://www.example.com/privacy');
    };

    const _handlePay = () => {
        const { name, email, password, cnfPassword, phone } = userData;

        if (!name || !email || !password || !cnfPassword || !phone) {
            setIsError(true);
            return;
        }

        if (password !== cnfPassword) {
            setIsError(true);
            return;
        }
        
        registerUser();
    }

    const registerUser = () => {
        setIsError(false);
        try {
            
          } catch (e: any) {
            console.log(e.message);
          }
        // navigation.navigate(SCREENS.HOME);
    }


    return (
        <SafeAreaView style={styles.container}>
            <Header label={localStrings.register!} navigation={navigation} />

            <View style={styles.centredContianer}>
                <InputText
                    placeholder={localStrings.name}
                    onChangeText={(text) => _handleChange(text, 'name')}
                    value={userData.name}
                    borderColor={colors.gray}
                    inputStyle={{...styles.bgWhite, color: colors.darkGray}}
                    style={styles.mh5}
                />

                <InputText
                    placeholder={localStrings.email}
                    onChangeText={(text) => _handleChange(text, 'email')}
                    value={userData.email}
                    inputStyle={{...styles.bgWhite, color: colors.darkGray}}
                    style={styles.mh5}
                    borderColor={colors.gray}
                    keyboardType="email-address"
                />

                <InputText
                    placeholder={localStrings.password}
                    onChangeText={(text) => _handleChange(text, 'password')}
                    value={userData.password}
                    inputStyle={{...styles.bgWhite, color: colors.darkGray}}
                    style={styles.mh5}
                    borderColor={colors.gray}
                    secureTextEntry={true}
                />

                <InputText
                    placeholder={localStrings.cnfPass}
                    onChangeText={(text) => _handleChange(text, 'cnfPassword')}
                    value={userData.cnfPassword}
                    inputStyle={{...styles.bgWhite, color: colors.darkGray}}
                    style={styles.mh5}
                    borderColor={colors.gray}
                    secureTextEntry={true}
                />

                <InputText
                    placeholder={localStrings.phone}
                    onChangeText={(text) => _handleChange(text, 'phone')}
                    value={userData.phone}
                    inputStyle={{...styles.bgWhite, color: colors.darkGray}}
                    style={styles.mh5}
                    borderColor={colors.gray}
                    keyboardType="number-pad"
                    maxLength={10}
                />

                {isError && <TextWrapper viewStyle={styles.mandatoryText} color={colors.primary} fontFamily={fonts.poppins.medium}>{localStrings.allFieldMandatory}</TextWrapper>}

                <View style={[styles.mh20, styles.mV30, { alignItems: 'center' }]}>
                    <TextWrapper color={colors.gray} style={styles.fw600}>
                        By signing up, you agree to our
                    </TextWrapper>
                    <View style={{ flexDirection: 'row' }}>
                        <TextWrapper color={colors.gray} fontFamily={fonts.poppins.bold}>
                            Terms of Service,
                        </TextWrapper>
                        <TextWrapper color={colors.gray} fontFamily={fonts.poppins.bold}>
                            {' '}Privacy Policy
                        </TextWrapper>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                    <TextWrapper color={colors.gray} style={styles.fw600}>
                        and
                    </TextWrapper>
                    <TextWrapper color={colors.gray} fontFamily={fonts.poppins.bold}>
                        {' '}Refund Policy
                    </TextWrapper>
                    </View>
                </View>

                <View style={[styles.mh20, { alignItems: 'center' }]}>
                    <RoundedButton label="Pay Now" onPress={_handlePay} btnType="filled" style={{height: 46, paddingHorizontal: 30, borderRadius: 23}}/>
                </View>
            </View>
        </SafeAreaView>
    );
}

export default Register;
