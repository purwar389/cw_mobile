import React, {useMemo, useState} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
/**
 * ? Local Imports
 */
import createStyles from './AddIndividual.style';
import {localStrings} from '../../../../shared/localization';
import RoundedButton from '../../../../shared/components/buttons/RoundedButton';
import InputText from '../../../../shared/components/InputText';
import fonts from '../../../../shared/theme/fonts';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
  style?: CustomStyleProp;
  cnrNumber: string;
  onChangeCNR: (cnr: string) => void;
}

const AddIndividual: React.FC<ICardItemProps> = ({
  style,
  cnrNumber,
  onChangeCNR,
}) => {
  const theme = useTheme();
  const styles = useMemo(() => createStyles(theme), [theme]);
  const {colors} = theme;
  const [screen, setScreen] = useState('individual');
  const navigation = useNavigation();

  const handleScreenChange = (screenName: string) => {
    setScreen(screenName);
  };

  return (
    <View style={styles.container}>
      <InputText
        placeholder={localStrings.clientName}
        keyboardType="name-phone-pad"
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10}}
        inputStyle={styles.textInput}
      />
      <InputText
        placeholder={localStrings.email}
        keyboardType="email-address"
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10}}
        inputStyle={styles.textInput}
      />
      <InputText
        placeholder={localStrings.phone}
        keyboardType="number-pad"
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10}}
        inputStyle={styles.textInput}
      />
      <InputText
        placeholder={localStrings.address}
        keyboardType="name-phone-pad"
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10}}
        inputStyle={styles.textInput}
      />
      <View
        style={[
          styles.fdRow,
          {
            justifyContent: 'center',
            width: '60%',
            alignSelf: 'center',
            marginTop: 20,
          },
        ]}>
        <RoundedButton
          label="Cancel"
          btnType="outlined"
          style={{marginHorizontal: 5, height: 37, flex: 1}}
          labelStyle={{fontSize: 16}}
          labelFont={fonts.poppins.regular}
          outlineColor={colors.gray}
          onPress={() => navigation.goBack()}
        />
        <RoundedButton
          label="Add"
          btnType="filled"
          style={{marginHorizontal: 5, height: 37, flex: 1}}
          onPress={() => navigation.goBack()}
          labelStyle={{fontSize: 16}}
          labelFont={fonts.poppins.regular}
        />
      </View>
    </View>
  );
};

export default AddIndividual;
