import React, {useMemo, useState} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
/**
 * ? Local Imports
 */
import createStyles from './AddOrganisational.style';
import {localStrings} from '../../../../shared/localization';
import RoundedButton from '../../../../shared/components/buttons/RoundedButton';
import InputText from '../../../../shared/components/InputText';
import TextWrapper from '../../../../shared/components/text-wrapper/TextWrapper';
import FilledButton from '../../../../shared/components/buttons/filledButton';
import fonts from '../../../../shared/theme/fonts';
import AddMoreIcon from '../../../../assets/icons/addMoreIcon';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
  style?: CustomStyleProp;
  cnrNumber: string;
  onChangeCNR: (cnr: string) => void;
}

const AddOrganisational: React.FC<ICardItemProps> = ({
  style,
  cnrNumber,
  onChangeCNR,
}) => {
  const theme = useTheme();
  const styles = useMemo(() => createStyles(theme), [theme]);
  const {colors} = theme;
  const [screen, setScreen] = useState('individual');
  const navigation = useNavigation();

  const handleScreenChange = (screenName: string) => {
    setScreen(screenName);
  };

  return (
    <View style={styles.container}>
      <InputText
        placeholder={localStrings.orgName}
        keyboardType="name-phone-pad"
        placeholderTextColor={colors.lightGray}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        inputStyle={styles.textInput}
        style={styles.mt10}
      />
      <InputText
        placeholder={localStrings.orgAdd}
        keyboardType="email-address"
        onChangeText={onChangeCNR}
        placeholderTextColor={colors.lightGray}
        value={cnrNumber}
        inputStyle={styles.textInput}
        style={styles.mt10}
      />
      <View style={styles.authCardStyle}>
        <TextWrapper
          fontFamily={fonts.poppins.medium}
          viewStyle={{marginStart: 5}}
          color={colors.darkGray}>
          {localStrings.authPersonDetails}
        </TextWrapper>
        <InputText
          placeholder={localStrings.authPersonName}
          keyboardType="name-phone-pad"
          onChangeText={onChangeCNR}
          placeholderTextColor={colors.lightGray}
          value={cnrNumber}
          style={{marginTop: 10, marginHorizontal: -15}}
          inputStyle={{backgroundColor: colors.background}}
        />
        <InputText
          placeholder={localStrings.email}
          keyboardType="email-address"
          onChangeText={onChangeCNR}
          placeholderTextColor={colors.lightGray}
          value={cnrNumber}
          style={{marginTop: 10, marginHorizontal: -15}}
          inputStyle={{backgroundColor: colors.background}}
        />
        <InputText
          placeholder={localStrings.phone}
          keyboardType="email-address"
          onChangeText={onChangeCNR}
          placeholderTextColor={colors.lightGray}
          value={cnrNumber}
          style={{marginTop: 10, marginHorizontal: -15}}
          inputStyle={{backgroundColor: colors.background}}
        />
        <FilledButton
          labelFont={fonts.poppins.regular}
          label={localStrings.addMore!}
          iconLeft={<AddMoreIcon />}
          labelStyle={{marginHorizontal: 10, fontSize: 16}}
          style={{
            marginTop: 10,
            flexDirection: 'row',
            marginHorizontal: 5,
            height: 37,
          }}
        />
      </View>
      <View style={styles.buttonContainer}>
        <RoundedButton
          label="Cancel"
          btnType="outlined"
          style={styles.roundedButtonStyle}
          labelStyle={styles.roundedButtonTextStyle}
          outlineColor={colors.gray}
          labelFont={fonts.poppins.regular}
          onPress={() => navigation.goBack()}
        />
        <RoundedButton
          label="Add"
          btnType="filled"
          style={styles.roundedButtonStyle}
          labelStyle={styles.roundedButtonTextStyle}
          labelFont={fonts.poppins.regular}
          onPress={() => navigation.goBack()}
        />
      </View>
    </View>
  );
};

export default AddOrganisational;
