import { StyleSheet } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        listContainer: {
            marginTop: 8,
        },
        cardContainer: {
            backgroundColor: colors.iconWhite,
            padding: 10,
            marginHorizontal: 20,
            borderRadius: 5,
            borderColor: colors.primary,
            borderWidth: 1,
            marginVertical: 5
        },
        mh20: {
            marginHorizontal: 20
        },
        mh_10: {
            marginHorizontal: -15
        },
        mh5: {
            marginHorizontal: 5
        },
        mv10: {
            marginHorizontal: 10
        },
        fdRow: {
            flexDirection: 'row'
        },
        labelStyle: {
            fontSize: 14,
            color: colors.primary,
            marginHorizontal: 5
        },
        authCardStyle: {
            backgroundColor: colors.iconWhite, 
            borderColor: colors.primary, 
            borderWidth: 0.5, 
            borderRadius: 5, 
            padding: 10, 
            marginHorizontal: 20, 
            marginVertical: 10
        },
        roundedButtonStyle: {
            marginHorizontal: 5,
            height: 37,
            flex: 1
        },
        textInput: {
            backgroundColor: colors.white
        },
        mt10: {
            marginTop: 10
        },
        roundedButtonTextStyle: {
            fontSize: 16
        },
        buttonContainer: {
            justifyContent: 'center', 
            width: '60%', 
            alignSelf: 'center', 
            flexDirection: 'row',
            marginTop: 20
        }
    });
};