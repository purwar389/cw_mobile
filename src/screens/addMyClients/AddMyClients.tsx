import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, ScrollView } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./AddMyClients.style";
import SelectButton from "../../shared/components/buttons/SelectButton";
import { localStrings } from "../../shared/localization";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import AddIndividual from "./components/addIndividual/AddIndividual";
import AddOrganisational from "./components/addOrganisational/AddOrganisational";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const AddMyClients: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const {colors} = theme;
    const [screen, setScreen] = useState("individual");

    const handleScreenChange = (screenName: string) => {
        setScreen(screenName);
    };

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row' , width: '92%', alignSelf: 'center'}}>
                <SelectButton
                    label="Individual"
                    selected={screen === "individual"}
                    labelStyle={{ fontSize: 14, fontWeight: '500' }}
                    outlineColor={colors.gray}
                    style={{ flex: 1, paddingHorizontal: 0 , height: 45}}
                    onPress={() => handleScreenChange("individual")}
                />
                <SelectButton
                    label={localStrings.organosation!}
                    selected={screen === localStrings.organosation}
                    labelStyle={{ fontSize: 14, fontWeight: '500' }}
                    outlineColor={colors.gray}
                    style={{ flex: 1, paddingHorizontal: 0 , height: 45}}
                    onPress={() => handleScreenChange(localStrings.organosation!)}
                />
                <SelectButton
                    label={localStrings.reference!}
                    selected={screen === localStrings.reference}
                    labelStyle={{ fontSize: 14, fontWeight: '500' }}
                    outlineColor={colors.gray}
                    style={{ flex: 1, paddingHorizontal: 0 , height: 45}}
                    onPress={() => handleScreenChange(localStrings.reference!)}
                />
            </View>
            <ScrollView>
                {screen == 'individual' && <AddIndividual cnrNumber="" onChangeCNR={() => {}}/>}
                {screen == localStrings.organosation && <AddOrganisational cnrNumber="" onChangeCNR={() => {}}/>}
                {screen == localStrings.reference && <AddIndividual cnrNumber="" onChangeCNR={() => {}}/>}
            </ScrollView>
        </View>
    );
};

export default AddMyClients;