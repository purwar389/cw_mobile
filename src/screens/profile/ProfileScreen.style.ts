import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";

interface Style {
    container: ViewStyle;
    profileContainer: ViewStyle;
    profileDetailContainer: ViewStyle;
    cardStyle: ViewStyle;
    valueTextStyle: TextStyle;
    outlinedbtnLabelStyle: TextStyle;
    filledbtnLabelStyle: TextStyle;
    buttonsContainer: ViewStyle;
    roundedButtonStyle: ViewStyle;
    mh5: ViewStyle;
    bgWhite: ViewStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        bgWhite: {
            backgroundColor: colors.white
        },
        mh5: {
            marginHorizontal: 5,
            marginVertical: 10
        },
        profileContainer: {
            marginTop: 32,
            alignItems: "center",
            justifyContent: "center",
        },
        profileDetailContainer: {
            marginTop: 16,
            width: ScreenWidth * 0.9,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-around",
        },
        cardStyle: {
            minWidth: ScreenWidth * 0.4,
            height: 75,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: colors.background,
            shadowRadius: 8,
            shadowOpacity: 0.3,
            shadowColor: "#757575",
            shadowOffset: {
                width: 0,
                height: 3,
            },
        },
        valueTextStyle: {
            marginTop: 8,
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center',
            width: '60%',
            alignSelf: 'center'
        },
        outlinedbtnLabelStyle: {
            color: colors.gray,
            fontSize: 18,
        },
        filledbtnLabelStyle: {
            color: colors.iconWhite,
            fontSize: 18,
        },
        roundedButtonStyle: {
            marginHorizontal: 5,
            flex: 1,
            height: 46,
            borderRadius: 100
        }
    });
};
