import React, { useMemo } from "react";
import { Dimensions, Image, ScrollView, StyleProp, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import { useSelector } from "react-redux";
/**
 * ? Local Imports
 */
import createStyles from "./ProfileScreen.style";
import { MainState } from "../../services/redux/RootReducer";
import { IUser } from "../../services/models";
import InputText from "../../shared/components/InputText";
import { localStrings } from "../../shared/localization";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import { images } from "../../constants/images/images";
import CameraIcon from "../../assets//icons/cameraIcon";
import fonts from "../../shared/theme/fonts";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ProfileScreenProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
 }

const ProfileScreen: React.FC<ProfileScreenProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const userData = useSelector(
        (state: MainState) => state.user.userData as IUser,
    );

    return (
        <ScrollView style={styles.container}>

            <View style={{alignItems: 'center', width: Dimensions.get('window').width * 0.5, alignSelf: 'center'}}>
                <Image source={images.profileImage} style={{width: Dimensions.get('window').width * 0.5, height: Dimensions.get('window').width * 0.5,borderColor: colors.white, borderRadius: Dimensions.get('window').width * 0.25, borderWidth: 10, marginTop: 10}}/>
                <View style={{position: 'absolute', backgroundColor: colors.primary, padding: 5, borderRadius: 13, bottom: 10, right: 30}}>
                    <CameraIcon/>
                </View>
            </View>
            <InputText
                placeholder={localStrings.firstName}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.lastName}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.email}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.phone}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.address}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                numberOfLines={3}
                inputStyle={{...styles.bgWhite, paddingVertical: 10}}
                style={{marginHorizontal: 5}}
                height={100}
            /> 
            <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: Dimensions.get('window').width * 0.7, alignSelf: 'center', marginTop: 10}}>
                <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{marginHorizontal: 5, height: 46, flex: 1, borderRadius: 100}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
                <RoundedButton label="Update" btnType="filled" style={{marginHorizontal: 5, height: 46, flex: 1, borderRadius: 100}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
            </View>
        </ScrollView>
    );
};

export default ProfileScreen;
