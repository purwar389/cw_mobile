import { StyleSheet} from "react-native";
import { ExtendedTheme } from "@react-navigation/native";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    separator: {
      height: 1, // Height of the separator
      backgroundColor: '#CED0CE',
      marginVertical: 10
    },
    ms10: {
      marginStart: 10
    },
    labelCard: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 5,
      alignItems: 'center',
      borderRadius: 5,
      borderColor: colors.iconBlack,
      borderWidth: 1,
      margin: 5
    },
    labelContainer: {
      flexDirection: 'row',
      flexWrap: "wrap"
    },
    fdRowAiCenterJcBW: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between'
    },
    labelContainerCard: {
      backgroundColor: colors.iconWhite,
      marginHorizontal: 15,
      padding: 10,
      borderWidth: 1,
      borderColor: colors.primary,
      borderRadius: 5
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    labelStyle: {
      fontSize: 14,
      color: colors.primary,
      marginHorizontal: 5
    },
    richTextEditorStyle: {
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      borderWidth: 1,
      borderColor: "#ccaf9b",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
      fontSize: 20,
    },
    editor: {
      backgroundColor: "black",
    },
    richTextToolbarStyle: {
      backgroundColor: "#c6c3b3",
      borderColor: "#c6c3b3",
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      borderWidth: 1,
    },
    rich: {
      minHeight: 300,
      flex: 1,
    },
    richBar: {
      height: 50,
      backgroundColor: "#F5FCFF",
    },
    text: {
      fontWeight: "bold",
      fontSize: 20,
    },
  });
};