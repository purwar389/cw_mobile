import React, { useMemo, useRef, useState } from "react";
import { View, StyleProp, ViewStyle, TextInput } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./AddNote.style";
import { actions, RichEditor, RichToolbar } from "react-native-pell-rich-editor";
import fonts from "../../shared/theme/fonts";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const AddNote: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const richText = useRef();
    const [showDescError, setShowDescError] = useState<boolean>(false);
    const [descHTML, setDescHTML] = useState("");
    const RichTextEditor = useRef();
    const [article, setArticle] = useState("");

    const richTextHandle = (descriptionText: any) => {
        if (descriptionText) {
            setShowDescError(false);
            setDescHTML(descriptionText);
        } else {
            setShowDescError(true);
            setDescHTML("");
        }
    };
    function editorInitializedCallback() {
        if (RichTextEditor.current) {
            RichTextEditor.current.setFontSize(20); // Set font size to 20
        }
        RichTextEditor.current?.registerToolbar(function (items) {
          // items contain all the actions that are currently active
          console.log(
            "Toolbar click, selected items (insert end callback):",
            items
          );
        });
      }

      function handleHeightChange(height) {
        // console.log("editor height change:", height);
      }

    return (
        <View style={styles.container}>
            <RichToolbar
                editor={richText}
                selectedIconTint="#873c1e"
                iconTint="#312921"
                actions={[
                    actions.insertImage,
                    actions.setBold,
                    actions.setItalic,
                    actions.insertBulletsList,
                    actions.insertOrderedList,
                    actions.insertLink,
                    actions.setStrikethrough,
                    actions.setUnderline,
                ]}
                style={styles.richTextToolbarStyle}
            />
            <View>
                <TextInput
                    placeholder="Title"
                    style={{fontSize: 20, fontFamily: fonts.poppins.medium, paddingBottom: 0, backgroundColor: 'white', marginStart: 5}}

                />
            </View>
            <RichEditor
                disabled={false}
                containerStyle={styles.editor}
                ref={RichTextEditor}
                style={styles.rich}
                placeholder={"Note"}
                onChange={(text) => setArticle(text)}
                editorInitializedCallback={editorInitializedCallback}
                onHeightChange={handleHeightChange}
            />
        </View>
    );
};

export default AddNote;