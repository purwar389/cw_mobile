import React, { useMemo, useState } from 'react';
import { View, StyleProp, ViewStyle, ScrollView, Pressable } from 'react-native';
import { useNavigation, useTheme } from '@react-navigation/native';
/**
 * ? Local Imports
 */
import createStyles from './CaseDetail.style';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { localStrings } from '../../shared/localization';
import RoundedButton from '../../shared/components/buttons/RoundedButton';
import TextWrapper from '../../shared/components/text-wrapper/TextWrapper';
import BasicInfo from './components/basicInfo/BasicInfo';
import CaseHistory from './components/caseHistory/CaseHistory';
import Orders from './components/orders/Orders';
import Notes from './components/notes/Notes';
import Remarks from './components/remarks/Remarks';
import Documents from './components/documents/Documents';
import InfoIcon from '../../assets/icons/infoIcon';
import fonts from '../../shared/theme/fonts';
import LabelIcon from '../../assets/icons/labelIcon';
import CrossIcon from '../../assets/icons/crossIcon';
import SwitchToggle from '@imcarlosguerrero/react-native-switch-toggle';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const CaseDetail: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [toggleMode, setToggleMode] = useState<boolean>(false);
    const navigation = useNavigation();

    const labels = [
        { name: 'lorem ipsum' },
        { name: 'lorem ipsum' },
        { name: 'lorem ipsum' },
        { name: 'lorem ipsum' },
    ]

    const icons = {
        edit: <Feather name="edit" color={colors.primary} size={16} />,
        update: <Feather name="refresh-ccw" color={colors.primary} size={16} />,
        delete: <AntDesign name="delete" color={colors.primary} size={16} />,
        bookmark: <LabelIcon/>,
        info: <InfoIcon/>,
        add: <Ionicons name="add-circle" color={colors.primary} size={22} />,
        cross: <CrossIcon/>
    };

    return (
        <View style ={styles.container}>
            <View style={[styles.fdRow, {marginHorizontal: 15}]}>
                <RoundedButton 
                    label={localStrings.edit!}
                    // selected={screen == localStrings.limitationDate}
                    btnType="outlined" 
                    labelStyle={styles.labelStyle} 
                    labelFont={fonts.poppins.regular}
                    style={{ flex: 1, marginEnd: 5, flexDirection: 'row', height: 36}} 
                    iconLeft={icons.edit}
                    onPress={() => {}}
                />
                <RoundedButton
                    label={localStrings.update!}
                    // selected={screen == localStrings.calculationOfDays} 
                    btnType="outlined" 
                    iconLeft={icons.update}
                    labelFont={fonts.poppins.regular}
                    labelStyle={styles.labelStyle} 
                    style={{ flex: 1 , marginStart: 5, flexDirection: 'row', height: 36}} 
                    onPress={() => setScreen(localStrings.calculationOfDays!)}
                />
                <RoundedButton
                    label={localStrings.delete!}
                    // selected={screen == localStrings.calculationOfDays} 
                    btnType="outlined" 
                    iconLeft={icons.delete}
                    labelStyle={styles.labelStyle} 
                    labelFont={fonts.poppins.regular}
                    style={{ flex: 1 , marginStart: 5, flexDirection: 'row', height: 36}} 
                    onPress={() => setScreen(localStrings.calculationOfDays!)}
                />
            </View>
            <ScrollView>
                <View style={styles.labelContainerCard}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 10}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            {icons.bookmark}
                            <TextWrapper color={colors.primary} style={{fontSize: 16, marginHorizontal: 10}} fontFamily={fonts.poppins.medium}>{localStrings.labels}</TextWrapper>
                            {icons.info}
                        </View>
                        {icons.add}
                    </View>
                    <View style={styles.labelContainer}>
                        {labels.map((item, index) => {
                            return <View style={styles.labelCard}>
                                <TextWrapper style={{fontSize: 12}} color={colors.darkGray}>{item.name}</TextWrapper>
                                <View style={[styles.ms10]}>
                                    {icons.cross}
                                </View>
                            </View>
                        })}
                    </View>
                </View>
                <BasicInfo data={{}}/>
                <CaseHistory data={{}}/>
                <Orders data={{}}/>
                <Notes data={{}}/>
                <Remarks data={{}}/>
                <Documents data={{}}/>
                <View style={[styles.fdRowAiCenterJcBW, {backgroundColor: colors.iconWhite, paddingHorizontal: 10, height: 36, borderRadius: 5, elevation: 3, marginHorizontal: 15, marginTop: 20}]}>
                    <TextWrapper color={colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 10}}>Decided</TextWrapper>
                    <SwitchToggle
                        switchOn={toggleMode}
                        onPress={() => setToggleMode(!toggleMode)}
                        circleColorOff={colors.background}
                        circleColorOn={colors.primary}
                        backgroundColorOff={colors.background}
                        backgroundColorOn={colors.background}
                        containerStyle={{
                            width: 51,
                            height: 22,
                            borderRadius: 25,
                            padding: 5,
                            borderColor: toggleMode ? colors.primary : colors.darkGray,
                            borderWidth: 0.5
                        }}
                        circleStyle={{
                        width: 16,
                        height: 16,
                        borderRadius: 20,
                        borderColor: toggleMode ? null : colors.darkGray,
                        borderWidth: 0.5
                        }}
                    />
                </View>

                <View style={[styles.fdRowAiCenterJcBW, {backgroundColor: colors.iconWhite, paddingHorizontal: 10, height: 36, borderRadius: 5, elevation: 3, marginHorizontal: 15, marginVertical: 20}]}>
                    <TextWrapper color={colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 10}}>{localStrings.abandoned}</TextWrapper>
                    <SwitchToggle
                        switchOn={toggleMode}
                        onPress={() => setToggleMode(!toggleMode)}
                        circleColorOff={colors.background}
                        circleColorOn={colors.primary}
                        backgroundColorOff={colors.background}
                        backgroundColorOn={colors.background}
                        containerStyle={{
                            width: 51,
                            height: 22,
                            borderRadius: 25,
                            padding: 5,
                            borderColor: toggleMode ? colors.primary : colors.darkGray,
                            borderWidth: 0.5
                        }}
                        circleStyle={{
                        width: 16,
                        height: 16,
                        borderRadius: 20,
                        borderColor: toggleMode ? null : colors.darkGray,
                        borderWidth: 0.5
                        }}
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default CaseDetail;