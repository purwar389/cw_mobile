import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, Pressable } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./BasicInfo.style";
import { localStrings } from "../../../../shared/localization";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../../../shared/theme/fonts";
import DownArrowIcon from "../../../../assets/icons/downArrowIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const BasicInfo: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [open, setOpen] = useState<boolean>(false);

    return (
        <View style ={[styles.container, {backgroundColor: open ? colors.iconWhite : null, borderRadius: 10, paddingBottom: open ? 10 : 0, elevation: open ? 3 : 0}]}>
            <Pressable style={[styles.fdRowAiCenterJcBW, {backgroundColor: open ? colors.primary : colors.iconWhite, paddingHorizontal: 15, borderRadius: 5, elevation: open ? 0 : 4, height: 36}]} onPress={() => setOpen(!open)}>
                <TextWrapper color={open ? colors.iconWhite : colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 5}}>{localStrings.basicInfo}</TextWrapper>
                <DownArrowIcon color={open ? colors.iconWhite : colors.gray}/>
            </Pressable>
            {open && <View>
                <View style={{flexDirection: 'row', alignItems: 'center',paddingHorizontal: 10, marginTop: 10, marginHorizontal: 5}}>
                    <TextWrapper fontFamily={fonts.poppins.semiBold} style={{fontSize: 16}} color={colors.text}>Sachin Sanjay Devprasad Singh</TextWrapper>
                    <TextWrapper style={{fontSize: 16}} color={colors.text}> VS</TextWrapper>
                </View>
                <TextWrapper fontFamily={fonts.poppins.semiBold} style={{fontSize: 16,paddingHorizontal: 10, marginHorizontal: 5}} color={colors.text}>Sachin Sanjay Devprasad Singh</TextWrapper>
                <TextWrapper 
                    color={colors.primary} 
                    viewStyle={styles.textWrapperStyle2}
                    fontFamily={fonts.poppins.medium}
                    style={{ fontSize: 16, marginHorizontal: 5 }} 
                >M.A.C.P./685/2022</TextWrapper>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Brief Number:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >115-A</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >CNR:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >MHNG0001543824</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Judge:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >SHRI. B. P. KSHIRSAGAR</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Room No.:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >404</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Clients:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >ICICI General Insurance Comp</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Brief for:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >Respondent</TextWrapper>
                </View>
                <View style={styles.fdRow}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Assigned to:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                    >Shivani Suryavanshi</TextWrapper>
                </View>
            </View>}
        </View>
    );
};

export default BasicInfo;