import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, FlatList, Pressable } from "react-native";
import { useTheme } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
/**
 * ? Local Imports
 */
import createStyles from "./CaseHistory.style";
import { localStrings } from "../../../../shared/localization";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import SearchBar from "../../../../shared/components/SearchBar/SearchBar";
import CaseHistoryCard from "../caseHistoryCard/CaseHistoryCard";
import fonts from "../../../../shared/theme/fonts";
import DownArrowIcon from "../../../../assets/icons/downArrowIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const CaseHistory: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [open, setOpen] = useState<boolean>(false);

    const data2=[
        {name: 'etefe'},
        {name: 'etefe'}
    ]

    const renderHistoryCard = () => {
        return <CaseHistoryCard data={{}}/>
    }

    return (
        <View style ={[styles.container, {backgroundColor: open ? colors.iconWhite : null, borderRadius: 10, paddingBottom: open ? 10 : 0, elevation: open ? 3 : 0}]}>
            <Pressable style={[styles.fdRowAiCenterJcBW, {backgroundColor: open ? colors.primary : colors.iconWhite, paddingHorizontal: 15, borderRadius: 5, elevation: open ? 0 : 4, height: 36}]} onPress={() => setOpen(!open)}>
                <TextWrapper color={open ? colors.iconWhite : colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 5}}>{localStrings.caseHistory}</TextWrapper>
                <DownArrowIcon color={open ? colors.iconWhite : colors.gray}/>
            </Pressable>
            {open && <View style={{paddingVertical: 10, backgroundColor: colors.iconWhite}}>
                <SearchBar
                    placeholder="Search"
                    onChangeText={() => { }}
                    leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                    style={{ flexDirection: 'row', backgroundColor: colors.background, height: 37, marginVertical: 10}}
                />
                <FlatList
                    data={data2}
                    renderItem={renderHistoryCard}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>}
        </View>
    );
};

export default CaseHistory;