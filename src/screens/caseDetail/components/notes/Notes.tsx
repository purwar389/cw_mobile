import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, Pressable, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import Entypo from "react-native-vector-icons/Entypo";
import Ionicons from "react-native-vector-icons/Ionicons";
/**
 * ? Local Imports
 */
import createStyles from "./Notes.style";
import { localStrings } from "../../../../shared/localization";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import SearchBar from "../../../../shared/components/SearchBar/SearchBar";
import AddNoteIcon from "../../../../assets/icons/addNoteIcon";
import NoteIcon from "../../../../assets/icons/noteIcon";
import TrashIcon from "../../../../assets/icons/trashIcon";
import fonts from "../../../../shared/theme/fonts";
import { SCREENS } from "../../../../shared/constants";
import DownArrowIcon from "../../../../assets/icons/downArrowIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const Notes: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [open, setOpen] = useState<boolean>(false);
    const navigation = useNavigation();

    const _handleAddNotes = () => {
        navigation.navigate(SCREENS.ADD_NOTE);
    }

    return (
        <View style ={[styles.container, {backgroundColor: open ? colors.iconWhite : null, borderRadius: 10, paddingBottom: open ? 10 : 0, elevation: open ? 3 : 0}]}>
            <Pressable style={[styles.fdRowAiCenterJcBW, {backgroundColor: open ? colors.primary : colors.iconWhite, paddingHorizontal: 15, borderRadius: 5, elevation: open ? 0 : 4, height: 36}]} onPress={() => setOpen(!open)}>
                <TextWrapper color={open ? colors.iconWhite : colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 5}}>{localStrings.notes}</TextWrapper>
                <DownArrowIcon color={open ? colors.iconWhite : colors.gray}/>
            </Pressable>
            {open && <View style={{padding: 10,backgroundColor: colors.iconWhite}}>
                <SearchBar
                    placeholder="Search"
                    onChangeText={() => { }}
                    leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                    style={{ marginHorizontal: 0, height: 37}}
                />

                <TouchableOpacity 
                    style={{backgroundColor: colors.cardBg, flexDirection: 'row', alignItems: 'center', marginTop: 10, paddingHorizontal: 10, borderRadius: 5, height: 37}}
                    onPress={_handleAddNotes}>
                    <AddNoteIcon/>
                    <TextWrapper color={colors.text} fontFamily={fonts.poppins.regular} style={styles.ms10}>{localStrings.addNewNote}</TextWrapper>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor: colors.cardBg, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginTop: 10, paddingHorizontal: 10, borderRadius: 5, height: 37}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <NoteIcon/>
                        <TextWrapper color={colors.text} fontFamily={fonts.poppins.regular} style={styles.ms10}>Note 1</TextWrapper>
                    </View>
                    <TrashIcon/>
                </TouchableOpacity>
            </View>}
        </View>
    );
};

export default Notes;