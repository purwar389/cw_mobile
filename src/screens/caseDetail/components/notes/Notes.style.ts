import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 20,
      marginHorizontal: 15,
    },
    listContainer: {
      marginTop: 8,
    },
    ms10: {
        marginStart: 10
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    fdRowAiCenterJcBW: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between'
    }
  });
};