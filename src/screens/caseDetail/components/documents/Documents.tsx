import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, Pressable, TouchableOpacity, Modal } from "react-native";
import { useTheme } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
/**
 * ? Local Imports
 */
import createStyles from "./Documents.style";
import { localStrings } from "../../../../shared/localization";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import SearchBar from "../../../../shared/components/SearchBar/SearchBar";
import AddDocIcon from "../../../../assets/icons/addDocIcon";
import DocIcon from "../../../../assets/icons/docIcon";
import fonts from "../../../../shared/theme/fonts";
import TrashIcon from "../../../../assets/icons/trashIcon";
import DownArrowIcon from "../../../../assets/icons/downArrowIcon";
import DeleteIconBig from "../../../../assets/icons/deleteIconBig";
import RoundedButton from "../../../../shared/components/buttons/RoundedButton";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const Documents: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [open, setOpen] = useState<boolean>(false);
    const [modalVisible, setModalVisible] = useState(false);

    return (
        <View style ={[styles.container, {backgroundColor: open ? colors.iconWhite : null, borderRadius: 10, paddingBottom: open ? 10 : 0, elevation: open ? 3 : 0}]}>
            <Pressable style={[styles.fdRowAiCenterJcBW, {backgroundColor: open ? colors.primary : colors.iconWhite, paddingHorizontal: 15, borderRadius: 5, elevation: open ? 0 : 4, height: 36}]} onPress={() => setOpen(!open)}>
                <TextWrapper color={open ? colors.iconWhite : colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 5}}>{localStrings.documents}</TextWrapper>
                <DownArrowIcon color={open ? colors.iconWhite : colors.gray}/>
            </Pressable>

            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}>
                    <View style={styles.modalOverlay}>
                        <View style={styles.modalContent}>
                            <View style={styles.emailIconCircle}>
                                <View style={styles.emailIconContainer}>
                                    <DeleteIconBig/>
                                </View>
                            </View>
                            <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium} style={{fontSize: 20}} viewStyle={{marginVertical: 20}}>{localStrings.deleteItem}</TextWrapper>
                            <TextWrapper style={{textAlign: 'center'}} viewStyle={{width: '80%', alignSelf: 'center'}} color={colors.gray}>{localStrings.areUSureToDelete}</TextWrapper>
                            <TextWrapper style={{textAlign: 'center'}} viewStyle={{width: '90%', alignSelf: 'center'}} color={colors.gray}>{localStrings.thisActionCantBeUndone}</TextWrapper>
                            <View style={[styles.fdRow, {marginTop: 30}]}>
                                <RoundedButton 
                                    btnType="outlined" 
                                    label={localStrings.cancel!} 
                                    outlineColor={colors.darkGray} 
                                    labelFont={fonts.poppins.regular} 
                                    style={{marginEnd: 5, paddingVertical: 5}} 
                                    labelStyle={styles.fs16}
                                    onPress={() => setModalVisible(false)}
                                />
                                <RoundedButton btnType="filled" label={localStrings.delete!} labelFont={fonts.poppins.regular} style={{marginStart: 5, paddingVertical: 5}} labelStyle={styles.fs16}/>
                            </View>
                        </View>
                    </View>
            </Modal>

            {open && <View style={{padding: 10,backgroundColor: colors.iconWhite}}>
                <SearchBar
                    placeholder="Search Document"
                    onChangeText={() => { }}
                    leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                    style={{ marginVertical: 5 , marginHorizontal: 0, height: 37}}
                />

                <TouchableOpacity style={{backgroundColor: colors.cardBg, flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10}}>
                    <AddDocIcon/>
                    <TextWrapper color={colors.text} fontFamily={fonts.poppins.medium} viewStyle={{marginStart: 10}}>{localStrings.addNewDoc}</TextWrapper>
                </TouchableOpacity>

                <TouchableOpacity style={{backgroundColor: colors.cardBg, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <DocIcon/>
                        <TextWrapper color={colors.text} fontFamily={fonts.poppins.medium} viewStyle={{marginStart: 10}}>Note 1</TextWrapper>
                    </View>
                    <Pressable onPress={() => setModalVisible(true)}>
                        <TrashIcon/>
                    </Pressable>
                </TouchableOpacity>
            </View>}
        </View>
    );
};

export default Documents;