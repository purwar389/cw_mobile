import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 20,
      marginHorizontal: 15,
    },
    listContainer: {
      marginTop: 8,
    },
    ms10: {
        marginStart: 10
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    fdRowAiCenterJcBW: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    modalOverlay: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)', // Modal background transparency
    },
    modalContent: {
      width: 300,
      padding: 20,
      backgroundColor: 'white',
      borderRadius: 10,
      alignItems: 'center',
    },
    fs16: {
      fontSize: 16
    },
    emailIconContainer: {backgroundColor:colors.primary, height: 77, width: 77, borderRadius: 38, alignItems: 'center', justifyContent: 'center'},
    emailIconCircle: {
      borderColor: colors.primary,
      borderWidth: 1,
      height: 102,
      width: 102,
      borderRadius: 51,
      alignItems: 'center',
      justifyContent: 'center'
    },
  });
};