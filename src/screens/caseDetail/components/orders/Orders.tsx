import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text, FlatList, Pressable } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Orders.style";
import { localStrings } from "../../../../shared/localization";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import PdfIcon from "../../../../assets/icons/pdfIcon";
import { color } from "react-native-reanimated";
import fonts from "../../../../shared/theme/fonts";
import DownArrowIcon from "../../../../assets/icons/downArrowIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const Orders: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const [open, setOpen] = useState<boolean>(false);

    return (
        <View style ={[styles.container, {backgroundColor: open ? colors.iconWhite : null, borderRadius: 10, paddingBottom: open ? 10 : 0, elevation: open ? 3 : 0}]}>
            <Pressable style={[styles.fdRowAiCenterJcBW, {backgroundColor: open ? colors.primary : colors.iconWhite, paddingHorizontal: 15, borderRadius: 5, elevation: open ? 0 : 4, height: 36}]} onPress={() => setOpen(!open)}>
                <TextWrapper color={open ? colors.iconWhite : colors.text} fontFamily={fonts.poppins.medium} style={{fontSize: 16, marginStart: 5}}>{localStrings.orders}</TextWrapper>
                <DownArrowIcon color={open ? colors.iconWhite : colors.gray}/>
            </Pressable>
            {open && <View style={{flexDirection: 'row', paddingStart: 5, alignItems: 'center', backgroundColor: colors.background, marginHorizontal: 15, marginVertical: 10, borderRadius: 5}}>
                <View style={{marginHorizontal: 5,marginVertical: 10}}>
                    <PdfIcon/>
                </View>
                <View style={[styles.ms10, {marginVertical: 10}]}>
                    <TextWrapper style={{fontSize: 16}} color={colors.text} fontFamily={fonts.poppins.medium}>Exhibit 5</TextWrapper>
                    <TextWrapper style={{fontSize: 14}} color={colors.text}>10th April 2024</TextWrapper>
                </View>
            </View>}
        </View>
    );
};

export default Orders;