import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, FlatList, TouchableOpacity, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./CaseHistoryCard.style";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../../../shared/components/buttons/OutlinedButton";
import fonts from "../../../../shared/theme/fonts";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const data2 = [
    {
        name: 'iwrfwie',
    },
    {
        name: 'iwrfwie',
    }
]

const CaseHistoryCard: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
 
    return (
        <TouchableOpacity style={{marginHorizontal: 20, paddingVertical: 10,marginVertical: 5, backgroundColor: colors.background, borderRadius: 5, elevation: 3}}
            onPress={() => {}}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingHorizontal: 10,
                    justifyContent: 'space-between',
                }}>
                    <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium}>15th April 2024</TextWrapper>  
                    <OutlinedButton label="Notice_Unready..." style={styles.outlinedBtnStyle} labelStyle={styles.outlinedbuttonLabel} labelFontFamily={fonts.poppins.medium}/>
                </View>
                <View style={{width: '95%', alignSelf: 'center'}}>
                <Text>
                    <Text style={{ fontFamily: fonts.poppins.semiBold , color: colors.darkGray}}>Court: </Text>
                    <Text style={{fontFamily: fonts.poppins.regular, color: colors.darkGray}}>34-DISTRICT JUDGE -11 ADDL. SESSION JUDGE NAGPUR</Text>
                </Text>
                </View>

                <View style={{borderColor: colors.primary, borderWidth: 0.5, borderRadius: 5, padding: 10, margin: 10}}>
                    <TextWrapper color={colors.darkGray}>Claimant absent and his counsel M. A. Sheikh present . Respondent No. 2 absent and his counsel Barapatre present. Respondent no.3 absent and his counsel Adv. D.B. Popat absent. witness absent. Respondent no.3 absent and his counsel </TextWrapper>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                    <TextWrapper 
                        color={colors.text} 
                        viewStyle={styles.textWrapperStyle}
                        fontFamily={fonts.poppins.medium}
                        style={{ fontSize: 14, marginStart: 5 }} 
                    >Next Date:</TextWrapper>
                    <TextWrapper 
                        color={colors.text} 
                        style={{ fontSize: 14 }} 
                        viewStyle={{flex: 1}}
                    >20th April 2024</TextWrapper>
                </View>
        </TouchableOpacity>
    );
};

export default CaseHistoryCard;