import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";
import { color } from "react-native-reanimated";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    separator: {
        height: 1, // Height of the separator
        backgroundColor: '#CED0CE',
        marginVertical: 10
    },
    ms10: {
        marginStart: 10
    },
    labelCard: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        paddingHorizontal: 10, 
        paddingVertical: 3,
        alignItems: 'center',
        borderRadius: 5, 
        borderColor: colors.darkGray, 
        borderWidth: 1, 
        margin: 5,
        height: 30,
        backgroundColor: colors.background
    },
    labelContainer: {
        flexDirection: 'row',
        flexWrap: "wrap",
    },
    fdRowAiCenterJcBW: { 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    labelContainerCard: {
        backgroundColor: colors.iconWhite, 
        marginTop: 5,
        marginHorizontal: 15, 
        padding: 10, 
        borderWidth: 1, 
        borderColor: colors.primary, 
        borderRadius: 5
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    labelStyle: {
        fontSize: 14,
        color: colors.primary,
        marginHorizontal: 5,
      },
  });
};