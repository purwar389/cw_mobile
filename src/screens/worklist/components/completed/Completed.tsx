import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Completed.style";
import WorkListCard from "../../../../shared/components/workListCard/WorkListCard";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const Completed: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);

    const data = [
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
    ]

    return (
        <View style={style}>
            <WorkListCard isCompleted={true}/>
        </View>
    );
};

export default Completed;