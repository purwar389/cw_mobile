import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./InProgress.style";
import WorkListCard from "../../../../shared/components/workListCard/WorkListCard";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onEdit: () => void;
}

const InProgress: React.FC<ICardItemProps> = ({ style, cnrNumber, onEdit }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);

    const data = [
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
        {
            name: "oiuygh"
        },
    ]

    return (
        <View style={style}>
            <WorkListCard isCompleted={false} onEdit={onEdit}/>
        </View>
    );
};

export default InProgress;