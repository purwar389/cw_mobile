import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
/**
 * ? Local Imports
 */
import createStyles from "./Worklist.style";
import SelectButton from "../../shared/components/buttons/SelectButton";
import { localStrings } from "../../shared/localization";
import InProgress from "./components/InProgress/InProgress";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import Completed from "./components/completed/Completed";
import { SCREENS } from "../../shared/constants";
import AddAWorkIcon from "../../assets//icons/addAWorkIcon";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const Worklist: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const {colors} = theme;
    const [screen, setScreen] = useState(localStrings.inProgress);
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={[styles.fdRow, {marginHorizontal: 15}]}>
                <SelectButton 
                    label={localStrings.inProgress!}
                    selected={screen == localStrings.inProgress} 
                    labelStyle={{...styles.labelStyle, color: screen == localStrings.inProgress ? colors.iconWhite : colors.darkGray}} 
                    style={{ flex: 1, height: 45 }} 
                    outlineColor={screen == localStrings.inProgress ? null : colors.darkGray}
                    onPress={() => setScreen(localStrings.inProgress!)}
                />
                <SelectButton
                    label={localStrings.completed!}
                    selected={screen == localStrings.completed} 
                    labelStyle={{...styles.labelStyle, color: screen == localStrings.completed ? colors.iconWhite : colors.darkGray}} 
                    outlineColor={screen == localStrings.completed ? null : colors.darkGray}
                    style={{ flex: 1, height: 45 }} 
                    onPress={() => setScreen(localStrings.completed!)}
                />
            </View>
            <SearchBar
                placeholder="Search"
                keyboardType="number-pad"
                onChangeText={() => {}}
                leftIcon={<Ionicons name='search' color={colors.primary} size={18}/>}
                style={{ flexDirection: 'row', backgroundColor: colors.white, elevation: 3, marginVertical: 5, height: 37}}
                // value={cnrNumber}
                // style={styles.textInput}
            /> 
            <TouchableOpacity
                style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: colors.iconWhite, padding: 10, marginVertical: 10, marginHorizontal: 20, elevation: 3, borderRadius: 5 }}
                onPress={() => {
                    navigation.navigate(SCREENS.ADD_WORK)
                }}
            >
                <AddAWorkIcon/>
                <TextWrapper color={colors.text} style={{ fontSize: 16, marginHorizontal: 10 }}>{localStrings.addAWork}</TextWrapper>
            </TouchableOpacity>
            {screen == localStrings.inProgress && <InProgress cnrNumber={""} onEdit={() => {}}/>}
            {screen == localStrings.completed && <Completed cnrNumber={""} onChangeCNR={() => {}}/>}
        </View>
    );
};

export default Worklist;