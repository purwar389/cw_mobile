import {useMemo} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import createStyles from './BareActs.style';
import SearchBar from '../../shared/components/SearchBar/SearchBar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import TextWrapper from '../../shared/components/text-wrapper/TextWrapper';

interface LoginScreenProps {}


const BareActs: React.FC<LoginScreenProps> = () => {
  const theme = useTheme();
  const {colors} = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  const navigation = useNavigation();

  const data = [
    {name: 'The Indian Penal Code, 1860'},
    {name: 'The National Highways Authority of India Act, 1988'},
  ];

  return (
    <View style={styles.container}>
      <SearchBar
        placeholder="Search"
        keyboardType="number-pad"
        onChangeText={() => {}}
        leftIcon={<Ionicons name="search" color={colors.primary} size={18} />}
        style={{
          flexDirection: 'row',
          backgroundColor: colors.white,
          elevation: 3,
          marginVertical: 5,
          height: 37,
        }}
        // value={cnrNumber}
        // style={styles.textInput}
      />

      {data.map((item: any, index: number) => (
        <TouchableOpacity
          key={index}
          style={styles.cardContainer}
          onPress={() => {}}>
          <TextWrapper viewStyle={{flex: 1}}>{item.name}</TextWrapper>
          <Ionicons
            name="chevron-forward-outline"
            color={colors.primary}
            size={24}
          />
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default BareActs;
