import React, { useMemo } from "react";
import { View, FlatList } from "react-native";
import { DrawerActions, useNavigation, useTheme } from "@react-navigation/native";
import * as NavigationService from "react-navigation-helpers";
/**
 * ? Local Imports
 */
import createStyles from "./HomeScreen.style";
import MockData from "./mock/MockData";
import CardItem from "./components/card-item/CardItem";
/**
 * ? Shared Imports
 */
import { SCREENS } from "../../shared/constants";
import fonts from "../../shared/theme/fonts";
import UserService from "./services/userService";
import { localStrings } from "../../shared/localization";
import DashBoard from "../dashboard/DashBoard";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import addCase from "../addCase/addCase";
import BareActs from "../bareActs/BareActs";
import MyClients from "../myClients/MyClients";
import Archives from "../archives/Archives";
import ConnectedMatters from "../connectedMatters/ConnectedMatters";
import Calculator from "../calculator/Calculator";
import Worklist from "../worklist/Worklist";
import MyAccounts from "../myAccounts/MyAccount";
import { TouchableOpacity } from "react-native-gesture-handler";
import Notification from "../notification/NotificationScreen";
import MenuIcon from "../../assets//icons/menuIcon";
import NotificationIcon from "../../assets//icons/notificationIcon";
import BackButtonIcon from "../../assets//icons/backButton";
import DashboardIcon from "../../assets//icons/dashboardIcon";
import BareActsIcon from "../../assets//icons/bareActsIcon";
import ArchiveMenuIcon from "../../assets//icons/archiveMenuIcon";
import CalculatorIcon from "../../assets//icons/calculatorIcon";
import MyClientsIcon from "../../assets//icons/myClientsIcon";
import WorkListIcon from "../../assets//icons/workListIcon";
import MyAccountIcon from "../../assets//icons/myAccountIcon";
import NotificationMenuIcon from "../../assets//icons/notificationMenuIcon";
import CustomDrawerContent from "../customDrawer/CustomDrawer";
import ActiveCases from "../activeCases/ActiveCases";
import { createDrawerNavigator } from "@react-navigation/drawer";

interface HomeScreenProps {}

const HomeScreen: React.FC<HomeScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();


    React.useEffect(() => {
        const mockUserData = {
            id: "301395-3150134",
            username: "FreakyCoder",
            fullname: "Kuray",
            email: "freakycoder@gmail.com",
            socialType: "google",
            creationDate: 1652631678000,
            photo: null,
        };
        UserService.setUserData(mockUserData);
    }, []);

    const handleItemPress = () => {
        NavigationService.push(SCREENS.DETAIL);
    };

  /* -------------------------------------------------------------------------- */
  /*                               Render Methods                               */
  /* -------------------------------------------------------------------------- */

    const renderList = () => (
        <View style={styles.listContainer}>
            <FlatList
                data={MockData}
                renderItem={({ item }) => (
                    <CardItem data={item} onPress={handleItemPress} />
                )}
            />
        </View>
    );

    const Drawer = createDrawerNavigator(); 

    return (
        <Drawer.Navigator 
            initialRouteName={SCREENS.DASHBOARD}
            drawerContent={props => (
                <CustomDrawerContent navigation={navigation}/>
            )}
            screenOptions={{
                drawerPosition: 'right',
                headerLeft:false,
                headerTitleAlign: 'center',
                headerRight: () => (
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.navigate(localStrings.notifications)}>
                            <NotificationIcon/>
                        </TouchableOpacity>
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
                            <MenuIcon/>
                        </TouchableOpacity>
                    </View>
                ),
            }}
            > 
            <Drawer.Screen 
                name={SCREENS.DASHBOARD} 
                component={DashBoard}
                options={{
                    title: SCREENS.DASHBOARD,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<DashboardIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.addCase!} 
                component={addCase}
                options={{
                    title: localStrings.addCase,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (
                    <Ionicons
                        name="add-circle-outline"
                        size={size}
                        color={focused ? colors.primary : colors.gray}
                    />
                    ),
                }}
            />
            <Drawer.Screen 
                name={localStrings.bareActs!} 
                component={BareActs}
                options={{
                    title: localStrings.bareActs,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<BareActsIcon/>),
                }}
            />
            <Drawer.Screen 
                name={SCREENS.ARCHIVES} 
                component={Archives}
                options={{
                    title: SCREENS.ARCHIVES,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<ArchiveMenuIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.connectedMatters!} 
                component={ConnectedMatters}
                options={{
                    title: localStrings.connectedMatters,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (
                    <Feather
                        name="link"
                        size={size}
                        color={focused ? colors.primary : colors.gray}
                    />
                    ),
                }}
            />
            <Drawer.Screen 
                name={localStrings.limitationCalculator!} 
                component={Calculator}
                options={{
                    title: localStrings.limitationCalculator,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<CalculatorIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.myClients!} 
                component={MyClients}
                options={{
                    title: localStrings.myClients,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<MyClientsIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.worklist!} 
                component={Worklist}
                options={{
                    title: localStrings.worklist,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<WorkListIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.myAccount!} 
                component={MyAccounts}
                options={{
                    title: localStrings.myAccount,
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<MyAccountIcon/>),
                }}
            />
            <Drawer.Screen 
                name={localStrings.notifications!} 
                component={Notification}
                options={{
                    title: localStrings.notifications,
                    headerTitleStyle: {fontFamily: fonts.poppins.medium},
                    headerLeft: () => (
                        <TouchableOpacity  
                            style={styles.menuIcon}
                            onPress={() => navigation.goBack()}>
                            <BackButtonIcon/>
                        </TouchableOpacity>
                    ),
                    drawerIcon: ({focused, size}) => (<NotificationMenuIcon/>),
                }}
            />
            <Drawer.Screen 
                name={SCREENS.ACTIVE_CASES} 
                component={ActiveCases}
                options={{ headerShown: false }}
            />
        </Drawer.Navigator>
    );
};

export default HomeScreen;
