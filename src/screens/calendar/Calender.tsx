import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, Text, Dimensions, TouchableOpacity, ScrollView } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Calender.style";
import { localStrings } from "../../shared/localization";
import { Calendar } from "react-native-calendars";
import Icon from 'react-native-vector-icons/AntDesign';
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import { SCREENS } from "../../shared/constants";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const Calender: React.FC<ICardItemProps> = (props) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);
    const navigation = useNavigation();
    const data = props?.route?.params?.data;
    const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    const renderArrow = (direction: string) => {
        return (
            <View style={{backgroundColor: colors.iconWhite, elevation: 2}}>
                <Icon
                    name={direction === 'left' ? 'left' : 'right'}
                    size={22}
                    color={colors.primary}
                    style={styles.arrow}
                />
            </View>
        );
    };

    const renderHeader = (date: Date) => {
        const header = month[date.getMonth()];
        return (
          <View style={styles.header}>
            <TextWrapper style={styles.headerText} color={colors.text}>{header}</TextWrapper>
            <TextWrapper style={styles.headerText} color={colors.text}> {date.getFullYear()}</TextWrapper>
          </View>
        );
    };

    const calendarTheme = { 
        'stylesheet.calendar.header': {
            dayTextAtIndex0: {
                color: 'red'
            },
            dayTextAtIndex6: {
                color: 'blue'
            }
        }
    }

    const markedDates = {
        '2024-07-05': { marked: true, dotColor: '#00adf5', customStyles: { text: { marginTop: 2 } }, text: '3' },
        // Add more marked dates as needed
    };

    const CustomDayComponent = ({ date, state }) => {
        return (
          <View style={[styles.dayContainer, state === 'disabled' ? styles.disabledDay : null]}>
            <Text style={[styles.dayText, state === 'disabled' ? styles.disabledText : null]}>
              {date.day}
            </Text>
          </View>
        );
      };
    
    return (
        <ScrollView style ={styles.container}>
            <Calendar
                style={{
                    height: Dimensions.get('window').height
                }}
                onDayPress={(day: { dateString: React.SetStateAction<string>; }) => {
                    setSelected(day.dateString);
                }}
                renderArrow={renderArrow}
                renderHeader={renderHeader}
                markedDates={markedDates}
                dayComponent={({date, state}) => {
                    return (
                      <TouchableOpacity style={{
                            height: 80,
                            borderColor: colors.gray,
                            borderWidth: 1,
                            width: 55,
                            marginVertical: -10,
                            backgroundColor: date.day == 6 ? colors.primary : colors.iconWhite
                      }} onPress={() => {navigation.navigate(SCREENS.ACTIVE_CASES, {data: data})}}>
                        <Text style={{textAlign: 'center', marginTop: 10, color: state === 'disabled' ? 'gray' : date.day == 6 ? colors.white : colors.black}}>{date.day}</Text>

                        {(date.day == 6 || date.day == 14) && <View style={{padding: 5, backgroundColor: colors.iconWhite, borderRadius: 5,borderColor: colors.primary, borderWidth: 0.5, alignSelf: 'center', marginTop: 5}}>
                            <Text style={{color: colors.primary,}}>04</Text>
                        </View>}
                      </TouchableOpacity>
                    );
                }}
                theme = {{
                    'stylesheet.calendar.header': {
                        week: {
                            flexDirection: 'row',
                            justifyContent: 'space-evenly',
                        },
                        dayHeader: {
                            paddingVertical: 30,
                            borderColor: colors.gray,
                            borderWidth: 1,
                            width: 50,
                            textAlign: 'center',
                            fontFamily: fonts.poppins.semiBold,
                            color: colors.primary
                        }
                    },
                }}
                
            />
        </ScrollView>
    );
};

export default Calender;