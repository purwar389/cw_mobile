import { StyleSheet} from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import fonts from "../../shared/theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
      justifyContent: 'space-between'
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    separator: {
      height: 1,
      backgroundColor: colors.background
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 24,
      fontFamily: fonts.poppins.regular,
      fontWeight: '600',
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    labelStyle: {
      fontSize: 14,
      color: colors.primary,
      marginHorizontal: 5
    },
    openButton: {
      backgroundColor: '#2196F3',
      padding: 10,
      borderRadius: 5,
    },
    buttonText: {
      color: '#fff',
      fontSize: 18,
    },
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    dialogBox: {
      width: '80%',
      padding: 20,
      backgroundColor: colors.iconWhite,
      alignItems: 'center',
    },
    dialogText: {
      fontSize: 18,
      marginBottom: 20,
    },
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
    },
    buttonStyle: {height: 46, borderRadius: 100, paddingHorizontal: '10%'},
    cancelText: {
      fontSize: 16,
      color: '#000',
    },
    logoutButton: {
      backgroundColor: '#b71c1c',
      padding: 10,
      borderRadius: 5,
      width: '45%',
      alignItems: 'center',
    },
    logoutText: {
      fontSize: 16,
      color: '#fff',
    },
  });
};