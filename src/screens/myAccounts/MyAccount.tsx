import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, Pressable, Text, Modal, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./MyAccounts.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import FilledButton from "../../shared/components/buttons/filledButton";
import { SCREENS } from "../../shared/constants";
import ProfileIcon from "../../assets//icons/profileIcon";
import ResetPwdIcon from "../../assets//icons/resetPwdIcon";
import SupportIcon from "../../assets//icons/supportIcon";
import PlanDetailsIcon from "../../assets//icons/planDetailsIcon";
import DltAccountIcon from "../../assets//icons/dltAccountIcon";
import ProfileImageIcon from "../../assets//icons/profileImageIcon";
import fonts from "../../shared/theme/fonts";
import AsyncStorage from "@react-native-async-storage/async-storage";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import SelectButton from "../../shared/components/buttons/SelectButton";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

interface AccountOptionsProps {
    optionName: string;
    icon: React.ReactNode;
    navigateTo: string;
}


const MyAccounts: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.inProgress);
    const navigation = useNavigation();
    const [modalVisible, setModalVisible] = useState<boolean>(false);

    const Header = () => {
        return <View style={[styles.fdRow, { backgroundColor: colors.primary, paddingHorizontal: 10, paddingVertical: 20 }]}>
            <View style={{ marginEnd: 10 , marginStart: '3%'}}>
                <ProfileImageIcon />
            </View>
            <View>
                <TextWrapper color={colors.iconWhite} style={{ fontSize: 18, marginBottom: 10 }} fontFamily={fonts.poppins.medium}>Adv. Aditya Joshi</TextWrapper>
                <TextWrapper color={colors.iconWhite} style={{ fontSize: 14 }}>Plan: Standard
                    <Text style={{ fontSize: 14, fontFamily: fonts.poppins.semiBold, color: colors.iconWhite }}> | </Text>
                    Monthly </TextWrapper>
                <TextWrapper color={colors.iconWhite} style={{ fontSize: 16 }} viewStyle={{ justifyContent: 'center' }}>Cases Added:
                    <Text style={{ fontSize: 14, fontFamily: fonts.poppins.semiBold, color: colors.iconWhite }}> 35 </Text>
                    out of
                    <Text style={{ fontSize: 14, fontFamily: fonts.poppins.semiBold, color: colors.iconWhite }}> 200 </Text>
                </TextWrapper>
                <TextWrapper color={colors.iconWhite} style={{ fontSize: 14 }}>Date of Joining: 23rd April, 2023</TextWrapper>
            </View>
        </View>
    }

    const _handleLogout = async () => {
        closeModal()
        await AsyncStorage.clear()
        navigation.reset({
            index: 0,
            routes: [{ name: SCREENS.LOGIN_WITH_OTP }],
        })
    }

    const openModal = (): void => {
        setModalVisible(true);
    };

    const closeModal = (): void => {
        setModalVisible(false);
    };

    const AccountOptions: React.FC<AccountOptionsProps> = ({ iconName, optionName, icon, navigateTo }) => {
        return (
            <>
                <Pressable style={[styles.fdRow, { backgroundColor: colors.iconWhite, paddingStart: '5%', paddingVertical: 17 }]}
                    onPress={() => navigation.navigate(navigateTo)}>
                    <View style={{ marginRight: 10 }}>
                        {icon}
                    </View>
                    <TextWrapper style={{ fontSize: 15 }} color={colors.darkGray}>{optionName}</TextWrapper>
                </Pressable>
                <View style={styles.separator} />
            </>
        );
    };

    return (
        <View style={styles.container}>
            <View>
                <Header />
                <AccountOptions icon={<ProfileIcon />} optionName={localStrings.profile!} navigateTo={SCREENS.PROFILE} />
                <AccountOptions icon={<PlanDetailsIcon />} optionName={localStrings.planDetails!} navigateTo={SCREENS.PLAN_DETAILS} />
                <AccountOptions icon={<ResetPwdIcon />} optionName={localStrings.resetPassword!} navigateTo={SCREENS.RESET_PASSWORD} />
                <AccountOptions icon={<SupportIcon />} optionName={localStrings.support!} navigateTo={SCREENS.SUPPORT} />
                <AccountOptions icon={<DltAccountIcon />} optionName={localStrings.deleteAccount!} navigateTo={SCREENS.PROFILE} />
            </View>

            <Modal
                animationType="none"
                transparent={true}
                visible={modalVisible}
                onRequestClose={closeModal}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.dialogBox}>
                        <TextWrapper style={styles.dialogText} color={colors.darkGray} fontFamily={fonts.poppins.medium}>Do you want to Log out?</TextWrapper>

                        {/* Buttons for Cancel and Log out */}
                        <View style={styles.buttonContainer}>
                            {/* Cancel Button */}
                            <OutlinedButton 
                                label={localStrings.cancel} 
                                labelColor={colors.darkGray} 
                                style={styles.buttonStyle}
                                onPress={closeModal}
                            />
                            <SelectButton 
                                selected={true} 
                                label={localStrings.logout} 
                                style={styles.buttonStyle}
                                onPress={_handleLogout}
                            />
                        </View>
                    </View>
                </View>
            </Modal>
            <FilledButton
                label={localStrings.logout}
                style={{ width: '85%', alignSelf: 'center', height: 40, marginVertical: 20 }}
                labelFont={fonts.poppins.medium}
                labelStyle={{ fontSize: 18 }}
                onPress={openModal}
            />
        </View>
    );
};

export default MyAccounts;