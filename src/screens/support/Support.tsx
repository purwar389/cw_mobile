import { useMemo } from "react";
import { StyleProp, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./Support.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import fonts from "../../shared/theme/fonts";
import MailOpenIcon from "../../assets//icons/mailOpenIcon";
import CallIconBig from "../../assets//icons/callIconBig";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const Support: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={styles.container}>
            <TextWrapper color={colors.primary} fontFamily={fonts.poppins.semiBold} style={styles.contactUsText}>{localStrings.contactUs}</TextWrapper>
            <TextWrapper style={{ color: colors.gray, fontSize: 16 }} viewStyle={{width: '90%', alignSelf: 'center'}}>{localStrings.query_text}</TextWrapper>
            <OutlinedButton
                label="support@casewise.in"
                iconLeft={<View style={{ marginEnd: 10 }}>
                    <MailOpenIcon/>
                </View>}
                labelStyle={{fontSize: 16}}
                style={styles.outlinedBtnStyle}
            />
            <OutlinedButton
                label="+91 917 520 5423"
                iconLeft={<View style={{ marginEnd: 10 }}>
                    <CallIconBig/>
                </View>}
                labelStyle={{fontSize: 16}}
                style={styles.outlinedBtnStyle}
            />
        </View>
    );
}

export default Support;
