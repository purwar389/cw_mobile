import { useMemo } from "react";
import { Linking, StyleProp, View, ViewStyle } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "../tellUsWhoUR/TellUsWhoUR.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import fonts from "../../shared/theme/fonts";
import { SafeAreaView } from "react-native-safe-area-context";
import MailOpenIcon from "../../assets/icons/mailOpenIcon";
import CallIcon from "../../assets/icons/callIcon";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const ContactUs: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation()

    return (
        <SafeAreaView style={styles.container}>
            <TextWrapper color={colors.primary} fontFamily={fonts.poppins.bold} style={styles.casewiseText} viewStyle={{flex: 0.3, justifyContent: 'center'}}>{localStrings.casewise}</TextWrapper>
            <TextWrapper style={{ fontSize: 24, textAlign: 'center' }} fontFamily={fonts.poppins.semiBold} color={colors.gray} >{localStrings.contactUs}</TextWrapper>
            <TextWrapper color={colors.gray} fontFamily={fonts.poppins.regular} style={{fontSize: 16, textAlign: 'center'}} viewStyle={{width: '70%', alignSelf: 'center', marginTop: '5%'}}>{localStrings.weAreDelighted}</TextWrapper>
            <View style={{flex: 0.5}}>
                <OutlinedButton
                    label={'support@casewise.in'}
                    labelStyle={{fontSize: 16, marginStart: 10}}
                    iconLeft={<MailOpenIcon/>}
                    style={{...styles.outlinedBtnStyle, marginTop: '7%'}}
                    onPress={() => Linking.openURL('mailto:support@casewise.in')}
                />
                <OutlinedButton
                    label={'+91 917 520 5423'}
                    labelStyle={{fontSize: 16, marginStart: 10}}
                    iconLeft={<CallIcon width={20} height={20}/>}
                    style={styles.outlinedBtnStyle}
                    onPress={() => Linking.openURL(`tel:+919175205423`)}
                />
            </View>
            <OutlinedButton
                label={localStrings.back!}
                labelStyle={{fontSize: 20, textAlign: 'center'}}
                labelColor={colors.gray}
                borderWidth={1}
                style={styles.backBtnStyle}
                onPress={() => navigation.goBack()}
            />
        </SafeAreaView>
    );
}

export default ContactUs;
