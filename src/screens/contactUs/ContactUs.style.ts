import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
    container: ViewStyle;
    buttonsContainer: ViewStyle;
    mh5: ViewStyle;
    mh20: ViewStyle;
    outlinedBtnStyle: ViewStyle;
    backBtnStyle: ViewStyle;
    casewiseText: TextStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        mh5: {
            marginHorizontal: 5
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center'
        },
        casewiseText: {
            fontSize: 48,
            textAlign: 'center'
        },
        mh20: {
            marginHorizontal: 20
        },
        outlinedBtnStyle: {
            flexDirection: 'row',
            height: 46,
            width: '80%',
            alignSelf: 'center',
            justifyContent: 'flex-start',
            paddingHorizontal: 10,
            backgroundColor: colors.iconWhite,
          },
          backBtnStyle: {
            flexDirection: 'row',
            height: 46,
            width: '80%',
            alignSelf: 'center',
            paddingHorizontal: 10,
          }
    });
};
