import { useMemo, useState } from "react";
import { FlatList, ScrollView, StyleProp, TouchableOpacity, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./SortBy.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import AscendingIcon from "../../assets//icons/ascendingIcon";
import DescendingIcon from "../../assets//icons/descendingIcon";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const SortBy: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    const [data, setData] = useState([
        {
            name: localStrings.briefNumber,
            isSelected: true
        },
        {
            name: localStrings.caseNumber,
            isSelected: false
        },
        {
            name: localStrings.prevDate,
            isSelected: false
        },
        {
            name: localStrings.nextDate,
            isSelected: false
        },
        {
            name: localStrings.petitioner,
            isSelected: false
        },
        {
            name: localStrings.petitionerAdv,
            isSelected: false
        },
        {
            name: localStrings.respondent,
            isSelected: false
        },
        {
            name: localStrings.respondentsAdv,
            isSelected: false
        },
        {
            name: localStrings.courts,
            isSelected: false
        },
        {
            name: localStrings.caseStage,
            isSelected: false
        },
        {
            name: localStrings.judgeName,
            isSelected: false
        },
        {
            name: localStrings.briefFor,
            isSelected: false
        },
        {
            name: localStrings.organosation,
            isSelected: false
        },
        {
            name: localStrings.attendance,
            isSelected: true
        },
    ])

    const options = [
        {item: "Ascending"},
        {item: "Desending"}
    ]

    const handlePress = (index: number) => {
        const newData = data.map((item, i) => ({
            ...item,
            isSelected: i === index,
        }));
        setData(newData);
    };

    return (
        <View style={styles.container}>
            <TextWrapper color={colors.gray} fontFamily={fonts.poppins.medium} style={styles.contactUsText}>{localStrings.sortBy}</TextWrapper>
            
            <View style={{flexDirection: 'row'}}>
                <FlatList
                    style={{marginBottom: 60}}
                    data={data}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item, index}: {item: any, index: number}) => {
                        return <TouchableOpacity onPress={() => handlePress(index)}>
                            <TextWrapper 
                                fontFamily={fonts.poppins.regular} 
                                color={item.isSelected ? colors.iconWhite : colors.gray} 
                                viewStyle={{paddingHorizontal: 10, paddingVertical: 15, backgroundColor: item.isSelected ? colors.primary : colors.background}}>{item.name}</TextWrapper>
                        </TouchableOpacity>
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return <View style={{backgroundColor: colors.white, height:1}}/>
                    }}
                />
                <FlatList
                    data={options}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item, index}: {item: any, index: number}) => {
                        return <TextWrapper 
                            fontFamily={fonts.poppins.regular} 
                            color={item.isSelected ? colors.iconWhite : colors.gray} 
                            leftIcon={item.item == "Ascending" ? <AscendingIcon/> : <DescendingIcon/>}
                            style={{marginStart: 10}}
                            viewStyle={{paddingHorizontal: 10, paddingVertical: 15, backgroundColor: colors.iconWhite, flexDirection: 'row', alignItems: 'center'}}>{item.item}</TextWrapper>
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return <View style={{backgroundColor: colors.background, height:1, width: '90%', alignSelf: 'center'}}/>
                    }}
                />
            </View>
        </View>
    );
}

export default SortBy;
