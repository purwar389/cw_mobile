import { useMemo, useState } from "react";
import { Pressable, Text, ToastAndroid, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./LoginWithOtp.style";
import { localStrings } from "../../shared/localization";
import InputText from "../../shared/components/InputText";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import FilledButton from "../../shared/components/buttons/filledButton";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import { SCREENS } from "../../shared/constants";
import { API } from "../../services/apiservices/apiHandler";
import { emailRegex } from "../../utils";
import { AppConfig } from "../../shared/constants/config";

interface LoginScreenProps {}

interface UserData {
    email: string,
}

const LoginWithOtp : React.FC<LoginScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [email, setEmail] = useState<string>();
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const _handleContinueBtn = () => {
        navigation.navigate(SCREENS.LOGIN);
    }
    
    const _handleOtpBtn = () => {
        if(emailRegex.test(email!!)) {
            handleRequestOtp()
        }  
    }

    const handleRequestOtp = async () => {
        try {
            setIsLoading(true)
            console.log( AppConfig.API_URL)
            const apiEndpoint = `/api/request-otp`;
            const formData = { email };
            const response = await API.postAPIService(apiEndpoint, formData);
            setIsLoading(false)
            navigation.navigate(SCREENS.OTP_SCREEN, {email: email});
            ToastAndroid.show(response.otp.toString(), ToastAndroid.SHORT)
          
        } catch (error) {
            console.error(error)
          setIsLoading(false)
        }
    };
    
    return (
        <View style={styles.container}>
            <View style={{flex: 0.29, justifyContent: 'center', }}>
                <Text style={styles.titleText}>{localStrings.casewise}</Text>
            </View>
            <View style={{flex: 0.68, width: '90%', alignSelf: 'center'}}>
                <Text style={styles.welcomeText}>{localStrings.welcome}</Text>
                <InputText 
                    placeholder={localStrings.email} 
                    onChangeText={(text) => setEmail(text)}
                    value={email}
                    height={42}
                    style={{marginTop: 24}}
                    inputStyle={{...styles.bgWhite, lineHeight: 24}}
                    borderColor={colors.gray}
                    keyboardType="email-address"
                />
                <FilledButton 
                    label={localStrings.getOpt!} 
                    labelFont={fonts.poppins.regular}
                    labelStyle={{fontSize: 18}}
                    style={{marginHorizontal: 20, height: 42, marginTop: 20}}
                    onPress={_handleOtpBtn}
                    isLoading={isLoading}
                />

                <View style={styles.separator}>
                    <View style={styles.separatorLine}/>
                    <TextWrapper fontFamily={fonts.poppins.medium}>  or  </TextWrapper>
                    <View style={styles.separatorLine}/>
                </View>

                <OutlinedButton 
                    label={localStrings.continueWithCred!} 
                    style={styles.continueButton}
                    borderWidth={1}
                    labelStyle={{fontSize: 18}}
                    onPress={_handleContinueBtn}/>
                
                <View style={[styles.fdRowJcCenter, styles.mt40]}>
                    <TextWrapper color={colors.gray} fontFamily={fonts.poppins.medium} style={{fontSize: 13}}>{localStrings.dontHaveAcc}</TextWrapper>
                    <Pressable onPress={() => navigation.navigate(SCREENS.TELL_US_WHO_U_R)}>
                        <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium} style={{fontSize: 13}}>  {localStrings.createAnAcc}</TextWrapper>
                    </Pressable> 
                </View>
            </View>
        </View>
    );
}

export default LoginWithOtp;