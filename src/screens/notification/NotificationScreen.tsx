import React, { useMemo } from "react";
import { FlatList, View } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./NotificationScreen.style";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import TimerIcon from "../../assets//icons/timerIcon";

interface NotificationScreenProps { }

interface NotificationItem {
    title: string;
    desc: string;
    date: string;
    time: string;
}

const Notification: React.FC<NotificationScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    

    const data = [
        {
            title: 'DRAFT WS',
            desc: 'Sachin Sanjay Devprasad Singh VS \nShaileshkumar Devprasad Singh',
            date: '12th April 2024',
            time: '12:00 PM'
        },
        {
            title: 'DRAFT WS',
            desc: 'Sachin Sanjay Devprasad Singh VS \nShaileshkumar Devprasad Singh',
            date: '12th April 2024',
            time: '12:00 PM'
        },
        {
            title: 'DRAFT WS',
            desc: 'Sachin Sanjay Devprasad Singh VS \nShaileshkumar Devprasad Singh',
            date: '12th April 2024',
            time: '12:00 PM'
        },
        {
            title: 'DRAFT WS',
            desc: 'Sachin Sanjay Devprasad Singh VS \nShaileshkumar Devprasad Singh',
            date: '12th April 2024',
            time: '12:00 PM'
        },
        {
            title: 'DRAFT WS',
            desc: 'Sachin Sanjay Devprasad Singh VS \nShaileshkumar Devprasad Singh',
            date: '12th April 2024',
            time: '12:00 PM'
        },
    ]

    const renderNotifications = ({item, index} : {item: NotificationItem, index: number}) => {
        return <View style={[styles.mv10, {marginStart: 10}]}>
            <TextWrapper fontFamily={fonts.poppins.medium} color={colors.primary}>{item.title}</TextWrapper>
            <TextWrapper color={colors.darkGray} style={styles.mv10} fontFamily={fonts.poppins.medium}>{item.desc}</TextWrapper>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TimerIcon/>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TextWrapper color={colors.darkGray} viewStyle={{marginStart: 10}}>{item.date}</TextWrapper>
                    <View style={{width: 2, height: 15, backgroundColor: colors.primary, marginHorizontal: 10}}/>
                    <TextWrapper color={colors.darkGray}>{item.time}</TextWrapper>
                </View>
            </View>
        </View>
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                renderItem={renderNotifications}
                ItemSeparatorComponent={() => {
                    return <View style={{width: '100%', height: 1, backgroundColor: colors.white, }}/>
                }}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
};

export default Notification;
