import { useMemo, useState } from "react";
import { ActivityIndicator, Dimensions, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./Login.style";
import { localStrings } from "../../shared/localization";
import InputText from "../../shared/components/InputText";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import FilledButton from "../../shared/components/buttons/filledButton";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import { SCREENS } from "../../shared/constants";
import { API } from "../../services/apiservices/apiHandler";
import { emailRegex, validatePassword } from "../../utils/index";

interface LoginScreenProps {}

interface UserData {
    email: string,
    password: string,
}

const Login : React.FC<LoginScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [pwdError, setPwdError] = useState<string | null>("");
    const [emailError, setEmailError] = useState<string | null>("");
    const [userData, setUserData] = useState<UserData>({
        email: '',
        password: ''
    });

    const _handleChange = (text: string, field: keyof UserData) => {
        setUserData({...userData, [field]: text});
    }

    const _handleRegister = () => {
        navigation.navigate(SCREENS.TELL_US_WHO_U_R);
    }

    const _handleLogin = async() => {
        const validationError = validatePassword(userData.password);
        if (validationError) {
            setPwdError(validationError);
        } else {
            setPwdError("");
        }

        if(!emailRegex.test(userData.email)) {
            setEmailError('Please provide correct email.')
            return;
        }

        if(emailRegex.test(userData.email) && validatePassword(userData.password) == null) {
            try {
                setIsLoading(true)
                const apiEndpoint = `/api/login`;
                const formData = { email : userData.email, password: userData.password };
            
                // Make API request
                const response = await API.postAPIService(apiEndpoint, formData);
                setIsLoading(false)
                navigation.navigate(SCREENS.HOME);
            } catch (error) {
                setIsLoading(false)
            }
        }
    }
    
    return (
        <ScrollView style={styles.container}>
            {isLoading && <View style={{alignItems: 'center', position: 'absolute', justifyContent: 'center', height: '100%', width: '100%', zIndex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)'}}><ActivityIndicator size={'large'} color={colors.white}/></View>}
            <View style={{height: Dimensions.get('window').height * 0.28, justifyContent: 'center', }}>
                <Text style={styles.titleText}>{localStrings.casewise}</Text>
            </View>
            <View style={{flex: 0.72, width: '90%', alignSelf: 'center'}}>
                <Text style={styles.welcomeText}>{localStrings.welcome}</Text>
                <View style={{marginTop: 30}}>
                    <InputText 
                        placeholder={localStrings.email} 
                        onChangeText={(text) => _handleChange(text, 'email')}
                        value={userData.email}
                        height={42}
                        borderColor={emailError== "" ? colors.gray : colors.primary}
                        inputStyle={styles.bgWhite}
                        style={styles.mh5}
                    />
                    <InputText 
                        placeholder={localStrings.password} 
                        onChangeText={(text) => _handleChange(text, 'password')}
                        value={userData.password}
                        height={42}
                        borderColor={pwdError== "" ? colors.gray : colors.primary}
                        inputStyle={styles.bgWhite}
                        style={styles.mh5}
                    />
                </View>
                <TouchableOpacity style={styles.mh20} onPress={() => navigation.navigate(SCREENS.FORGOT_PASSWORD)}>
                    <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium}>{localStrings.forgot_pass}</TextWrapper>
                </TouchableOpacity>
                <View style={[styles.mh20, {marginTop: 20}]}>
                    <FilledButton 
                        label={localStrings.login!} 
                        style={{height: 42}} 
                        labelStyle={{fontSize: 18}}
                        labelFont={fonts.poppins.regular}
                        onPress={_handleLogin}
                    />
                </View>
                <View style={{marginTop: '15%'}}>
                    <TextWrapper color={colors.gray} style={styles.textGray16_500}>{localStrings.dontHaveAcc}</TextWrapper>
                    <View style={styles.mh20}>
                        <OutlinedButton 
                            label={localStrings.register!} 
                            onPress={_handleRegister} 
                            labelStyle={{fontSize: 18}}
                            style={{...styles.outlinedButtonStyle, height: 42}} 
                            borderWidth={1}
                        />
                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

export default Login;