import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
    container: ViewStyle;
    buttonsContainer: ViewStyle;
    mh5: ViewStyle;
    mh20: ViewStyle;
    outlinedBtnStyle: ViewStyle;
    contactUsText: TextStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        mh5: {
            marginHorizontal: 5
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center'
        },
        contactUsText: {
            fontSize: 16,
            margin: 15
        },
        mh20: {
            marginHorizontal: 20
        },
        outlinedBtnStyle: {
            flexDirection: 'row',
            paddingVertical: 5,
            marginHorizontal: 20,
            justifyContent: 'flex-start',
            paddingHorizontal: 10
        },
    });
};
