import { useMemo } from "react";
import { StyleProp, TouchableOpacity, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./Extract.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import CsvIcon from "../../assets//icons/csvIcon";
import PdfIconSmall from "../../assets//icons/pdfIconSmall";
import ShareIcon from "../../assets//icons/shareIcon";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const Extract: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    const data = [
        {
            name: localStrings.csv,
            icon: <CsvIcon/>
        },
        {
            name: localStrings.pdf,
            icon: <PdfIconSmall/>
        },
        {
            name: localStrings.shareTo,
            icon: <ShareIcon/>
        }
    ]

    return (
        <View style={styles.container}>
            <TextWrapper color={colors.gray} fontFamily={fonts.poppins.medium} style={{fontSize: 16}} viewStyle={{backgroundColor: colors.background, paddingHorizontal: 40, paddingVertical: 30, justifyContent: 'center'}}>{localStrings.extractAs}</TextWrapper>
            <View style={{backgroundColor: colors.iconWhite}}>
                {data.map((item) => {
                    return <TouchableOpacity>
                        <TextWrapper fontFamily={fonts.poppins.regular} color={colors.iconBlack} rightIcon={item.icon} viewStyle={{flexDirection: 'row', alignItems: 'center'}} style={{marginStart: 40, marginEnd: 20, marginVertical: 10, fontSize: 16}}>{item.name}</TextWrapper>
                        <View style={{backgroundColor: colors.background, height: 1, width: '90%', alignSelf: 'center'}}/>
                    </TouchableOpacity>
                })}
            </View>
        </View>
    );
}

export default Extract;
