import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";
import { color } from "react-native-reanimated";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    separator: {
        height: 1, // Height of the separator
        backgroundColor: '#CED0CE',
        marginVertical: 10
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 24,
      fontFamily: fonts.poppins.regular,
      fontWeight: '600',
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    labelStyle: {
        fontSize: 14,
        marginHorizontal: 5
      },
  });
};