import React, { useMemo, useState } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text, FlatList, ScrollView } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Calculator.style";
import SelectButton from "../../shared/components/buttons/SelectButton";
import { localStrings } from "../../shared/localization";
import LimitationDate from "./components/limitationDate/LimitationDate";
import Calculation from "./components/calculation/Calculation";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const Calculator: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [screen, setScreen] = useState(localStrings.limitationDate!);

    return (
        <ScrollView style ={styles.container}>
            <View style={[styles.fdRow, {marginHorizontal: 15}]}>
                <SelectButton 
                    label={localStrings.limitationDate!}
                    selected={screen == localStrings.limitationDate} 
                    labelStyle={styles.labelStyle} 
                    outlineColor={colors.gray}
                    style={{ flex: 1 , height: 45}} 
                    onPress={() => setScreen(localStrings.limitationDate!)}
                />
                <SelectButton
                    label={localStrings.calculationOfDays!}
                    selected={screen == localStrings.calculationOfDays} 
                    labelStyle={styles.labelStyle} 
                    outlineColor={colors.gray}
                    style={{ flex: 1 , height: 45 }} 
                    onPress={() => setScreen(localStrings.calculationOfDays!)}
                />
            </View>
            {screen == localStrings.limitationDate && <LimitationDate cnrNumber={""} onChangeCNR={() => {}}/>}
            {screen == localStrings.calculationOfDays && <Calculation cnrNumber={""} onChangeCNR={() => {}}/>}
        </ScrollView>
    );
};

export default Calculator;