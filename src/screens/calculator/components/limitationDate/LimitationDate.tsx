import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Limitation.style";
import InputText from "../../../../shared/components/InputText";
import { localStrings } from "../../../../shared/localization";
import RoundedButton from "../../../../shared/components/buttons/RoundedButton";
import DropDown from "../../../../shared/components/DropDown";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const LimitationDate: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const {colors} = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={style}>
            <InputText
                placeholder={localStrings.date}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={{backgroundColor: colors.white}}
            /> 
            <DropDown
                data={['Number of Days', 'Number of Weeks','Number of Months' ,'Number of Years']}
                label={localStrings.noOfDays}
                labelColor={colors.lightGray}
                style={{width: '90%', alignSelf: 'center', marginVertical: 10}}
                onChangeText={onChangeCNR}
                value={cnrNumber}
            /> 
            <InputText
                placeholder={localStrings.enterNoOfDays}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={{backgroundColor: colors.white}}
            /> 
            <View style={{alignItems: 'center'}}>
                <RoundedButton label={localStrings.calculate!} 
                    btnType="filled" 
                    labelStyle={{fontSize: 16}} 
                    style={{ height: 46, borderRadius: 100 , marginVertical: 30}}
                />
                <InputText
                    placeholder={localStrings.dateFormat}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    inputStyle={{backgroundColor: colors.white}}
                /> 
            </View>
        </View>
    );
};

export default LimitationDate;