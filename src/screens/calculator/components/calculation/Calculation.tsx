import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, TextInput, Text } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./Calculation.style";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";
import InputText from "../../../../shared/components/InputText";
import { localStrings } from "../../../../shared/localization";
import RoundedButton from "../../../../shared/components/buttons/RoundedButton";
import fonts from "../../../../shared/theme/fonts";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const Calculation: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const {colors} = theme;

    return (
        <View style={style}>
            <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20}}>
                <TextWrapper style={{fontWeight: '500', fontSize: 16, fontFamily: fonts.poppins.medium}} color={colors.text}>Start Date</TextWrapper>
                <InputText
                    placeholder={localStrings.dateFormat}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    style={{marginHorizontal: -20, marginTop: 10}}
                    inputStyle={styles.bgWhite}
                /> 
            </View>
            <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20}}>
                <TextWrapper style={{fontWeight: '500', fontSize: 16, fontFamily: fonts.poppins.medium}} color={colors.text}>Certified Copy Applied</TextWrapper>
                <InputText
                    placeholder={localStrings.dateFormat}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    style={{marginHorizontal: -20, marginTop: 10}}
                    inputStyle={styles.bgWhite}
                /> 
            </View>
            <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20}}>
                <TextWrapper style={{fontWeight: '500', fontSize: 16, fontFamily: fonts.poppins.medium}} color={colors.text}>Certified Copy Received</TextWrapper>
                <InputText
                    placeholder={localStrings.dateFormat}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    style={{marginHorizontal: -20, marginTop: 10}}
                    inputStyle={styles.bgWhite}
                />
            </View>
            <View style={{flexDirection: 'row',alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 20}}>
                <TextWrapper style={{fontWeight: '500', fontSize: 16, fontFamily: fonts.poppins.medium}} color={colors.text}>End Date</TextWrapper>
                <InputText
                    placeholder={localStrings.dateFormat}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    style={{marginHorizontal: -20, marginTop: 10}}
                    inputStyle={styles.bgWhite}
                /> 
            </View>
            
            <View style={{alignItems: 'center'}}>
                <RoundedButton 
                    label={localStrings.calculate!} 
                    btnType="filled"
                    labelFont={fonts.poppins.regular}
                    labelStyle={{fontSize: 16}} 
                    style={{ height: 46, borderRadius: 100 , marginVertical: 30}}/>
                <InputText
                    placeholder={localStrings.yrsMonthsDays}
                    onChangeText={onChangeCNR}
                    value={cnrNumber}
                    inputStyle={{backgroundColor: colors.white}}
                /> 
            </View>
            
            <View style={[styles.fdRow, {margin: 20, alignItems: 'center'}]}>
                <TextWrapper color={colors.text} viewStyle={{flex:1}}>Duration between Certified Copy Applied and Ready</TextWrapper>
                <InputText placeholder="XX Days" onChangeText={() => {}} value={''} style={{flex: 1, marginHorizontal: -20}} inputStyle={styles.bgWhite}/>
            </View>
        </View>
    );
};

export default Calculation;