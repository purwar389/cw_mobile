import React, {useMemo, useState} from 'react';
import {SafeAreaView, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import createStyles from './addCase.style';
import SelectButton from '../../shared/components/buttons/SelectButton';
import CnrNumber from './components/CnrNumber/CnrNumber';
import CaseNumber from './components/CaseNumber/CaseNumber';
import CustomCase from './components/CustomCase/CustomCase';

interface AddCaseScreenProps {}

const AddCase: React.FC<AddCaseScreenProps> = () => {
  const theme = useTheme();
  const {colors} = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  const [cnr, setCnr] = useState('');
  const [screen, setScreen] = useState('cnr');

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flexDirection: 'row', marginHorizontal: 15}}>
        <SelectButton
          label="CNR"
          selected={screen == 'cnr'}
          labelStyle={{fontSize: 14, fontWeight: '500'}}
          style={{flex: 1, paddingHorizontal: 0, height: 45}}
          outlineColor={colors.gray}
          onPress={() => setScreen('cnr')}
        />
        <SelectButton
          label="Case Number"
          selected={screen == 'caseNumber'}
          labelStyle={{fontSize: 14, fontWeight: '500'}}
          style={{flex: 1, paddingHorizontal: 0, height: 45}}
          outlineColor={colors.gray}
          onPress={() => setScreen('caseNumber')}
        />
        <SelectButton
          label="Custom Case"
          selected={screen == 'customCase'}
          outlineColor={colors.gray}
          labelStyle={{fontSize: 14, fontWeight: '500'}}
          style={{flex: 1, paddingHorizontal: 0, height: 45}}
          onPress={() => setScreen('customCase')}
        />
      </View>

      {screen == 'cnr' && (
        <CnrNumber cnrNumber={cnr} onChangeCNR={item => setCnr(item)} />
      )}
      {screen == 'caseNumber' && (
        <CaseNumber cnrNumber={cnr} onChangeCNR={item => setCnr(item)} />
      )}
      {screen == 'customCase' && (
        <CustomCase cnrNumber={cnr} onChangeCNR={item => setCnr(item)} />
      )}
    </SafeAreaView>
  );
};

export default AddCase;
