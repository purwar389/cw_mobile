import React, { useMemo, useRef, useState } from 'react';
import { View, StyleProp, ViewStyle, ScrollView, TouchableOpacity, TextInput, Dimensions, Pressable } from 'react-native';
import { useTheme } from '@react-navigation/native';
import createStyles from './CustomeCase.style';
import TextWrapper from '../../../../shared/components/text-wrapper/TextWrapper';
import InputText from '../../../../shared/components/InputText';
import { localStrings } from '../../../../shared/localization';
import RoundedButton from '../../../../shared/components/buttons/RoundedButton';
import DropDown from '../../../../shared/components/DropDown';
import AddMoreDetailCard from '../../../../shared/components/addMoreDetailCard/AddMoreDetailCard';
import fonts from '../../../../shared/theme/fonts';
import AddDocIcon from '../../../../assets/icons/addDocIcon';
import DocIcon from '../../../../assets/icons/docIcon';
import TrashIcon from '../../../../assets/icons/trashIcon';
import { actions, RichToolbar } from 'react-native-pell-rich-editor';
import CalendarIcon from '../../../../assets/icons/calendarIcon';
import SwitchToggle from '@imcarlosguerrero/react-native-switch-toggle';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const CustomCase: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const richText = useRef();
    const [toggleMode, setToggleMode] = useState<boolean>(false);

    const inputFields = [
        { placeholder: localStrings.briefNumber, keyboardType: 'default' as const },
        { placeholder: localStrings.state, keyboardType: 'default' as const },
        { placeholder: localStrings.district, keyboardType: 'default' as const },
        { placeholder: localStrings.courtbench, keyboardType: 'default' as const },
        { placeholder: localStrings.caseType, keyboardType: 'default' as const },
        { placeholder: localStrings.caseNumber, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.caseYear, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.prevDate, keyboardType: 'default' as const },
        { placeholder: localStrings.nextDate, keyboardType: 'default' as const },
        { placeholder: localStrings.petitioner, keyboardType: 'default' as const },
        { placeholder: localStrings.petitionerAdv, keyboardType: 'default' as const },
        { placeholder: localStrings.respondent, keyboardType: 'default' as const },
        { placeholder: localStrings.respondentsAdv, keyboardType: 'default' as const },
        { placeholder: localStrings.judgeName, keyboardType: 'default' as const },
        { placeholder: localStrings.courtRoomNumber, keyboardType: 'default' as const },
        { placeholder: localStrings.caseStage, keyboardType: 'default' as const },
        { placeholder: localStrings.srnoInCourt, keyboardType: 'default' as const },
        { placeholder: localStrings.briefFor, keyboardType: 'default' as const },
        { placeholder: localStrings.organosation, keyboardType: 'default' as const },
        { placeholder: localStrings.reference, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.policeStation, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.clientName, keyboardType: 'default' as const },
        { placeholder: localStrings.decidedCase, keyboardType: 'default' as const },
        { placeholder: localStrings.abondonedCase, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.remarks, keyboardType: 'number-pad' as const },
        { placeholder: localStrings.addDocument, keyboardType: 'number-pad' as const },
    ];

    return (
        <ScrollView style={style}>
            <InputText value={'MHNG0001543824'}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.briefNumber}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.state}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.district}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />

            <DropDown label={localStrings.tabs} labelColor={colors.lightGray} onChangeText={onChangeCNR} data={['fefe', 'fefgtht', 'nhjmnh']} style={{ marginHorizontal: 20, marginVertical: 5 }} />

            <InputText
                placeholder={localStrings.courtbench}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.caseType}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.caseNumber}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <InputText
                placeholder={localStrings.caseYear}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }} />
            <View style={{ borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, paddingHorizontal: 10, marginVertical: 5, width: '90%', alignSelf: 'center', backgroundColor: colors.iconWhite, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <TextInput placeholder={localStrings.prevDate} editable={false} />
                <Pressable>
                    <CalendarIcon />
                </Pressable>
            </View>
            <View style={{ borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, paddingHorizontal: 10, marginVertical: 5, width: '90%', alignSelf: 'center', backgroundColor: colors.iconWhite, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <TextInput placeholder={localStrings.nextDate} editable={false} />
                <Pressable>
                    <CalendarIcon />
                </Pressable>
            </View>
            <AddMoreDetailCard labels={['Petitioner Name', 'Petitioner’s Advocate Name']} title={'Petitioner details'} />
            <AddMoreDetailCard labels={[localStrings.respondent!, localStrings.respondentsAdv!]} title={'Respondent details'} />
            <InputText
                placeholder={localStrings.judgeName}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <InputText
                placeholder={localStrings.courtRoomNumber}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <InputText
                placeholder={localStrings.caseStage}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <InputText
                placeholder={localStrings.srnoInCourt}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <InputText
                placeholder={localStrings.briefFor}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <AddMoreDetailCard labels={[localStrings.organosation!]} title={'Organization details'} isDropDown={true} />
            <AddMoreDetailCard labels={[localStrings.reference!]} title={'Reference details'} isDropDown={true} />
            <InputText
                placeholder={localStrings.policeStation}
                onChangeText={onChangeCNR}
                inputStyle={{ backgroundColor: colors.white }}
                style={{ marginVertical: 5 }}
            />
            <AddMoreDetailCard labels={[localStrings.clientName!]} title={'Client details'} isDropDown={true} />
            <View style={{ backgroundColor: colors.iconWhite, padding: 10, marginHorizontal: 20, borderColor: colors.primary, borderWidth: 0.5, borderRadius: 5, marginVertical: 5 }}>
                <TextWrapper fontFamily={fonts.poppins.regular} color={colors.darkGray} style={{ marginVertical: 5 }}>Assigned to</TextWrapper>
                <DropDown data={['Shivani Suryavanshi', 'Ajinkya Joshi']} label="Name" style={{ marginVertical: 5 }} backgroundColor={colors.background} />
            </View>

            <View style={{ flexDirection: 'row', padding: 10, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, backgroundColor: colors.iconWhite, marginHorizontal: 20, justifyContent: 'space-between', marginVertical: 5 }}>
                <TextWrapper>{localStrings.decidedCase}</TextWrapper>
                <SwitchToggle
                    switchOn={toggleMode}
                    onPress={() => setToggleMode(!toggleMode)}
                    circleColorOff={colors.background}
                    circleColorOn={colors.primary}
                    backgroundColorOff={colors.background}
                    backgroundColorOn={colors.background}
                    containerStyle={{
                        width: 51,
                        height: 22,
                        borderRadius: 25,
                        padding: 5,
                        borderColor: toggleMode ? colors.primary : colors.darkGray,
                        borderWidth: 0.5
                      }}
                      circleStyle={{
                        width: 16,
                        height: 16,
                        borderRadius: 20,
                        borderColor: toggleMode ? null : colors.darkGray,
                        borderWidth: 0.5
                      }}
                />
            </View>

            <View style={{ flexDirection: 'row', padding: 10, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, backgroundColor: colors.iconWhite, marginHorizontal: 20, justifyContent: 'space-between', marginVertical: 5 }}>
                <TextWrapper>{localStrings.abondonedCase}</TextWrapper>
                <SwitchToggle
                    switchOn={toggleMode}
                    onPress={() => setToggleMode(!toggleMode)}
                    circleColorOff={colors.background}
                    circleColorOn={colors.primary}
                    backgroundColorOff={colors.background}
                    backgroundColorOn={colors.background}
                    containerStyle={{
                        width: 51,
                        height: 22,
                        borderRadius: 25,
                        padding: 5,
                        borderColor: toggleMode ? colors.primary : colors.darkGray,
                        borderWidth: 0.5
                      }}
                      circleStyle={{
                        width: 16,
                        height: 16,
                        borderRadius: 20,
                        borderColor: toggleMode ? null : colors.darkGray,
                        borderWidth: 0.5
                      }}
                />
            </View>
            <InputText
                placeholder={localStrings.remarks}
                onChangeText={onChangeCNR}
                inputStyle={{ ...styles.bgWhite, paddingVertical: 10 }}
                style={{ marginVertical: 5 }}
                numberOfLines={3}
                height={100}
            />

            <View style={{ marginHorizontal: 20, backgroundColor: colors.iconWhite, borderRadius: 10, borderWidth: 0.5, marginVertical: 5, borderColor: colors.primary, justifyContent: 'flex-start' }}>
                <RichToolbar
                    editor={richText}
                    selectedIconTint="#873c1e"
                    iconTint="#312921"
                    actions={[
                        actions.insertImage,
                        actions.setBold,
                        actions.setItalic,
                        actions.insertBulletsList,
                        actions.insertOrderedList,
                        actions.insertLink,
                        actions.setStrikethrough,
                        actions.setUnderline,
                    ]}
                    style={styles.richTextToolbarStyle}
                />
                <View style={{}}>
                    <TextInput
                        placeholder={localStrings.title}
                        onChangeText={onChangeCNR}
                        style={{ fontSize: 20, fontWeight: '500', marginStart: 10 }}
                        placeholderTextColor={colors.lightGray}
                        numberOfLines={1}
                    />
                </View>

                <View>
                    <TextInput
                        placeholder={localStrings.note}
                        onChangeText={onChangeCNR}
                        style={{ marginVertical: 5, fontSize: 16, fontWeight: '400', marginStart: 10 }}
                        textAlignVertical="top"
                        placeholderTextColor={colors.lightGray}
                        numberOfLines={5}
                    />
                </View>

            </View>
            <View style={{ padding: 10, backgroundColor: colors.iconWhite, marginHorizontal: 20, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, marginVertical: 5 }}>
                <TextWrapper fontFamily={fonts.poppins.regular} color={colors.darkGray} style={{ marginVertical: 5 }}>Documents</TextWrapper>
                <TouchableOpacity style={{ backgroundColor: colors.cardBg, borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10, height: 46 }}>
                    <AddDocIcon />
                    <TextWrapper color={colors.darkGray} fontFamily={fonts.poppins.regular} style={styles.ms10}>{localStrings.addNewDoc}</TextWrapper>
                </TouchableOpacity>

                <TouchableOpacity style={{ backgroundColor: colors.cardBg, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10, height: 46, borderRadius: 5 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <DocIcon />
                        <TextWrapper color={colors.darkGray} fontFamily={fonts.poppins.regular} style={styles.ms10}>Note 1</TextWrapper>
                    </View>
                    <TrashIcon width={16} height={18}/>
                </TouchableOpacity>
            </View>

            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: Dimensions.get('window').width * 0.6, alignSelf: 'center', marginTop: 20 }}>
                <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{ marginHorizontal: 5, height: 37, flex: 1 }} labelStyle={{ fontSize: 16 }} labelFont={fonts.poppins.regular} />
                <RoundedButton label="Add" btnType="filled" style={{ marginHorizontal: 5, height: 37, flex: 1 }} labelStyle={{ fontSize: 16 }} labelFont={fonts.poppins.regular} />
            </View>
        </ScrollView>
    );
};

export default CustomCase;
