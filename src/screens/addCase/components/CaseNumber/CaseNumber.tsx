import React, { useMemo } from 'react';
import { View, StyleProp, ViewStyle, TextInput, Text, Dimensions } from 'react-native';
import { useTheme } from '@react-navigation/native';
/**
 * ? Local Imports
 */
import createStyles from './CaseNumber.style';
import InputText from '../../../../shared/components/InputText';
import { localStrings } from '../../../../shared/localization/index';
import RoundedButton from '../../../../shared/components/buttons/RoundedButton';
import DropDown from '../../../../shared/components/DropDown';
import fonts from '../../../../shared/theme/fonts';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const CaseNumber: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;

    return (
        <View style={style}>
            <DropDown
                placeholder={localStrings.hearingCourt}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                label={localStrings.hearingCourt}
                labelColor={colors.lightGray}
                style={{marginHorizontal: 20, marginVertical: 5}}
                data={['fefe','fefgtht', 'nhjmnh']}
            />  
            <DropDown
                placeholder={localStrings.state}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                labelColor={colors.lightGray}
                label={localStrings.state}
                style={{marginHorizontal: 20, marginVertical: 5}}
                data={['fefe','fefgtht', 'nhjmnh']}
            />  
            <DropDown
                placeholder={localStrings.district}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                labelColor={colors.lightGray}
                label={localStrings.district}
                style={{marginHorizontal: 20, marginVertical: 5}}
                data={['fefe','fefgtht', 'nhjmnh']}
            />  
            <DropDown
                placeholder={localStrings.courtbench}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                label={localStrings.courtbench}
                labelColor={colors.lightGray}
                style={{marginHorizontal: 20, marginVertical: 5}}
                data={['fefe','fefgtht', 'nhjmnh']}
            />  
            <InputText
                placeholder={localStrings.caseType}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={{backgroundColor: colors.white}}
                style={{marginVertical: 5}}
            />  
            <InputText
                placeholder={localStrings.caseNumber}
                keyboardType="number-pad"
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={{backgroundColor: colors.white}}
                style={{marginVertical: 5}}
            />  
            <InputText
                placeholder={localStrings.caseYear}
                keyboardType="number-pad"
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={{backgroundColor: colors.white}}
                style={{marginVertical: 5}}
            />  
            <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'center', marginTop: 20, width: Dimensions.get('window').width * 0.6, alignSelf: 'center'}}>
                <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{marginHorizontal: 5, height: 37, flex: 1}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
                <RoundedButton label="Fetch" btnType="filled" style={{marginHorizontal: 5, height: 37, flex: 1}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
            </View>
        </View>
    );
};

export default CaseNumber;