import { ScreenWidth } from "@freakycoder/react-native-helpers";
import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
  container: ViewStyle;
  textInput: ViewStyle;
  inputStyle: ViewStyle;
}

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create<Style>({
    container: {
      padding: 16,
      marginTop: 16,
      borderWidth: 1,
      borderRadius: 8,
      width: ScreenWidth * 0.9,
      borderColor: colors.borderColor,
      backgroundColor: colors.dynamicBackground,
    },
    textInput: {
        backgroundColor: colors.iconWhite,
        marginHorizontal: 20,
        marginVertical: 5,
        paddingHorizontal: 10,
        borderColor: colors.primary,
        borderWidth: 0.5,
        borderRadius: 5,
    },
    inputStyle: { 
        backgroundColor: colors.white , 
        height: 40 
    }
  });
};
