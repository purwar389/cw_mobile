import React, { useEffect, useMemo, useRef, useState } from "react";
import { View, StyleProp, ViewStyle, Dimensions, TextInput, Pressable, TouchableOpacity, ScrollView, Platform } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./CnrNumber.style";
import InputText from "../../../../shared/components/InputText";
import RoundedButton from "../../../../shared/components/buttons/RoundedButton";
import fonts from "../../../../shared/theme/fonts";
import { API } from "@services/apiservices/apiHandler";
import { localStrings } from "shared/localization";
import CalendarIcon from "assets/icons/calendarIcon";
import AddMoreDetailCard from "@shared-components/addMoreDetailCard/AddMoreDetailCard";
import TextWrapper from "@shared-components/text-wrapper/TextWrapper";
import DropDown from "@shared-components/DropDown";
import SwitchToggle from "@imcarlosguerrero/react-native-switch-toggle";
import { actions, RichToolbar } from "react-native-pell-rich-editor";
import AddDocIcon from "assets/icons/addDocIcon";
import DocIcon from "assets/icons/docIcon";
import TrashIcon from "assets/icons/trashIcon";
import RNDateTimePicker, { DateTimePickerAndroid, DateTimePickerEvent } from "@react-native-community/datetimepicker";
import { formatDate } from "@utils";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
}

const CnrNumber: React.FC<ICardItemProps> = ({ style, cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [caseData, setCaseData] = useState();
    const [cnrid, setCnr] = useState<string>();
    const [toggleMode, setToggleMode] = useState<boolean>(false);
    const [petitionerData, setPetitionerData] = useState();
    const richText = useRef();
    const [show, setShow] = useState(false);
    const [date, setDate] = useState(new Date());
    const [selectedDateType, setSelectedDateType] = useState<'previous_date' | 'listingDate' | null>(null);
    const [selectedDate, setSelectedDate] = useState<Date>(new Date());

    const onChange = (event: DateTimePickerEvent, selectedDate: Date | undefined) => {
        const currentDate = selectedDate || new Date();
        setShow(Platform.OS === 'ios' ? true : false);
        const formattedDate = formatDate(currentDate);

        if (selectedDateType === 'previous_date') {
          setCaseData((prevState) => ({
            ...prevState,
            data: {
              ...prevState.data,
              previous_date: formattedDate
            },
          }));
        } else if (selectedDateType === 'listingDate') {
            setCaseData((prevState) => ({
            ...prevState,
            data: {
              ...prevState.data,
              listingDate: formattedDate
            },
          }));
        }
    };

    const showDatePicker = (dateType: string) => {
        setSelectedDateType(dateType);
        console.log('1234567890 -->'+caseData.data[dateType])
        setSelectedDate(new Date(caseData.data[dateType]));
        setShow(true);
    };


    const fetchByCnr = async () => {
        try {
            setIsLoading(true);
            const apiEndpoint = '/api/cases/fetch-by-cnr';
            const response = await API.postAPIService(apiEndpoint, {cnrid: cnrid});
            const { petitioners, padvocates } = response.data;
            const combinedList = combinePetitionersAndAdvocates(petitioners, padvocates);
            console.log('combinedList --> '+JSON.stringify(combinedList))
            setPetitionerData(combinedList);

            setCaseData(response);
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        fetchByCnr();
    }, [])

    const _handleInputText = (text: string) => setCnr(text);

    const combinePetitionersAndAdvocates = (petitioners, padvocates) =>
        petitioners.map((petitioner: string, index: number) => ({
          name: petitioner,
          advocateName: padvocates[index] || ''
        })
    );

    return (
        <View style={style}>  
                <ScrollView>
                    <InputText
                        placeholder="CNR number"
                        onChangeText={_handleInputText}
                        value={cnrid}
                        placeholderTextColor={colors.lightGray}
                        inputStyle={styles.inputStyle}
                    />
                {caseData!= null &&
                    <View>
                        <InputText
                            placeholder={localStrings.briefNumber}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.state}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.district}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <DropDown 
                            label={localStrings.tabs} 
                            labelColor={colors.lightGray} 
                            data={['District Courts & Tribunals', 'High Courts & Supreme Court']} 
                            style={{ marginHorizontal: 20, marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.courtbench}
                            onChangeText={(item: string) => {

                            }}
                            value={caseData?.data.courtName}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.caseType}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.caseTypeStr}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.caseNumber}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.cn}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }} />

                        <InputText
                            placeholder={localStrings.caseYear}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.cy}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5}} />

                        {show && (
                            <RNDateTimePicker 
                                value={date}
                                onChange={onChange}
                                onTouchCancel={() => setShow(false)}
                            />
                        )}


                        <Pressable style={{ height: 40, borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, paddingHorizontal: 10, marginVertical: 5, width: '90%', alignSelf: 'center', backgroundColor: colors.iconWhite, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
                            onPress={() => showDatePicker('previous_date')}>
                            <TextWrapper>{caseData?.data.previous_date}</TextWrapper>
                            <CalendarIcon />
                        </Pressable>

                        <Pressable style={{ height: 40, borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, paddingHorizontal: 10, marginVertical: 5, width: '90%', alignSelf: 'center', backgroundColor: colors.iconWhite, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
                            onPress={() => showDatePicker('listingDate')}>
                            <TextWrapper>{caseData?.data.listingDate}</TextWrapper>
                            <CalendarIcon />
                        </Pressable>

                        {petitionerData?.map((item, index: number) => (
                            <AddMoreDetailCard 
                                key={index}
                                labels={[item.name, item.advocateName]} 
                                title={'Petitioner details'} 
                                isButton={index + 1 === petitionerData?.length} 
                                isLabelAValue={true}
                            />
                        ))}

                        
                        <AddMoreDetailCard labels={[localStrings.respondent!, localStrings.respondentsAdv!]} title={'Respondent details'} />
                        
                        <InputText
                            placeholder={localStrings.judgeName}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                            value={caseData?.data.judgeName}
                        />
                        
                        <InputText
                            placeholder={localStrings.courtRoomNumber}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.courtRoomNo}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                        />
                        
                        <InputText
                            placeholder={localStrings.caseStage}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.listingStage}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                        />
                        
                        <InputText
                            placeholder={localStrings.srnoInCourt}
                            onChangeText={onChangeCNR}
                            value={caseData?.data.causeLists[0].Sno}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                        />
                        
                        <InputText
                            placeholder={localStrings.briefFor}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                        />
                        
                        <AddMoreDetailCard labels={[localStrings.organosation!]} title={'Organization details'} isDropDown={true} />
                        
                        <AddMoreDetailCard labels={[localStrings.reference!]} title={'Reference details'} isDropDown={true} />
                        
                        <InputText
                            placeholder={localStrings.policeStation}
                            onChangeText={onChangeCNR}
                            inputStyle={styles.inputStyle}
                            style={{ marginVertical: 5 }}
                        />
                        
                        <AddMoreDetailCard labels={[localStrings.clientName!]} title={'Client details'} isDropDown={true} />
                        
                        <View style={{ backgroundColor: colors.iconWhite, padding: 10, marginHorizontal: 20, borderColor: colors.primary, borderWidth: 0.5, borderRadius: 5, marginVertical: 5 }}>
                            <TextWrapper fontFamily={fonts.poppins.regular} color={colors.darkGray} style={{ marginVertical: 5 }}>Assigned to</TextWrapper>
                            <DropDown data={['Shivani Suryavanshi', 'Ajinkya Joshi']} label="Name" style={{ marginVertical: 5 }} backgroundColor={colors.background} />
                        </View>

                        <View style={{ flexDirection: 'row', padding: 10, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, backgroundColor: colors.iconWhite, marginHorizontal: 20, justifyContent: 'space-between', marginVertical: 5 }}>
                            <TextWrapper>{localStrings.decidedCase}</TextWrapper>
                            <SwitchToggle
                                switchOn={caseData?.data.decided == "Y"}
                                onPress={() => setToggleMode(!toggleMode)}
                                circleColorOff={colors.background}
                                circleColorOn={colors.primary}
                                backgroundColorOff={colors.background}
                                backgroundColorOn={colors.background}
                                containerStyle={{
                                    width: 51,
                                    height: 22,
                                    borderRadius: 25,
                                    padding: 5,
                                    borderColor: toggleMode ? colors.primary : colors.darkGray,
                                    borderWidth: 0.5
                                }}
                                circleStyle={{
                                    width: 16,
                                    height: 16,
                                    borderRadius: 20,
                                    borderColor: toggleMode ? null : colors.darkGray,
                                    borderWidth: 0.5
                                }}
                            />
                        </View>

                        <View style={{ flexDirection: 'row', padding: 10, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, backgroundColor: colors.iconWhite, marginHorizontal: 20, justifyContent: 'space-between', marginVertical: 5 }}>
                            <TextWrapper>{localStrings.abondonedCase}</TextWrapper>
                            <SwitchToggle
                                switchOn={toggleMode}
                                onPress={() => setToggleMode(!toggleMode)}
                                circleColorOff={colors.background}
                                circleColorOn={colors.primary}
                                backgroundColorOff={colors.background}
                                backgroundColorOn={colors.background}
                                containerStyle={{
                                    width: 51,
                                    height: 22,
                                    borderRadius: 25,
                                    padding: 5,
                                    borderColor: toggleMode ? colors.primary : colors.darkGray,
                                    borderWidth: 0.5
                                }}
                                circleStyle={{
                                    width: 16,
                                    height: 16,
                                    borderRadius: 20,
                                    borderColor: toggleMode ? null : colors.darkGray,
                                    borderWidth: 0.5
                                }}
                            />
                        </View>
                        <InputText
                            placeholder={localStrings.remarks}
                            onChangeText={onChangeCNR}
                            inputStyle={{ ...styles.bgWhite, paddingVertical: 10 }}
                            style={{ marginVertical: 5 }}
                            numberOfLines={3}
                            height={100}
                        />

                        <View style={{ marginHorizontal: 20, backgroundColor: colors.iconWhite, borderRadius: 10, borderWidth: 0.5, marginVertical: 5, borderColor: colors.primary, justifyContent: 'flex-start' }}>
                            <RichToolbar
                                editor={richText}
                                selectedIconTint="#873c1e"
                                iconTint="#312921"
                                actions={[
                                    actions.insertImage,
                                    actions.setBold,
                                    actions.setItalic,
                                    actions.insertBulletsList,
                                    actions.insertOrderedList,
                                    actions.insertLink,
                                    actions.setStrikethrough,
                                    actions.setUnderline,
                                ]}
                                style={styles.richTextToolbarStyle}
                            />
                            <View style={{}}>
                                <TextInput
                                    placeholder={localStrings.title}
                                    onChangeText={onChangeCNR}
                                    style={{ fontSize: 20, fontWeight: '500', marginStart: 10 }}
                                    placeholderTextColor={colors.lightGray}
                                    numberOfLines={1}
                                />
                            </View>

                            <View>
                                <TextInput
                                    placeholder={localStrings.note}
                                    onChangeText={onChangeCNR}
                                    style={{ marginVertical: 5, fontSize: 16, fontWeight: '400', marginStart: 10 }}
                                    textAlignVertical="top"
                                    placeholderTextColor={colors.lightGray}
                                    numberOfLines={5}
                                />
                            </View>

                        </View>
                        <View style={{ padding: 10, backgroundColor: colors.iconWhite, marginHorizontal: 20, borderRadius: 5, borderColor: colors.primary, borderWidth: 0.5, marginVertical: 5 }}>
                            <TextWrapper fontFamily={fonts.poppins.regular} color={colors.darkGray} style={{ marginVertical: 5 }}>Documents</TextWrapper>
                            <TouchableOpacity style={{ backgroundColor: colors.cardBg, borderRadius: 5, borderWidth: 0.5, borderColor: colors.primary, flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10, height: 46 }}>
                                <AddDocIcon />
                                <TextWrapper color={colors.darkGray} fontFamily={fonts.poppins.regular} style={styles.ms10}>{localStrings.addNewDoc}</TextWrapper>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ backgroundColor: colors.cardBg, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginVertical: 5, padding: 10, height: 46, borderRadius: 5 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <DocIcon />
                                    <TextWrapper color={colors.darkGray} fontFamily={fonts.poppins.regular} style={styles.ms10}>Note 1</TextWrapper>
                                </View>
                                <TrashIcon width={16} height={18}/>
                            </TouchableOpacity>
                        </View>

                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: Dimensions.get('window').width * 0.6, alignSelf: 'center', marginTop: 20 }}>
                            <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{ marginHorizontal: 5, height: 37, flex: 1 }} labelStyle={{ fontSize: 16 }} labelFont={fonts.poppins.regular} />
                            <RoundedButton label="Add" btnType="filled" style={{ marginHorizontal: 5, height: 37, flex: 1 }} labelStyle={{ fontSize: 16 }} labelFont={fonts.poppins.regular} />
                        </View>
                    </View>
                }
                </ScrollView>
            
            <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'center', marginTop: 20, width: Dimensions.get('window').width * 0.6, alignSelf: 'center'}}>
                <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{marginHorizontal: 5, height: 37, flex: 1}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
                <RoundedButton 
                    label="Fetch" 
                    btnType="filled" 
                    style={{marginHorizontal: 5, height: 37, flex: 1}} 
                    labelStyle={{fontSize: 16}} 
                    labelFont={fonts.poppins.regular}
                    onPress={() => {
                        fetchByCnr()
                    }}
                />
            </View>
        </View>
    );
};

export default CnrNumber;
