import fonts from "../../shared/theme/fonts";
import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
    container: ViewStyle;
    buttonsContainer: ViewStyle;
    mh5: ViewStyle;
    mh20: ViewStyle;
    outlinedBtnStyle: ViewStyle;
    contactUsText: TextStyle;
    fdRow: ViewStyle;
    planText: TextStyle;
    simpleText: TextStyle;
    simpleTextRed: TextStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        mh5: {
            marginHorizontal: 5
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center'
        },
        contactUsText: {
            fontSize: 18,
            fontWeight: '500',
            margin: 20
        },
        mh20: {
            marginHorizontal: 20
        },
        outlinedBtnStyle: {
            flexDirection: 'row',
            paddingVertical: 5,
            marginHorizontal: 20,
            justifyContent: 'flex-start',
            paddingHorizontal: 10
        },
        planText: {
            fontSize: 16,
            color: colors.darkGray,
            fontFamily: fonts.poppins.medium
        },
        simpleText: {
            fontSize: 16,
            color: colors.text,
            fontFamily: fonts.poppins.regular
        },
        simpleTextRed: {
            fontSize: 16,
            color: colors.primary,
            fontFamily: fonts.poppins.medium
        },
        fdRow: {
            flexDirection: 'row'
        }
    });
};
