import { useMemo } from "react";
import { FlatList, ScrollView, StyleProp, Text, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./PlanDetails.style"
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import HistoryCard from "./components/historyCard/HistoryCard";
import fonts from "../../shared/theme/fonts";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const PlanDetails: React.FC<SupportProps> = () => {
    const theme = useTheme();
    // const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    const data = [
        {
            name: 'hello',
        },
        {
            name: 'hello',
        },
        {
            name: 'hello',
        },
        {
            name: 'hello',
        }
      ]

    return (
        <ScrollView style={styles.container} contentContainerStyle={{padding: 20}}>
            <Text style={styles.planText}>Plan: Standard | Monthly</Text>
            <View style={styles.fdRow}>
                <Text style={styles.simpleText}>Subscription ends on</Text>
                <Text style={styles.simpleTextRed}>  30th June 2024</Text>
            </View>
            <RoundedButton label="Renew Now" btnType="filled" style={{alignSelf: 'center', height: 37, marginTop: 30}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
            <FlatList
                data={data}
                style={{marginTop: 50}}
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                renderItem={({item} : {item: any}) => {
                    return <HistoryCard data={{}}/>
                }}
                keyExtractor={(_item, index) => index.toString()}
            />
        </ScrollView>
    );
}

export default PlanDetails;
