import React, { useMemo } from "react";
import { View, StyleProp, ViewStyle, FlatList } from "react-native";
import { useTheme } from "@react-navigation/native";
/**
 * ? Local Imports
 */
import createStyles from "./HistoryCard.style";
import TextWrapper from "../../../../shared/components/text-wrapper/TextWrapper";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
    data: any;
}

const data2 = [
    {
        name: 'iwrfwie',
    },
    {
        name: 'iwrfwie',
    }
]

const HistoryCard: React.FC<ICardItemProps> = ({ data }) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const { colors } = theme;

    const CaseCard = () => {
        return (
            <>
                <TextWrapper style={styles.mh20}>M.A.C.P./685/2022</TextWrapper>
                <View style={[styles.fdRow, styles.mv10 , styles.mh20]}>
                    <TextWrapper bold>Sachin Sanjay Devprasad Singh</TextWrapper>
                    <TextWrapper> VS</TextWrapper>
                </View>
                <View style={[styles.mv10, styles.mh20]}>
                    <TextWrapper bold >Sachin Sanjay Devprasad Singh</TextWrapper>
                </View>
            </>
        )
    }

    const ItemAndValue = ({item, value} : {item: string, value: string}) => {
        return (
            <View style={styles.header}>
                <TextWrapper color={colors.text}>{item}</TextWrapper>
                <TextWrapper color={colors.text}>{value}</TextWrapper>
            </View>
        );
    }
 
    return (
        <View style ={{backgroundColor: colors.iconWhite, justifyContent: 'center', paddingVertical: 10, marginVertical: 10, borderRadius: 5, elevation: 3}}>
            <View style={styles.header}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TextWrapper color={colors.primary} bold>Standard</TextWrapper>
                    <View style={{width: 2, height: 15, backgroundColor: colors.black, marginHorizontal: 5}}/>
                    <TextWrapper color={colors.primary} bold>Monthly</TextWrapper>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <TextWrapper color={colors.text}>1st June 2024</TextWrapper>
                    <View style={{width: 2, height: 15, backgroundColor: colors.black, marginHorizontal: 5}}/>
                    <TextWrapper color={colors.text}>12:00</TextWrapper>
                </View>
            </View>
            <View style={styles.separator}/>
            <ItemAndValue item="Transaction ID" value="#75"/>
            <ItemAndValue item="Transaction Amount" value="₹ 9.99"/>
            <ItemAndValue item="Payment Method" value="UPI"/>
            <ItemAndValue item="Order ID" value="#1682239446"/>
            <ItemAndValue item="Tracking ID" value="#112860781690"/>
        </View>
    );
};

export default HistoryCard;