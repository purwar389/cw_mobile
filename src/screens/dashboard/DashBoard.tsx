import {useEffect, useMemo, useRef, useState} from 'react';
import {Image, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import createStyles from './DashBoard.style';
import {localStrings} from '../../shared/localization';
import TextWrapper from '../../shared/components/text-wrapper/TextWrapper';
import {FlatList} from 'react-native-gesture-handler';
import {SCREENS} from '../../shared/constants';
import ActiveCasesIcon from '../../assets/icons/activeCasesIcon';
import TodaysCasesIcon from '../../assets/icons/todaysCasesIcon';
import TomorrowsCasesIcon from '../../assets/icons/tomorrowsCasesIcon';
import DateAwaitedIcon from '../../assets/icons/dateAwaitedIcon';
import DailyBoardIcon from '../../assets/icons/dailyBoardIcon';
import ArchivesIcon from '../../assets/icons/archivesIcon';
import SortingSheet from '../../shared/components/bottomSheet/SortingSheet';
import fonts from '../../shared/theme/fonts';
import {API} from '../../services/apiservices/apiHandler';

interface DashBoardScreenProps {}

interface DashBoardProps {
  id: number;
  title: string;
  highCourtCases: number;
  districtCases: number;
  icon: React.FC;
}

const DashBoard: React.FC<DashBoardScreenProps> = () => {
  const theme = useTheme();
  const {colors} = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  const navigation = useNavigation();
  const refRBSheet = useRef(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [data, setData] = useState();

  const dashboardData = [
    {
      title: 'Active Cases',
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <ActiveCasesIcon />,
    },
    {
      title: "Today's Cases",
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <TodaysCasesIcon />,
    },
    {
      title: "Tomorrow's Cases",
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <TomorrowsCasesIcon />,
    },
    {
      title: 'Date Awaited Cases',
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <DateAwaitedIcon />,
    },
    {
      title: 'Daily Board',
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <DailyBoardIcon />,
    },
    {
      title: 'Archives',
      districtAndTribunals: 51,
      highCourt: 13,
      icon: <ArchivesIcon />,
    },
  ];

  const getDashBoardData = async () => {
    try {
      setIsLoading(true);
      const apiEndpoint = '/api/dashboard';

      // Make API request
      const response = await API.getAPIService(apiEndpoint);

      // Map through the response data to add icons
      const updatedData = response.map((item: any) => ({
        ...item,
        icon: titleToIconMap[item.title] || null, // Add icon based on title
      }));

      console.log(updatedData);
      setIsLoading(false);
      setData(updatedData);
    } catch (error) {
      setIsLoading(false);
      console.error(error);
    }
  };

  const titleToIconMap: Record<string, JSX.Element> = {
    'Active Cases': <ActiveCasesIcon />,
    "Today's Cases": <TodaysCasesIcon />,
    "Tomorrow's Cases": <TomorrowsCasesIcon />,
    'Date Awaited Cases': <DateAwaitedIcon />,
    'Daily Board': <DailyBoardIcon />,
    Archives: <ArchivesIcon />,
  };

  useEffect(() => {
    getDashBoardData();
  }, []);

  const _renderItem = ({
    item,
    index,
  }: {
    item: DashBoardProps;
    index: number;
  }) => {
    return (
      <TouchableOpacity
        style={styles.dashboardCard}
        onPress={() => {
          if (item.title != 'Daily Board') {
            navigation.navigate(SCREENS.ACTIVE_CASES, {data: item.title});
          } else {
            navigation.navigate(SCREENS.CALENDAR, {data: item.title});
          }
        }}>
        <View style={{padding: 10}}>
          <TextWrapper
            color={colors.primary}
            fontFamily={fonts.poppins.semiBold}
            style={{fontSize: 20}}>
            {item.title}
          </TextWrapper>
          <View style={{marginTop: 10}}>
            <TextWrapper
              color={colors.gray}
              style={{
                fontSize: 16,
              }}>{`${localStrings.districtAndTribunals} ${item.districtCases}`}</TextWrapper>
            <TextWrapper
              color={colors.gray}
              style={{
                fontSize: 16,
              }}>{`${localStrings.highCourt} ${item.highCourtCases}`}</TextWrapper>
          </View>
        </View>
        <View
          style={{
            backgroundColor: colors.primary,
            height: 40,
            width: 40,
            alignItems: 'center',
            justifyContent: 'center',
            marginHorizontal: 20,
            borderBottomRightRadius: 5,
            borderBottomLeftRadius: 5,
          }}>
          {item.icon}
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        renderItem={_renderItem}
        keyExtractor={(item: any, index: number) => index.toString()}
      />
      <SortingSheet sheetRef={refRBSheet} />
    </SafeAreaView>
  );
};

export default DashBoard;
