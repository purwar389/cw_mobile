import React, {useMemo, useState} from 'react';
import {
  View,
  StyleProp,
  ViewStyle,
  TextInput,
  Text,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Pressable,
} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
/**
 * ? Local Imports
 */
import createStyles from './AddWork.style';
import SelectButton from '../../shared/components/buttons/SelectButton';
import {localStrings} from '../../shared/localization';
import SearchBar from '../../shared/components/SearchBar/SearchBar';
import TextWrapper from '../../shared/components/text-wrapper/TextWrapper';
import InputText from '../../shared/components/InputText';
import RoundedButton from '../../shared/components/buttons/RoundedButton';
import fonts from '../../shared/theme/fonts';
import DropDown from '../../shared/components/DropDown';
import CalendarIcon from '../../assets/icons/calendarIcon';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
  style?: CustomStyleProp;
  cnrNumber: string;
  onChangeCNR: (cnr: string) => void;
}

const AddWork: React.FC<ICardItemProps> = ({style, cnrNumber, onChangeCNR}) => {
  const theme = useTheme();
  const styles = useMemo(() => createStyles(theme), [theme]);
  const {colors} = theme;
  const [screen, setScreen] = useState(localStrings.inProgress);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  const onChange = (event: any, selectedDate?: Date | undefined) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
  };
  return (
    <ScrollView style={styles.container}>
      <InputText
        placeholder={localStrings.title}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        inputStyle={styles.bgWhite}
        style={styles.mt10}
      />
      <InputText
        placeholder={localStrings.description}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        multiline={true}
        numberOfLines={6}
        inputStyle={{...styles.bgWhite, paddingVertical: 10}}
        style={styles.mt10}
        height={100}
      />

      <DateTimePickerModal
        isVisible={true}
        mode="date"
        onConfirm={() => {}}
        onCancel={() => {}}
      />

      <View style={{flexDirection: 'row', marginHorizontal: 10}}>
        <InputText
          placeholder={localStrings.dateFormat}
          onChangeText={onChangeCNR}
          value={cnrNumber}
          style={{flex: 1, marginHorizontal: -10, marginTop: 10}}
          inputStyle={styles.bgWhite}
        />
        <InputText
          placeholder={localStrings.timeFormat}
          onChangeText={onChangeCNR}
          value={cnrNumber}
          style={{flex: 1, marginHorizontal: -20, marginTop: 10}}
          inputStyle={styles.bgWhite}
        />
        <DropDown
          label={'AM'}
          onChangeText={onChangeCNR}
          value={cnrNumber}
          style={{flex: 0.5, marginTop: 10, marginHorizontal: 10}}
          data={['AM', 'PM']}
        />
      </View>
      <DropDown
        label={localStrings.repeatsDaily}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10, marginHorizontal: 20}}
        data={['Shivani Suryavanshi', 'Ajinkya Joshi']}
      />
      <DropDown
        label={localStrings.ends}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginTop: 10, marginHorizontal: 20}}
        data={['Shivani Suryavanshi', 'Ajinkya Joshi']}
      />
      <View
        style={{
          borderRadius: 5,
          borderWidth: 0.5,
          borderColor: colors.primary,
          paddingHorizontal: 10,
          marginTop: 10,
          width: '90%',
          alignSelf: 'center',
          backgroundColor: colors.iconWhite,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <TextInput placeholder={localStrings.endDate} editable={false} />
        <Pressable onPress={() => setShow(true)}>
          <CalendarIcon />
        </Pressable>
      </View>
      <View style={styles.cardContainer}>
        <TextWrapper
          color={colors.text}
          style={{marginHorizontal: 0, fontSize: 16, fontWeight: '500'}}>
          {localStrings.assignedTo}
        </TextWrapper>
        <DropDown
          label={localStrings.name}
          onChangeText={onChangeCNR}
          value={cnrNumber}
          style={{marginTop: 10}}
          backgroundColor={colors.background}
          data={['Shivani Suryavanshi', 'Ajinkya Joshi']}
        />
      </View>
      <DropDown
        label={localStrings.case}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={{marginHorizontal: 20}}
        backgroundColor={colors.background}
        isSearch={true}
        searchPlaceHolder={'Search Case'}
        data={['Shivani Suryavanshi', 'Ajinkya Joshi']}
      />
      <View
        style={{
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
          width: Dimensions.get('window').width * 0.6,
          alignSelf: 'center',
          marginTop: 10,
        }}>
        <RoundedButton
          label="Cancel"
          btnType="outlined"
          outlineColor={colors.text}
          style={{marginHorizontal: 5, height: 37, flex: 1, borderRadius: 100}}
          labelStyle={{fontSize: 16}}
          labelFont={fonts.poppins.regular}
        />
        <RoundedButton
          label="Add"
          btnType="filled"
          style={{marginHorizontal: 5, height: 37, flex: 1, borderRadius: 100}}
          labelStyle={{fontSize: 16}}
          labelFont={fonts.poppins.regular}
        />
      </View>
    </ScrollView>
  );
};

export default AddWork;
