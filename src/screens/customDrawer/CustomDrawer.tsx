import {useMemo, useState} from 'react';
import {FlatList, TouchableOpacity, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import createStyles from '../sortBy/SortBy.style';
import {localStrings} from '../../shared/localization';
import TextWrapper from '../../shared/components/text-wrapper/TextWrapper';
import fonts from '../../shared/theme/fonts';
import DashboardIcon from '../../assets/icons/dashboardIcon';
import BareActsIcon from '../../assets/icons/bareActsIcon';
import ArchiveMenuIcon from '../../assets/icons/archiveMenuIcon';
import CalculatorIcon from '../../assets/icons/calculatorIcon';
import MyClientsIcon from '../../assets/icons/myClientsIcon';
import WorkListIcon from '../../assets/icons/workListIcon';
import MyAccountIcon from '../../assets/icons/myAccountIcon';
import NotificationMenuIcon from '../../assets/icons/notificationMenuIcon';
import {SafeAreaView} from 'react-native-safe-area-context';
import ConnectedMattersIcon from '../../assets/icons/connectedMattersIcon';
import {SCREENS} from '../../shared/constants';
import Ionicons from 'react-native-vector-icons/Ionicons';

interface SupportProps {
  navigation: any;
}

const CustomDrawerContent: React.FC<SupportProps> = props => {
  const theme = useTheme();
  const {colors} = theme;
  const styles = useMemo(() => createStyles(theme), [theme]);
  const {navigation} = props;

  // State to keep track of the selected screen
  const [selectedScreen, setSelectedScreen] = useState<string>('');

  const data = [
    {
      name: localStrings.dashboard,
      screenName: SCREENS.DASHBOARD,
      icon: (
        <DashboardIcon
          color={
            selectedScreen == SCREENS.DASHBOARD
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.addCase,
      screenName: localStrings.addCase,
      icon: (
        <Ionicons
          name="add-circle-outline"
          size={24}
          color={
            selectedScreen == localStrings.addCase
              ? colors.iconWhite
              : colors.gray
          }
        />
      ),
    },
    {
      name: localStrings.bareActs,
      screenName: localStrings.bareActs,
      icon: (
        <BareActsIcon
          color={
            selectedScreen == localStrings.bareActs
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.archives,
      screenName: localStrings.archives,
      icon: (
        <ArchiveMenuIcon
          color={
            selectedScreen == localStrings.archives
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.connectedMatters,
      screenName: localStrings.connectedMatters,
      icon: (
        <ConnectedMattersIcon
          color={
            selectedScreen == localStrings.connectedMatters
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.limitationCalculator,
      screenName: localStrings.limitationCalculator,
      icon: (
        <CalculatorIcon
          color={
            selectedScreen == localStrings.limitationCalculator
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.myClients,
      screenName: localStrings.myClients,
      icon: (
        <MyClientsIcon
          color={
            selectedScreen == localStrings.myClients
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.worklist,
      screenName: localStrings.worklist,
      icon: (
        <WorkListIcon
          color={
            selectedScreen == localStrings.worklist
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.myAccount,
      screenName: localStrings.myAccount,
      icon: (
        <MyAccountIcon
          color={
            selectedScreen == localStrings.myAccount
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
    {
      name: localStrings.notifications,
      screenName: localStrings.notifications,
      icon: (
        <NotificationMenuIcon
          color={
            selectedScreen == localStrings.notifications
              ? colors.iconWhite
              : colors.darkGray
          }
        />
      ),
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        style={{flex: 0.3, backgroundColor: colors.iconWhite}}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => {
          const isSelected = selectedScreen === item.screenName;
          return (
            <TouchableOpacity
              onPress={() => {
                setSelectedScreen(item.screenName); // Set the selected screen
                navigation.navigate(item.screenName); // Navigate to the selected screen
              }}>
              <TextWrapper
                fontFamily={fonts.poppins.medium}
                color={isSelected ? colors.iconWhite : colors.gray}
                leftIcon={item.icon}
                style={{marginStart: 10}}
                viewStyle={{
                  paddingHorizontal: 10,
                  paddingVertical: 15,
                  flexDirection: 'row',
                  alignItems: 'center',
                  backgroundColor: isSelected
                    ? colors.primary
                    : colors.iconWhite, // Highlight selected item
                }}>
                {item.name}
              </TextWrapper>
            </TouchableOpacity>
          );
        }}
        ListHeaderComponent={() => {
          return (
            <View style={{backgroundColor: colors.background, padding: 20}}>
              <TextWrapper
                fontFamily={fonts.poppins.semiBold}
                color={colors.text}
                style={{fontSize: 16}}>
                Menu
              </TextWrapper>
            </View>
          );
        }}
        ItemSeparatorComponent={() => {
          return (
            <View
              style={{
                width: '100%',
                marginHorizontal: 10,
                height: 1,
                backgroundColor: colors.background,
              }}
            />
          );
        }}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  );
};

export default CustomDrawerContent;
