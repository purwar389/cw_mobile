import React, { useMemo, useState } from "react";
import { FlatList, View } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./Archives.style";
import SelectButton from "../../shared/components/buttons/SelectButton";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import Ionicons from 'react-native-vector-icons/Ionicons';
import CaseCard from "../../shared/components/caseCard/CaseCard";
import { SafeAreaView } from "react-native-safe-area-context";

interface AddCaseScreenProps {}

const Archives: React.FC<AddCaseScreenProps> = () => {
  const theme = useTheme();
  const styles = useMemo(() => createStyles(theme), [theme]);
  const [screen, setScreen] = useState("DCT");
  const { colors } = theme;

  const data = [
    {
        name: 'hello',
    },
    {
        name: 'hello',
    },
    {
        name: 'hello',
    },
    {
        name: 'hello',
    }
  ]

  return (
    <SafeAreaView style={styles.container}>
        <View style={[styles.fdRow, {marginHorizontal: 15}]}>
            <SelectButton 
                label="Decided cases" 
                selected={screen=="DCT"} 
                labelStyle={styles.labelStyle} 
                style={{ flex: 1 }} 
                onPress={() => setScreen("DCT")}
            />
            <SelectButton 
                label="Abandoned Cases" 
                selected={screen == "HCSC"} 
                labelStyle={styles.labelStyle} 
                style={{ flex: 1 }} 
                onPress={() => setScreen("HCSC")}
            />
        </View>
        <SearchBar
            placeholder="Search"
            onChangeText={() => { }}
            leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
            style={{ flexDirection: 'row', marginVertical: 10 }}
        />
        <FlatList
            data={data}
            renderItem={({item} : {item: any}) => {
                return <CaseCard/>
            }}
            keyExtractor={(item, index) => index.toString()}
        />
    </SafeAreaView>
  );
}

export default Archives;
