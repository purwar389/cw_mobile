import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
    container: ViewStyle;
    buttonsContainer: ViewStyle;
    mh5: ViewStyle;
    outlinedbtnLabelStyle: ViewStyle;
    roundedButtonStyle: ViewStyle;
    labelStyle: TextStyle;
    filledbtnLabelStyle: TextStyle;
    bgWhite: ViewStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        mh5: {
            marginHorizontal: 5,
            marginVertical: 10
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center',
            width: '70%',
            alignSelf: 'center',
            marginTop: 30
        },
        bgWhite: {
            backgroundColor: colors.white
        },
        labelStyle: {
            fontSize: 14,
            color: colors.primary,
            marginHorizontal: 5
        },
        outlinedbtnLabelStyle: {
            color: colors.gray,
            fontSize: 20,
        },
        filledbtnLabelStyle: {
            color: colors.iconWhite,
            fontSize: 20,
        },
        roundedButtonStyle: {
            marginHorizontal: 5,
            flex: 1,
            height: 46,
            borderRadius: 100
        }
    });
};
