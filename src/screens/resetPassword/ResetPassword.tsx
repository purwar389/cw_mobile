import { useMemo } from "react";
import { Dimensions, StyleProp, View, ViewStyle } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./ResetPasword.style";
import InputText from "../../shared/components/InputText";
import { localStrings } from "../../shared/localization";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import fonts from "../../shared/theme/fonts";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ResetPasswordProps {
    style?: CustomStyleProp;
    cnrNumber: string;
    onChangeCNR: (cnr: string) => void;
};

const ResetPassword : React.FC<ResetPasswordProps> = ({ cnrNumber, onChangeCNR }) => {
    const theme = useTheme();
    const navigation = useNavigation();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);

    return (
        <View style={styles.container}>
            <InputText
                placeholder={localStrings.currentPwd}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.newPwd}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <InputText
                placeholder={localStrings.cnfrmNewPwd}
                onChangeText={onChangeCNR}
                value={cnrNumber}
                inputStyle={styles.bgWhite}
                style={styles.mh5}
            /> 
            <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'center', width: Dimensions.get('window').width * 0.7, alignSelf: 'center', marginTop: 10}}>
                <RoundedButton label="Cancel" btnType="outlined" outlineColor={colors.text} style={{marginHorizontal: 5, height: 46, flex: 1, borderRadius: 100}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
                <RoundedButton label="Update" btnType="filled" style={{marginHorizontal: 5, height: 46, flex: 1, borderRadius: 100}} labelStyle={{fontSize: 16}} labelFont={fonts.poppins.regular}/>
            </View>
        </View>
    );
}

export default ResetPassword;
