import React, { useMemo, useState } from "react";
import { ScrollView, TouchableOpacity, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { localStrings } from "../../shared/localization";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import SelectButton from "../../shared/components/buttons/SelectButton";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import createStyles from "./MyClients.style";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import { SCREENS } from "../../shared/constants";
import CallIcon from "../../assets//icons/callIcon";
import fonts from "../../shared/theme/fonts";
import MailIcon from "../../assets//icons/mailIcon";
import LocationIcon from "../../assets//icons/locationIcon";
import AddNewIcon from "../../assets//icons/addNewIcon";
import EditIcon from "../../assets//icons/editIcon";
import DeleteIcon from "../../assets//icons/deleteIcon";

interface LoginScreenProps {}

interface UserData {
    phone: string;
    name: string;
    mail: string;
    address: string;
}

interface UserDataOrg {
    orgName: string;
    address: string;
    persons: authPerson[];
}

interface authPerson {
    phone: string;
    name: string;
    mail: string;
}

const MyClients: React.FC<LoginScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [screen, setScreen] = useState("individual");

    const data: UserData[] = [
        {
            phone: "+918209060563",
            name: "Anand Joshi Singh",
            mail: "abhishek_ji@gmail.com",
            address: "laxmi narsimha gents pg, btm layout stage 1, bengaluru, India"
        },
        {
            phone: "+918209060563",
            name: "Abhishek ji",
            mail: "abhishek_ji@gmail.com",
            address: "laxmi narsimha gents pg, btm layout stage 1, bengaluru, India"
        },
        // Add more data as needed
    ];


    const dataOrg: UserDataOrg[] = [
        {
            orgName: "Katol Nagri Sahakari Pat Sanstha",
            address: "laxmi narsimha gents pg, btm layout stage 1, bengaluru, India",
            persons: [
                {
                    phone: "+918209060563",
                    name: "Anand Joshi Singh",
                    mail: "advadityajoshi@gmail.com",
                },
                {
                    phone: "+918209060563",
                    name: "Anand Joshi Singh",
                    mail: "advadityajoshi@gmail.com",
                }
            ]
        },
        {
            orgName: "Katol Nagri Sahakari Pat Sanstha",
            address: "laxmi narsimha gents pg, btm layout stage 1, bengaluru, India",
            persons: [
                {
                    phone: "+918209060987",
                    name: "Anand Joshi Singh",
                    mail: "advadityajoshi@gmail.com",
                },
                {
                    phone: "+918209060563",
                    name: "Anand Joshi Singh",
                    mail: "advadityajoshi@gmail.com",
                }
            ]
        },
    ];

    const handleScreenChange = (screenName: string) => {
        setScreen(screenName);
    };

    const renderClients = () => {
        return (
        <View>
            <SearchBar
                placeholder="Search"
                keyboardType="number-pad"
                onChangeText={() => { }}
                leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                style={{ flexDirection: 'row', backgroundColor: colors.white, elevation: 3, marginVertical: 5, height: 37}}
            />
            <TouchableOpacity
                style={styles.addNewButton}
                onPress={handleAddNew}
            >
                <AddNewIcon/>
                <TextWrapper color={colors.text} style={{ fontSize: 16, marginHorizontal: 10 }}>Add New</TextWrapper>
            </TouchableOpacity>
            {data.map((item, index) => (
            <View key={index} style={{ marginVertical: 10, marginHorizontal: 20, backgroundColor: colors.iconWhite, elevation: 3, borderRadius: 5 , padding: 10}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',marginHorizontal: 10}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <CallIcon/>
                        <TextWrapper color={colors.text} style={{ fontSize: 16, marginStart: 10 }}>{item.phone}</TextWrapper>
                    </View>
                    <OutlinedButton label="Send SMS" iconLeft={<AntDesign name='message1' color={colors.primary} size={16}/>} style={styles.outlinedBtnStyle} labelStyle={styles.outlinedbuttonLabel}/>
                </View>
                <TextWrapper color={colors.text} style={{ fontSize: 16, marginHorizontal: 10 , marginBottom: 10}} fontFamily={fonts.poppins.semiBold}>{item.name}</TextWrapper>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={styles.textWrapperStyle}
                    style={styles.labelStyle2} 
                    leftIcon={<View style={styles.me5}><MailIcon/></View>}
                >{item.mail}</TextWrapper>
                <TextWrapper 
                    color={colors.text} 
                    viewStyle={styles.textWrapperStyle}
                    style={styles.labelStyle2} 
                    leftIcon={<View style={styles.me5}><LocationIcon/></View>}
                >{item.address}</TextWrapper>
                <View style={styles.rowCentered}>
                    <RoundedButton 
                        label="Edit Info" 
                        btnType="filled" 
                        style={styles.roundedButtonStyle} 
                        labelStyle={styles.labelStyle3} 
                        iconLeft={<EditIcon/>}
                        onPress={handleAddNew}
                    />
                    <RoundedButton 
                        label="Delete" 
                        btnType="outlined" 
                        style={styles.roundedButtonStyle} 
                        labelStyle={styles.labelStyle} 
                        iconLeft={<DeleteIcon/>}/>
                </View>
                
            </View>
        ))}
        </View>)
    };

    const renderOrganisations = () => {
        return (
            <View>
                <SearchBar
                    placeholder="Search"
                    keyboardType="number-pad"
                    onChangeText={() => { }}
                    leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                    style={{ flexDirection: 'row', backgroundColor: colors.white, elevation: 3, marginVertical: 5, height: 37}}
                />
                <TouchableOpacity
                    style={styles.addNewButton}
                    onPress={handleAddNew}
                >
                    <AddNewIcon/>
                    <TextWrapper color={colors.text} style={{ fontSize: 16, marginHorizontal: 10 }}>{localStrings.addNew}</TextWrapper>
                </TouchableOpacity> 

                {dataOrg.map((item, index) => (
                <View key={index} style={{ marginVertical: 10, marginHorizontal: 20, backgroundColor: colors.iconWhite, elevation: 3, borderRadius: 5 }}>
                    <TextWrapper color={colors.text} style={{ fontSize: 16, margin: 10 }} bold>{item.orgName}</TextWrapper>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginHorizontal: 10}}>
                        <LocationIcon/>
                        <TextWrapper color={colors.text} style={{ fontSize: 16, margin: 10 }}>{item.address}</TextWrapper>
                    </View>
                    <View style={{height: 1, backgroundColor: colors.primary, marginVertical: 10}}/>
                    {item.persons.map((person, idx) => (
                        <>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',marginHorizontal: 10}}>
                                <TextWrapper 
                                    color={colors.text} 
                                    style={{ fontSize: 16}} 
                                >{person.name}</TextWrapper>
                                <OutlinedButton label="Send email" iconLeft={<AntDesign name='message1' color={colors.primary} size={16}/>} style={styles.outlinedBtnStyle} labelStyle={styles.outlinedbuttonLabel}/>    
                            </View>
                            <TextWrapper 
                                color={colors.text} 
                                viewStyle={styles.textWrapperStyle}
                                style={styles.labelStyle2} 
                                leftIcon={<View style={styles.me5}><MailIcon/></View>}
                            >{person.mail}</TextWrapper>
                            <TextWrapper 
                                color={colors.text} 
                                viewStyle={styles.textWrapperStyle}
                                style={styles.labelStyle2} 
                                leftIcon={<View style={styles.me5}><CallIcon/></View>}
                            >{person.phone}</TextWrapper>
                        </>
                    ))}
                    <View style={styles.rowCentered}>
                        <RoundedButton 
                            label="Edit info" 
                            btnType="filled" 
                            style={styles.roundedButtonStyle} 
                            labelStyle={styles.labelStyle3} 
                            iconLeft={<EditIcon/>}
                            onPress={handleAddNew}
                        />
                        <RoundedButton label="Delete" btnType="outlined" style={styles.roundedButtonStyle} labelStyle={styles.labelStyle} iconLeft={<DeleteIcon/>}/>
                    </View>
                </View>
            ))}
        </View>
        )
    }

    const handleAddNew = () => {
        navigation.navigate(SCREENS.ADD_MY_CLIENTS);
    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', marginHorizontal: 15 }}>
                <SelectButton
                    label="Individual"
                    selected={screen === "individual"}
                    labelStyle={styles.fs_14}
                    style={styles.tabStyle}
                    outlineColor={colors.gray}
                    onPress={() => handleScreenChange("individual")}
                />
                <SelectButton
                    label={localStrings.organosation!}
                    selected={screen === localStrings.organosation}
                    labelStyle={styles.fs_14}
                    style={styles.tabStyle}
                    outlineColor={colors.gray}
                    onPress={() => handleScreenChange(localStrings.organosation!)}
                />
                <SelectButton
                    label={localStrings.reference!}
                    selected={screen === localStrings.reference}
                    labelStyle={styles.fs_14}
                    style={styles.tabStyle}
                    outlineColor={colors.gray}
                    onPress={() => handleScreenChange(localStrings.reference!)}
                />
            </View>
            <ScrollView>
                {screen == 'individual' && renderClients()}
                {screen == localStrings.organosation && renderOrganisations()}
                {screen == localStrings.reference && renderClients()}
            </ScrollView>
        </View>
    );
};

export default MyClients;
