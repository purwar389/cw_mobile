import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    titleTextStyle: {
      fontSize: 32,
    },
    buttonStyle: {
      height: 45,
      width: ScreenWidth * 0.9,
      marginTop: 32,
      borderRadius: 12,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.primary,
      shadowRadius: 5,
      shadowOpacity: 0.7,
      shadowColor: colors.shadow,
      shadowOffset: {
        width: 0,
        height: 3,
      },
    },
    outlinedBtnStyle: {
      flexDirection: 'row',
      paddingVertical: 3,
      paddingHorizontal: 5,
      height: 30
    },
    labelStyle2: {
      fontSize: 14,
      fontWeight: '500'
    },
    addNewButton: { 
      flexDirection: 'row', 
      alignItems: 'center', 
      backgroundColor: colors.iconWhite, 
      padding: 10, 
      marginVertical: 10, 
      marginHorizontal: 20, 
      elevation: 3, 
      borderRadius: 5 
    },
    roundedButtonStyle: {
      height: 25, 
      marginHorizontal: 5, 
      flexDirection: 'row', 
      alignItems: 'center',
      flex: 1
    },
    textWrapperStyle: {
      flexDirection: 'row',
      marginHorizontal: 10
    },
    rowCentered: {
      flexDirection: 'row', 
      justifyContent: 'center', 
      alignItems: 'center'
    },
    outlinedbuttonLabel: {
      fontSize: 14,
      color: colors.primary,
      marginHorizontal: 5
    },
    me5: {
      marginEnd: 5
    },
    fs_14: {
      fontSize: 14,
    },
    tabStyle: { 
      flex: 1, 
      paddingHorizontal: 0,
      height: 45
    },
    labelStyle: {
      fontSize: 12,
      color: colors.primary,
      marginHorizontal: 5,
    },
    labelStyle3: {
      fontSize: 12,
      color: colors.iconWhite,
      marginStart: 10,
    },
    buttonTextStyle: {
      color: colors.iconWhite,
      fontWeight: "700",
    },
    header: {
      width: ScreenWidth * 0.9,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
    contentContainer: {
      flex: 1,
      marginTop: 16,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    titleText: {
      color: colors.primary,
      fontSize: 48,
      fontFamily: fonts.poppins.regular,
      fontWeight: '700',
      textAlign: 'center'
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 24,
      fontFamily: fonts.poppins.regular,
      fontWeight: '600',
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    cardContainer: {
      paddingVertical: 10,
      backgroundColor: 'white',
      marginVertical: 5,
      borderRadius: 5,
      elevation: 3,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      marginHorizontal: 20,
      paddingHorizontal: 10
    }
  });
};