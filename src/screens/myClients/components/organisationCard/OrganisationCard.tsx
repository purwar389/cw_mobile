import { useMemo, useState } from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./OrganisationCard.style";
interface LoginScreenProps {}

interface UserData {
    email: string,
    password: string,
}

const OrganisationCard : React.FC<LoginScreenProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [screen, setScreen] = useState("cnr");

    return (
        <View style={styles.container}>
            
        </View>
    );
}

export default OrganisationCard;