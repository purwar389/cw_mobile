import { RefObject, useEffect, useMemo, useRef, useState } from "react";
import { ActivityIndicator, Text, TextInput, ToastAndroid, TouchableOpacity, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./OtpScreen.style";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import { SCREENS } from "../../shared/constants";
import SelectButton from "../../shared/components/buttons/SelectButton";
import OTPInputComponent from "../../shared/components/otpInput/OtpInput";
import { API } from "../../services/apiservices/apiHandler";
import { ReduxServices } from "../../services/redux/ReduxService";
import { setToken } from "../../services/redux/token/TokenReducer";
import AsyncStorage from "@react-native-async-storage/async-storage";

interface LoginScreenProps { }

interface UserData {
    email: string,
    password: string,
}

const OtpScreen: React.FC<LoginScreenProps> = ({ route }) => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const {email} = route.params
    const navigation = useNavigation();
    const [userData, setUserData] = useState<UserData>({
        email: '',
        password: ''
    });
    let otpInput = useRef(null);
    const [otp, setOtp] = useState('');
    const [errorMessages, setErrorMessages] = useState<string[]>();
    const [codes, setCodes] = useState<string[] | undefined>(Array(6).fill(""));
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [timer, setTimer] = useState<number>(30);
    const [isResendDisabled, setIsResendDisabled] = useState<boolean>(false);
    const [timerInterval, setTimerInterval] = useState<NodeJS.Timeout | null>(null);
    const refs: RefObject<TextInput>[] = [
      useRef<TextInput>(null),
      useRef<TextInput>(null),
      useRef<TextInput>(null),
      useRef<TextInput>(null),
      useRef<TextInput>(null),
      useRef<TextInput>(null),
    ];
  

    const _handleChange = (text: string, field: keyof UserData) => {
        setUserData({ ...userData, [field]: text });
    }

    const _handleContinueBtn = () => {
        navigation.navigate(SCREENS.LOGIN);
    }

    const handleOtpChange = (code) => {
        console.log('code ==> '+code)
        setOtp(code);
    };

    useEffect(() => {
        return () => {
            if (timerInterval) {
                clearInterval(timerInterval);
            }
        };
    }, [timerInterval]);

    const startTimer = () => {
        setIsResendDisabled(true);
        setTimer(30);
        const interval = setInterval(() => {
            setTimer(prevTimer => {
                if (prevTimer <= 1) {
                    clearInterval(interval);
                    setIsResendDisabled(false);
                    return 0;
                }
                return prevTimer - 1;
            });
        }, 1000);
        setTimerInterval(interval);
    };

    const onChangeCode = (text: string, index: number) => {
        if (text.length > 1) {
            setErrorMessages(undefined);
            const newCodes = text.split("");
            setCodes(newCodes);
            refs[5]!.current?.focus();
            return;
        }
        setErrorMessages(undefined);
        const newCodes = [...codes!];
        newCodes[index] = text;
        setCodes(newCodes);
        if (text !== "" && index < 5) {
            refs[index + 1]!.current?.focus();
        }
    };

    const _handleResend = async () => {
        try {
            setIsLoading(true)
            const apiEndpoint = `/api/resend-otp`;
            const formData = { email: email};
            console.log(formData);
        
            // Make API request
            const response = await API.postAPIService(apiEndpoint, formData);
            if(response) {
                setIsLoading(false)
                ToastAndroid.show(response.message, ToastAndroid.SHORT)
                startTimer();
            }
        } catch (error) {
            setIsLoading(false)
        }
    }

    const _handleLogin = async () => {
        try {
            setIsLoading(true)
            const apiEndpoint = `/api/verify-otp`;
            const formData = { email, otp };
        
            // Make API request
            const response = await API.postAPIService(apiEndpoint, formData);
            ReduxServices.dispatchAction(setToken(response.access_token));
            await AsyncStorage.setItem('loggedInUserEmail', email);
            await AsyncStorage.setItem('loggedInUserToken', response.access_token);
            setIsLoading(false)
            navigation.navigate(SCREENS.HOME);
        } catch (error) {
            setIsLoading(false)
        }
    }

    return (
        <View style={styles.container}>
            {isLoading && <View style={{alignItems: 'center', position: 'absolute', justifyContent: 'center', height: '100%', width: '100%', zIndex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)'}}><ActivityIndicator size={'large'} color={colors.white}/></View>}
            <View style={{ flex: 0.28, justifyContent: 'center', }}>
                <Text style={styles.titleText}>{localStrings.casewise}</Text>
            </View>
            <View style={{ flex: 0.72, alignItems: 'center' }}>
                <TextWrapper fontFamily={fonts.poppins.semiBold} color={colors.gray} style={styles.welcomeText}>{localStrings.verifyEmail}</TextWrapper>
                <TextWrapper fontFamily={fonts.poppins.medium} color={colors.gray} style={{ fontSize: 14, marginTop: '11%' }}>{localStrings.weHaveSentVerification}</TextWrapper>
                <TextWrapper fontFamily={fonts.poppins.bold} color={colors.gray} style={{ fontSize: 14 }}>{email}</TextWrapper>

                 <OTPInputComponent
                     length={6} onOtpChange={handleOtpChange}
                />

                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10}}>
                    <TextWrapper color={colors.gray} fontFamily={fonts.poppins.medium}>{localStrings.didntGetOTP}</TextWrapper>
                    {/* <TouchableOpacity onPress={_handleResend}>
                        <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium}> {localStrings.resendOTPin}</TextWrapper>
                    </TouchableOpacity> */}
                    <TouchableOpacity onPress={_handleResend} disabled={isResendDisabled}>
                        <TextWrapper color={isResendDisabled ? colors.lightGray : colors.primary} fontFamily={fonts.poppins.medium}>
                            {isResendDisabled ? ` Resend in ${timer}s` : localStrings.resendOTPin}
                        </TextWrapper>
                    </TouchableOpacity>
                </View>
                <View style={styles.buttonContainer}>
                    <SelectButton
                        label={localStrings.login!}
                        style={{ width: '80%', height: 42 }}
                        selected={true}
                        labelStyle={{fontSize: 18}}
                        onPress={_handleLogin}
                    />
                    <SelectButton
                        label={localStrings.back!}
                        style={{ width: '80%', height: 42 }}
                        selected={false}
                        onPress={() => navigation.goBack()}
                        labelStyle={{fontSize: 18}}
                        outlineColor={colors.gray}
                    />
                </View>
            </View>
        </View>
    );
}

export default OtpScreen;