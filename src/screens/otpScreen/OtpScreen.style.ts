
import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    titleTextStyle: {
      fontSize: 32,
    },
    buttonStyle: {
      height: 45,
      width: ScreenWidth * 0.9,
      marginTop: 32,
      borderRadius: 12,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.primary,
      shadowRadius: 5,
      shadowOpacity: 0.7,
      shadowColor: colors.shadow,
      shadowOffset: {
        width: 0,
        height: 3,
      },
    },
    buttonContainer: {
      marginTop: '15%',
      width: '100%',
      alignItems: 'center'
    },
    buttonTextStyle: {
      color: colors.iconWhite,
      fontWeight: "700",
    },
    header: {
      width: ScreenWidth * 0.9,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
    contentContainer: {
      flex: 1,
      marginTop: 16,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    titleText: {
      color: colors.primary,
      fontSize: 42,
      fontFamily: fonts.poppins.semiBold,
      textAlign: 'center'
    },
    outlinedButtonStyle: {
      height: 48
    },
    welcomeText: {
      fontSize: 22,
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    forgotPassTxt: {
      fontSize: 14,
      fontFamily: fonts.poppins.regular,
      fontWeight: 'bold',
    },
    textGray16_500:{
      color: colors.gray,
      fontSize: 16,
      fontWeight: '500',
      textAlign: 'center'
    },
    separator: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: 20
    },
    separatorLine: {
      height: 1, 
      backgroundColor: colors.gray, 
      flex: 1
    },
    continueButton: {
      marginHorizontal: 20, 
      paddingVertical: 5
    },
    otpInput: {
        width: 40,
        height: 40,
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 5,
        textAlign: 'center',
        fontSize: 18,
    }, 
    otpContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        marginTop: 20,
        width: '80%',
    },   
  });
};
