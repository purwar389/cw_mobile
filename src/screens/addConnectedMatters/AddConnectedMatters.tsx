import React, {useMemo, useState} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
/**
 * ? Local Imports
 */
import createStyles from './AddConnectedMatters.style';
import {localStrings} from '../../shared/localization';
import OutlinedButton from '../../shared/components/buttons/OutlinedButton';
import RoundedButton from '../../shared/components/buttons/RoundedButton';
import DropDown from '../../shared/components/DropDown';
import LinkIcon from '../../assets/icons/linkIcon';

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface ICardItemProps {
  data: any;
}

const AddConnectedMatters: React.FC<ICardItemProps> = ({data}) => {
  const theme = useTheme();
  const styles = useMemo(() => createStyles(theme), [theme]);
  const {colors} = theme;
  const [cnrNumber, setCNRNumber] = useState();
  const navigation = useNavigation();

  const onChangeCNR = () => {};

  return (
    <View style={styles.container}>
      <DropDown
        label={localStrings.selectPrimaryCase}
        keyboardType="name-phone-pad"
        data={['huikghi', 'hikshuiokw']}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={styles.dropDown}
      />
      <DropDown
        label={localStrings.selectConnectedMatter}
        keyboardType="email-address"
        data={['huikghi', 'hikshuiokw']}
        onChangeText={onChangeCNR}
        value={cnrNumber}
        style={styles.dropDown}
      />
      <OutlinedButton
        label="Link More"
        iconLeft={<LinkIcon />}
        style={{
          flexDirection: 'row',
          marginHorizontal: 20,
          height: 37,
          marginTop: 20,
        }}
        labelStyle={{marginHorizontal: 10, fontSize: 16}}
        borderWidth={1}
      />
      <View style={[styles.fdRow, {justifyContent: 'center', marginTop: 10}]}>
        <RoundedButton
          label="Cancel"
          btnType="outlined"
          style={{height: 37, marginEnd: 5}}
          outlineColor={colors.gray}
          labelStyle={{fontSize: 16}}
          onPress={() => navigation.goBack()}
        />
        <RoundedButton
          label="Save"
          btnType="filled"
          style={{height: 37, marginStart: 5}}
          onPress={() => navigation.goBack()}
          labelStyle={{fontSize: 16}}
        />
      </View>
    </View>
  );
};

export default AddConnectedMatters;
