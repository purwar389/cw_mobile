import { StyleSheet } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    listContainer: {
      marginTop: 8,
    },
    mh20: {
      marginHorizontal: 20
    },
    mv10: {
      marginHorizontal: 10
    },
    fdRow: {
      flexDirection: 'row'
    },
    mh5: {
        marginHorizontal: 5
    },
    labelStyle: {
        fontSize: 14,
        color: colors.primary,
        marginHorizontal: 5
    },
    dropDown: {
      width: '90%',
      alignSelf: 'center',
      marginTop: 20
    }
  });
};