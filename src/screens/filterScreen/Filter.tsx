import { useMemo, useState } from "react";
import { Dimensions, FlatList, StyleProp, TouchableOpacity, View, ViewStyle } from "react-native";
import { useTheme } from "@react-navigation/native";
import createStyles from "./Filter.style"
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import MarkIcon from "../../assets//icons/markIcon";
import SearchIcon from "../../assets//icons/searchIcon";
import CheckBox from '@react-native-community/checkbox';
import CheckIcon from "../../assets//icons/checkIcon";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const Filter: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const [selectedFilter, setSelectedFilter] = useState();

    const [data, setData] = useState([
        {
            name: localStrings.briefNumber,
            isSelected: true
        },
        {
            name: localStrings.caseNumber,
            isSelected: false
        },
        {
            name: localStrings.caseType,
            isSelected: false
        },
        {
            name: localStrings.caseYear,
            isSelected: false
        },
        {
            name: localStrings.prevDate,
            isSelected: false
        },
        {
            name: localStrings.nextDate,
            isSelected: false
        },
        {
            name: localStrings.petitioner,
            isSelected: false
        },
        {
            name: localStrings.respondent,
            isSelected: false
        },
        {
            name: localStrings.petitionerAdv,
            isSelected: false
        },
        {
            name: localStrings.respondentsAdv,
            isSelected: false
        },
        {
            name: localStrings.courts,
            isSelected: false
        },
        {
            name: localStrings.caseStage,
            isSelected: false
        },
        {
            name: localStrings.judgeName,
            isSelected: false
        },
        {
            name: localStrings.attendance,
            isSelected: false
        },
        {
            name: localStrings.starredCases,
            isSelected: false
        },
    ])

    const options = [
        { item: "Marked" },
        { item: "Unmarked" }
    ]

    const handlePress = (item: any, index: number) => {
        const newData = data.map((item, i) => ({
            ...item,
            isSelected: i === index,
        }));
        setData(newData);
        setSelectedFilter(item.name)
    };

    const MarkedUnmarked = (isSelected: any, itemName: string) => {
        console.log(itemName)
        return <View style={{ flexDirection: 'row', backgroundColor: colors.iconWhite, alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 10 }}>
            <TextWrapper
                fontFamily={fonts.poppins.regular}
                color={colors.gray}
                leftIcon={<MarkIcon />}
                style={{ marginStart: 10 }}
                viewStyle={{ paddingHorizontal: 10, paddingVertical: 15, flexDirection: 'row', alignItems: 'center' }}>{'Marked'}</TextWrapper>
            <TextWrapper color={colors.gray}>999</TextWrapper>
        </View>
    }

    const SearchData = () => {
        return <>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 10}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextWrapper color={colors.darkGray} leftIcon={<CheckIcon/>} style={{marginStart: 10}} viewStyle={{flexDirection: 'row', alignItems: 'center', padding: 5}}>lorem ipsum</TextWrapper>
            </View>
            <TextWrapper color={colors.darkGray}>999</TextWrapper>
        </View>
        <View style={{backgroundColor: colors.background, height: 1}}/>
        </>
        
    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                <TextWrapper color={colors.gray} fontFamily={fonts.poppins.medium} style={{ fontSize: 16 }}>{'Filters'}</TextWrapper>
                <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium} style={{ fontSize: 16 }}>Clear All</TextWrapper>
            </View>

            <View style={{ flexDirection: 'row' }}>
                <FlatList
                    data={data}
                    style={{ width: Dimensions.get('window').width * 0.4, marginBottom: 70 }}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item, index }: { item: any, index: number }) => {
                        return <TouchableOpacity onPress={() => handlePress(item, index)}>
                            <TextWrapper
                                fontFamily={fonts.poppins.regular}
                                color={item.isSelected ? colors.iconWhite : colors.gray}
                                viewStyle={{ paddingHorizontal: 10, paddingVertical: 15, backgroundColor: item.isSelected ? colors.primary : colors.background }}>{item.name}</TextWrapper>
                        </TouchableOpacity>
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={() => {
                        return <View style={{ backgroundColor: colors.white, height: 1 }} />
                    }}
                />
                {selectedFilter == localStrings.petitioner ? <View style={{backgroundColor: colors.white}}>
                    <TextWrapper fontFamily={fonts.poppins.regular} color={colors.primary} viewStyle={styles.searchBox} style={{marginStart: 10}} leftIcon={<SearchIcon />}>{localStrings.quickSearch}</TextWrapper>
                    <View style={{backgroundColor: colors.background, height: 1}}/> 
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 10}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <CheckBox
                                disabled={false}
                                value={false}

                                onValueChange={(newValue) => {}}
                            />
                            <TextWrapper color={colors.darkGray}>Select All</TextWrapper>
                        </View>
                        <TextWrapper color={colors.darkGray}>999</TextWrapper>
                    </View>
                    <View style={{backgroundColor: colors.background, height: 1}}/>

                    {data.map((item) => {
                        return <SearchData/>
                    })}

                </View> :
                    <FlatList
                        data={options}
                        style={{ width: Dimensions.get('window').width * 0.6, backgroundColor: colors.white }}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item, index }: { item: any, index: number }) => {
                            return <MarkedUnmarked isSelected={item.isSelected} itemName={item.item} />
                        }}
                        ItemSeparatorComponent={() => {
                            return <View style={{ backgroundColor: colors.background, height: 1, width: '90%', alignSelf: 'center' }} />
                        }}
                        keyExtractor={(item, index) => index.toString()}
                    />}
            </View>
        </View>
    );
}

export default Filter;
