import { useMemo, useState } from "react";
import { Modal, ToastAndroid, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./ForgotPassword.style";
import { localStrings } from "../../shared/localization";
import InputText from "../../shared/components/InputText";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import fonts from "../../shared/theme/fonts";
import FilledButton from "../../shared/components/buttons/filledButton";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import EmailSentIcon from "../../assets//icons/emailSentIcon";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import { API } from "../../services/apiservices/apiHandler";
import { emailRegex } from "../../utils";
import Loader from "../../shared/components/loader/Loader";

interface ForgotPasswordProps {}

const ForgotPassword : React.FC<ForgotPasswordProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation();
    const [email, setEmail] = useState<string>("");
    const [modalVisible, setModalVisible] = useState(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isEmailError, setIsEmailError] = useState<boolean>(false);

    const _handleForgotPassword = async () => {
        if(emailRegex.test(email)) {
            try {
                setIsLoading(true)
                const apiEndpoint = `/api/forget-password`;
                const formData = { email };
    
                const response = await API.postAPIService(apiEndpoint, formData);
                
                if(response.message_type == "success") {
                    setIsLoading(false)
                    setModalVisible(true)
                }
            } catch (error) {
                console.error(error);
                setIsLoading(false)
            }
        } else {
            setIsEmailError(true)
        }
    }

    const _handleContinue = () => {
        setModalVisible(false)
        navigation.goBack()
    }

    return (
        <View style={styles.container}>
            {isLoading && <Loader/>}
            <View style={{flex: 0.28, justifyContent: 'center', }}>
                <TextWrapper style={styles.titleText} color={colors.primary} fontFamily={fonts.poppins.bold}>{localStrings.casewise}</TextWrapper>
            </View>
            <View style={{flex: 0.72}}>
                <View style={{flex:1}}>
                    <TextWrapper style={styles.welcomeText} fontFamily={fonts.poppins.semiBold}>{localStrings.forgot_pass}</TextWrapper>
                    <View style={{marginTop: '10%', marginHorizontal: '10%'}}>
                        <TextWrapper color={colors.gray} style={{textAlign: 'center', fontSize: 15}} fontFamily={fonts.poppins.regular}>{localStrings.noProblem}</TextWrapper>
                    </View>
                    <View style={[{marginTop: '10%',}, styles.mh20]}>
                        <InputText 
                            placeholder={localStrings.email} 
                            onChangeText={(text) => {
                                setEmail(text)
                                if(emailRegex.test(email)) setIsEmailError(false)
                            }}
                            value={email}
                            height={42}
                            inputStyle={styles.bgWhite}
                            borderColor={isEmailError ? colors.primary : colors.gray}
                        />
                        {isEmailError && <TextWrapper viewStyle={styles.mandatoryText} color={colors.primary} fontFamily={fonts.poppins.regular}>{localStrings.provideCorrectEmail}</TextWrapper>}
                    </View>
                </View>

                <Modal 
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => setModalVisible(false)}>
                        <View style={styles.modalOverlay}>
                            <View style={styles.modalContent}>
                                <View style={styles.emailIconCircle}>
                                    <View style={styles.emailIconContainer}>
                                        <EmailSentIcon/>
                                    </View>
                                </View>
                                <TextWrapper color={colors.primary} fontFamily={fonts.poppins.medium} style={{fontSize: 20}} viewStyle={{marginVertical: 20}}>{localStrings.emailSent}</TextWrapper>
                                <TextWrapper style={{textAlign: 'center', fontSize: 16}} color={colors.gray}>{localStrings.verification_mail_sent}</TextWrapper>
                                <RoundedButton btnType="filled" label={localStrings.continue!} labelFont={fonts.poppins.regular} style={{marginTop: 40, paddingVertical: 5}} labelStyle={{fontSize: 16}} onPress={_handleContinue}/>
                            </View>
                        </View>
                </Modal>
                <View style={{flex:1}}>
                    <View style={[styles.mh20]}>
                        <View style={styles.mh20}>
                            <FilledButton label={localStrings.sendLink!} style={{height: 42}} labelFont={fonts.poppins.regular} labelStyle={{fontSize: 18}} onPress={_handleForgotPassword}/>
                            <OutlinedButton label={localStrings.back!} borderWidth={1} labelStyle={{color: colors.gray, fontSize: 18}} labelColor = {colors.gray} onPress={() => navigation.goBack()} style={{height: 42}}/>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}

export default ForgotPassword;