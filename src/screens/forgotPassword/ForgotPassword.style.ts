import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
    titleTextStyle: {
      fontSize: 32,
    },
    buttonStyle: {
      height: 45,
      width: ScreenWidth * 0.9,
      marginTop: 32,
      borderRadius: 12,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.primary,
      shadowRadius: 5,
      shadowOpacity: 0.7,
      shadowColor: colors.shadow,
      shadowOffset: {
        width: 0,
        height: 3,
      },
    },
    bgWhite: {backgroundColor: colors.iconWhite},
    modalOverlay: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)', // Modal background transparency
    },
    modalContent: {
      width: 300,
      padding: 20,
      backgroundColor: 'white',
      borderRadius: 10,
      alignItems: 'center',
    },
    emailIconContainer: {backgroundColor:colors.primary, height: 77, width: 77, borderRadius: 38, alignItems: 'center', justifyContent: 'center'},
    emailIconCircle: {
      borderColor: colors.primary,
      borderWidth: 1,
      height: 102,
      width: 102,
      borderRadius: 51,
      alignItems: 'center',
      justifyContent: 'center'
    },
    closeButton: {
      marginTop: 20,
      padding: 10,
      backgroundColor: 'red',
      borderRadius: 5,
    },
    closeButtonText: {
      color: '#fff',
      fontSize: 16,
    },
    buttonTextStyle: {
      color: colors.iconWhite,
      fontWeight: "700",
    },
    header: {
      width: ScreenWidth * 0.9,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
    contentContainer: {
      flex: 1,
      marginTop: 16,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    titleText: {
      fontSize: 42,
      textAlign: 'center'
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 22,
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    forgotPassTxt: {
      fontSize: 14,
      fontFamily: fonts.poppins.regular,
      fontWeight: 'bold',
    },
    textGray16_500:{
      color: colors.gray,
      fontSize: 16,
      fontWeight: '500',
      textAlign: 'center'
    },
    mandatoryText: {
      width: '90%',
      alignSelf: 'center',
      marginTop: 5
    },
  });
};
