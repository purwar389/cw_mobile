import { ExtendedTheme } from "@react-navigation/native";
import { ViewStyle, StyleSheet, TextStyle } from "react-native";

interface Style {
    container: ViewStyle;
    buttonsContainer: ViewStyle;
    mh5: ViewStyle;
    mh20: ViewStyle;
    outlinedBtnStyle: ViewStyle;
    backBtnStyle: ViewStyle;
    casewiseText: TextStyle;
}

export default (theme: ExtendedTheme) => {
    const { colors } = theme;
    return StyleSheet.create<Style>({
        container: {
            flex: 1,
            backgroundColor: colors.background,
        },
        mh5: {
            marginHorizontal: 5
        },
        buttonsContainer: {
            flexDirection: 'row',
            justifyContent: 'center'
        },
        casewiseText: {
            fontSize: 42,
            textAlign: 'center'
        },
        mh20: {
            marginHorizontal: 20
        },
        outlinedBtnStyle: {
            flexDirection: 'row',
            height: 42,
            width: '80%',
            alignSelf: 'center',
            backgroundColor: colors.white,
            justifyContent: 'flex-start',
            paddingHorizontal: '5%',
          },
          backBtnStyle: {
            flexDirection: 'row',
            height: 42,
            width: '80%',
            alignSelf: 'center',
            paddingHorizontal: 10,
          }
    });
};
