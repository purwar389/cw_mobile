import { useMemo } from "react";
import { StyleProp, View, ViewStyle } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import { localStrings } from "../../shared/localization";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import OutlinedButton from "../../shared/components/buttons/OutlinedButton";
import fonts from "../../shared/theme/fonts";
import { SafeAreaView } from "react-native-safe-area-context";
import { SCREENS } from "../../shared/constants";
import createStyles from "./TellUsWhoUR.style";

type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SupportProps {
    style?: CustomStyleProp;
};

const TellUsWhoUR: React.FC<SupportProps> = () => {
    const theme = useTheme();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const navigation = useNavigation()

    return (
        <SafeAreaView style={styles.container}>
            <TextWrapper color={colors.primary} fontFamily={fonts.poppins.bold} style={styles.casewiseText} viewStyle={{flex: 0.3, justifyContent: 'center'}}>{localStrings.casewise}</TextWrapper>
            <TextWrapper style={{ fontSize: 22, textAlign: 'center' }} fontFamily={fonts.poppins.semiBold} color={colors.gray} >{localStrings.tell_us_you_are}</TextWrapper>
            <View style={{flex: 0.45}}>
                <OutlinedButton
                    label={localStrings.individual!}
                    labelStyle={{fontSize: 16}}
                    style={{...styles.outlinedBtnStyle, marginTop: '10%'}}
                    onPress={() => navigation.navigate(SCREENS.PLAN)}
                />
                <OutlinedButton
                    label={localStrings.lawFirmOrOrganisation!}
                    labelStyle={{fontSize: 16}}
                    style={styles.outlinedBtnStyle}
                    onPress={() => navigation.navigate(SCREENS.CONTACT_US)}
                />
            </View>
            <OutlinedButton
                label={localStrings.back!}
                labelStyle={{fontSize: 18, textAlign: 'center'}}
                labelColor={colors.gray}
                borderWidth={1}
                style={styles.backBtnStyle}
                onPress={() => navigation.goBack()}
            />
        </SafeAreaView>
    );
}

export default TellUsWhoUR;
