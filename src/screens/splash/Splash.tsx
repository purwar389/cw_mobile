import { Image, StyleProp, View, ViewStyle } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import { images } from "../../constants/images/images";
import { useEffect } from "react";
import { SCREENS } from "../../shared/constants";
import { ReduxServices } from "../../services/redux/ReduxService";
import { setToken } from "../../services/redux/token/TokenReducer";
import AsyncStorage from "@react-native-async-storage/async-storage";


type CustomStyleProp = StyleProp<ViewStyle> | Array<StyleProp<ViewStyle>>;

interface SplashProps {
    style?: CustomStyleProp;
};

const Splash: React.FC<SplashProps> = () => {
    const navigation = useNavigation();

    useEffect(() => {
        const checkUserToken = async () => {
            try {
                const token = await AsyncStorage.getItem('loggedInUserToken');
                
                if (token) {
                    ReduxServices.dispatchAction(setToken(token));
                    navigation.replace(SCREENS.HOME)
                } else {
                    // If token doesn't exist, navigate to login screen
                    navigation.replace(SCREENS.LOGIN_WITH_OTP); // Adjust as necessary
                }
            } catch (error) {
                console.error("Error retrieving token:", error);
                // Optionally handle the error, e.g., navigate to login
                navigation.replace(SCREENS.LOGIN_WITH_OTP); // Adjust as necessary
            }
        };

        const timer = setTimeout(() => {
            checkUserToken();
        }, 3000); // 3 seconds

        return () => clearTimeout(timer); // Cleanup the timer on unmount
    }, [navigation]);

    


    return (
        <View style={{flex: 1}}>
            <Image source={images.splash_image} style={{width: '100%' , height: '100%'}}/>
        </View>
    );
}

export default Splash;
