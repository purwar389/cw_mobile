import React, { useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import { ActivityIndicator, FlatList, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./ActiveCases.style";
import SelectButton from "../../shared/components/buttons/SelectButton";
import SearchBar from "../../shared/components/SearchBar/SearchBar";
import Ionicons from 'react-native-vector-icons/Ionicons';
import CaseCard from "../../shared/components/caseCard/CaseCard";
import fonts from "../../shared/theme/fonts";
import SortingBar from "../../shared/components/sortingBar/SortingBar";
import { localStrings } from "../../shared/localization";
import { API } from "../../services/apiservices/apiHandler";
import Header2 from "../../shared/components/headerComponent/Header2";

interface AddCaseScreenProps { }

const ActiveCases: React.FC<AddCaseScreenProps> = (props) => {
    const theme = useTheme();
    const styles = useMemo(() => createStyles(theme), [theme]);
    const [screen, setScreen] = useState("DCT");
    const { colors } = theme;
    const navigation = useNavigation();
    const refRBSheet = useRef(null);
    const title = props?.route?.params?.data;
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [caseData, setCaseData] = useState();

    const data = [
        {
            name: 'hello',
        },
        {
            name: 'hello',
        },
        {
            name: 'hello',
        },
        {
            name: 'hello',
        }
    ]

    useLayoutEffect(() => {
        console.log('title => ' + JSON.stringify(props));
        if (title != undefined) {
            navigation.setOptions({
                title: title
            });
        }
    }, [navigation, title]);

    const convertTitleToSlug = (title: string): string => {
        // Define specific replacements for known cases
        console.log(title);
        const specificReplacements: Record<string, string> = {
            "Today's Cases": "today-cases",
            "Tomorrow's Cases": "tomorrow-cases",
        };

        // Return specific replacements if available
        if (specificReplacements[title]) {
            return specificReplacements[title];
        }

        return title
            .toLowerCase()                  // Convert to lowercase
            .replace(/'/g, '')              // Remove apostrophes
            .replace(/\s+/g, '-')           // Replace spaces with hyphens
            .replace(/[^a-z0-9-]/g, '');    // Remove any non-alphanumeric characters except hyphens
    };

    const casesData = async (type: string, caseType: string) => {
        console.log('caseType => ' + caseType);
        const apiEndpoint = `/api/cases/${caseType}?type=${type}`;
        setIsLoading(true)
        try {
            const response = await API.getAPIService(apiEndpoint);
            console.log(`response ==> ` + JSON.stringify(response))
            setIsLoading(false);
            setCaseData(response)
        } catch (error) {
            setIsLoading(false)
            console.error('Error fetching active cases:', error);
            throw error;
        }
    }

    useEffect(() => {
        casesData("high", convertTitleToSlug(title))
    }, [])

    const titleToDataKeyMap: Record<string, string> = {
        'Active Cases': 'active_cases',
        'Today\'s Cases': 'today_cases',
        'Tomorrow\'s Cases': 'tomorrow_cases',
        'Date awaited Cases': 'date_awaited_cases',
        'Daily Board': 'daily_board',
    };

    // Your component code
    const ApniFlatlist = ({ caseData, title }: { caseData: any; title: string }) => {
        // Retrieve the data key from the title
        const dataKey = titleToDataKeyMap[title];

        // Safely access the data from caseData
        const data = caseData?.[dataKey]?.data || [];

        return (
            <FlatList
                data={data}
                renderItem={({ item }: { item: any }) => (
                    <CaseCard itemData={item} title={title} />
                )}
                keyExtractor={(_item, index) => index.toString()}
            />
        );
    };

    return (
        <SortingBar onPress={() => { }}>
            <Header2 label={title} navigation={navigation} />
            <View style={[styles.fdRow, { marginHorizontal: 15 }]}>
                <SelectButton
                    label={title == 'Archives' ? localStrings.decidedCase! : "District Courts & Tribunals"}
                    selected={screen == "DCT"}
                    labelStyle={styles.labelStyle}
                    style={{ flex: 1 }}
                    onPress={() => {
                        setScreen("DCT")
                        casesData("district", convertTitleToSlug(title))
                    }}
                    labelFontFamily={screen == "DCT" ? fonts.poppins.medium : fonts.poppins.regular}
                    outlineColor={colors.gray}
                />
                <SelectButton
                    label={title == 'Archives' ? localStrings.abondonedCase! : `High Courts &\nSupreme Court`}
                    selected={screen == "HCSC"}
                    labelStyle={styles.labelStyle}
                    style={{ flex: 1 }}
                    onPress={() => {
                        setScreen("HCSC")
                        casesData("high", convertTitleToSlug(title))
                    }}
                    labelFontFamily={screen == "HCSC" ? fonts.poppins.medium : fonts.poppins.regular}
                    outlineColor={colors.gray}
                // labelFontFamily={fonts.poppins.semiBold}
                />
            </View>

            <SearchBar
                placeholder="Search"
                onChangeText={() => { }}
                leftIcon={<Ionicons name='search' color={colors.primary} size={18} />}
                style={{ flexDirection: 'row', backgroundColor: colors.white, elevation: 3, marginVertical: 5, height: 37, }}
            />
            {isLoading ? <View style={{ flex: 1, justifyContent: 'center' }}><ActivityIndicator size={'large'} /></View> : <ApniFlatlist caseData={caseData} title={title} />}

            {/* <View style={{backgroundColor: 'black', height: 20}}/> */}
        </SortingBar>
    );
}

export default ActiveCases;
