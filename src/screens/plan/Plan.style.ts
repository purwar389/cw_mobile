import { ViewStyle, StyleSheet, TextStyle, ImageStyle } from "react-native";
import { ExtendedTheme } from "@react-navigation/native";
import { ScreenWidth } from "@freakycoder/react-native-helpers";
import fonts from "../../shared/theme/fonts";

export default (theme: ExtendedTheme) => {
  const { colors } = theme;
  return StyleSheet.create({
    container: {
      backgroundColor: colors.background,
    },
    titleTextStyle: {
      fontSize: 32,
    },
    fw600: {
      fontWeight: '600',
      textAlign: 'center'
    },
    buttonStyle: {
      height: 45,
      width: ScreenWidth * 0.9,
      marginTop: 32,
      borderRadius: 12,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.primary,
      shadowRadius: 5,
      shadowOpacity: 0.7,
      shadowColor: colors.shadow,
      shadowOffset: {
        width: 0,
        height: 3,
      },
    },
    fs12:{ fontSize: 12 },
    textViewStyle: {
      paddingStart: 15,
      height: 50,
      width: '100%',
      justifyContent: 'center',
    },
    renderedTextViewStyle: {
      width: 70,
      paddingEnd: 15,
      height: 50,
      justifyContent: "center",
      alignItems: 'center',
      marginHorizontal:5
    },
    buttonTextStyle: {
      color: colors.iconWhite,
      fontWeight: "700",
    },
    header: {
      width: ScreenWidth * 0.9,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
    },
    contentContainer: {
      flex: 1,
      marginTop: 16,
    },
    listContainer: {
      marginTop: 8,
    },
    profilePicImageStyle: {
      height: 50,
      width: 50,
      borderRadius: 30,
    },
    titleText: {
      color: colors.primary,
      fontSize: 48,
      fontFamily: fonts.poppins.regular,
      fontWeight: '700',
      textAlign: 'center'
    },
    welcomeText: {
      color: colors.gray,
      fontSize: 24,
      fontFamily: fonts.poppins.regular,
      fontWeight: '600',
      textAlign: 'center'
    },
    mh20: {
      marginHorizontal: 20
    },
    forgotPassTxt: {
      fontSize: 14,
      fontFamily: fonts.poppins.regular,
      fontWeight: 'bold',
    },
    textGray16_500:{
      color: colors.gray,
      fontSize: 16,
      fontWeight: '500',
      textAlign: 'center'
    },
    centredContianer: {
      flex:1,
      justifyContent: 'center',
    },
    mV30: {
      marginVertical: 30
    },
    spearatorLine: { backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }
  });
};
