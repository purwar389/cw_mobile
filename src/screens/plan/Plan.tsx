import { ScrollView, View } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import createStyles from "./Plan.style";
import { localStrings } from "../../shared/localization";
import Header from "../../shared/components/header/Header";
import TextWrapper from "../../shared/components/text-wrapper/TextWrapper";
import { FlatList } from "react-native-gesture-handler";
import RoundedButton from "../../shared/components/buttons/RoundedButton";
import { useMemo, useState } from "react";
import { SCREENS } from "../../shared/constants";
import { SafeAreaView } from "react-native-safe-area-context";
import fonts from "../../shared/theme/fonts";
import RightIcon from "../../assets//icons/rightIcon";
import SwitchToggle from "@imcarlosguerrero/react-native-switch-toggle";

interface PlanScreenProps {};

const Plan: React.FC<PlanScreenProps> = () => {
    const theme = useTheme();
    const navigation = useNavigation();
    const { colors } = theme;
    const styles = useMemo(() => createStyles(theme), [theme]);
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const plan2 = [
        {
            subscription_name: "Basic",
            total_features: [
                {
                    title: "No. of cases",
                    isAvailable: true
                },
                {
                    title: "Daily Board",
                    isAvailable: true
                },
                {
                    title: "Calendar",
                    isAvailable: false
                },
                {
                    title: "Bare Acts",
                    isAvailable: false
                },
                {
                    title: "Notes",
                    isAvailable: false
                },
                {
                    title: "My Clients",
                    isAvailable: true
                },
                {
                    title: "Case History",
                    isAvailable: true
                },
                {
                    title: "To Do List",
                    isAvailable: true
                },
                {
                    title: "Connected Matters",
                    isAvailable: true
                },
                {
                    title: "Case Extraction in Excel, PDF",
                    isAvailable: true
                },
                {
                    title: "Court Fee Calculator",
                    isAvailable: true
                },
                {
                    title: "Case Assignment with team",
                    isAvailable: true
                }
            ],
            price: 299
        },
        {
            subscription_name: "Standard",
            total_features: [
                {
                    title: "No. of cases",
                    isAvailable: true
                },
                {
                    title: "Daily Board",
                    isAvailable: true
                },
                {
                    title: "Calendar",
                    isAvailable: false
                },
                {
                    title: "Bare Acts",
                    isAvailable: false
                },
                {
                    title: "Notes",
                    isAvailable: false
                },
                {
                    title: "My Clients",
                    isAvailable: true
                },
                {
                    title: "Case History",
                    isAvailable: true
                },
                {
                    title: "To Do List",
                    isAvailable: true
                },
                {
                    title: "Connected Matters",
                    isAvailable: true
                },
                {
                    title: "Case Extraction in Excel, PDF",
                    isAvailable: true
                },
                {
                    title: "Court Fee Calculator",
                    isAvailable: true
                },
                {
                    title: "Case Assignment with team",
                    isAvailable: true
                }
            ],
            price: 299
        },
        {
            subscription_name: "Premium",
            total_features: [
                {
                    title: "No. of cases",
                    isAvailable: true
                },
                {
                    title: "Daily Board",
                    isAvailable: true
                },
                {
                    title: "Calendar",
                    isAvailable: false
                },
                {
                    title: "Bare Acts",
                    isAvailable: false
                },
                {
                    title: "Notes",
                    isAvailable: false
                },
                {
                    title: "My Clients",
                    isAvailable: true
                },
                {
                    title: "Case History",
                    isAvailable: true
                },
                {
                    title: "To Do List",
                    isAvailable: true
                },
                {
                    title: "Connected Matters",
                    isAvailable: true
                },
                {
                    title: "Case Extraction in Excel, PDF",
                    isAvailable: true
                },
                {
                    title: "Court Fee Calculator",
                    isAvailable: true
                },
                {
                    title: "Case Assignment with team",
                    isAvailable: true
                }
            ],
            price: 299
        }
    ];

    const renderItems = ({ item, index }: { item: any, index: number }) => {
        return (
            <View>
                <TextWrapper color={colors.primary} viewStyle={{ alignItems: 'center', width: 70, height: 30, marginHorizontal:5 }} fontFamily={fonts.poppins.medium}>{item.subscription_name}</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.medium}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <View style={{ width: 70, height: 50, alignItems: 'center', justifyContent: 'center' }}><RightIcon /></View>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><RightIcon /></View>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.darkGray} fontFamily={fonts.poppins.semiBold}>-</TextWrapper>
                <View style={{ backgroundColor: colors.iconWhite, height: 1, width: '100%', alignSelf: 'center' }} />
                <TextWrapper viewStyle={styles.renderedTextViewStyle} color={colors.primary} fontFamily={fonts.poppins.semiBold}>{item.price}</TextWrapper>
            </View>
        );
    }

    return (
        <SafeAreaView>
            <Header label={localStrings.choosePlan!} navigation={navigation} />
            <ScrollView style={styles.container}>
                <View style={{marginBottom: 70}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20, alignItems: 'center' }}>
                        <TextWrapper color={isEnabled ? colors.gray : colors.primary}
                            fontFamily={!isEnabled ? fonts.poppins.semiBold : fonts.poppins.regular} style={{ fontSize: 16 }}>{localStrings.monthly}</TextWrapper>
                        <View style={{ marginHorizontal: 10 }}>
                            <SwitchToggle
                                isOn={isEnabled}
                                onColor={colors.cardBg}
                                offColor={colors.cardBg}
                                size="small"
                                trackOffStyle={{ borderColor: colors.text, borderWidth: 1 }}
                                trackOnStyle={{ borderColor: colors.primary, borderWidth: 1 }}
                                circleColor={colors.primary}
                                onToggle={toggleSwitch}
                            />
                        </View>
                        <TextWrapper
                            color={isEnabled ? colors.primary : colors.gray}
                            fontFamily={isEnabled ? fonts.poppins.semiBold : fonts.poppins.regular} style={{ fontSize: 16 }}>{localStrings.yearly}</TextWrapper>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5, marginTop: 50 }}>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>No. of cases</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.dailyBoard}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.calendar}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.bareActs}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.notes}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.myClients}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.caseHistory}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.todoList}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.connectedMatters}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.caseExtraction}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.courtFeeCalculator}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.caseAssignmentWithTeam}</TextWrapper>
                            <View style={styles.spearatorLine} />
                            <TextWrapper viewStyle={styles.textViewStyle} style={styles.fs12} fontFamily={fonts.poppins.medium}>{localStrings.price}</TextWrapper>
                        </View>

                        <View style={{flex: 0.5, marginTop: 20}}>
                            <FlatList
                                data={plan2}
                                horizontal
                                renderItem={renderItems}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>

                    <View style={{ justifyContent: 'center', width: 200, alignSelf: 'center', marginTop: 10 }}>
                        <RoundedButton
                            btnType="outlined"
                            label="Get Started"
                            borderWidth={1}
                            onPress={() => { navigation.navigate(SCREENS.REGISTER) }}
                            labelStyle={{ fontFamily: fonts.poppins.semiBold, fontSize: 20 }}
                            labelFont={fonts.poppins.medium}
                            style={{ height: 46, borderRadius: 23}}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

export default Plan;